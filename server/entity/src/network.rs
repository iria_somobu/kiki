use sea_orm::entity::prelude::*;
use super::user;

#[derive(Clone, Debug, PartialEq, DeriveEntityModel)]
#[sea_orm(table_name = "network")]
pub struct Model {
    #[sea_orm(primary_key, auto_increment = false)]
    pub id: String,
    pub title: String,
    pub descr: String,
    pub icon: String,
    pub ext_info: String,
    pub creator: i64,
    pub owner: i64,
    pub stoplist: String
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "user::Entity",
        from = "Column::Creator",
        to = "user::Column::Id",
        on_update = "Cascade",
        on_delete = "Restrict"
    )]
    Creator,
    #[sea_orm(
        belongs_to = "user::Entity",
        from = "Column::Owner",
        to = "user::Column::Id",
        on_update = "Cascade",
        on_delete = "Restrict"
    )]
    Owner
}

impl ActiveModelBehavior for ActiveModel {}
