use sea_orm::entity::prelude::*;
use super::token;

#[derive(Clone, Debug, PartialEq, DeriveEntityModel)]
#[sea_orm(table_name = "auth_log")]
pub struct Model {
    #[sea_orm(primary_key, auto_increment = false)]
    pub token: String,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "token::Entity",
        from = "Column::Token",
        to = "token::Column::Token",
        on_update = "Cascade",
        on_delete = "Restrict"
    )] 
    Token 
}

impl Related<token::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Token.def()
    } 
}

impl ActiveModelBehavior for ActiveModel {}
