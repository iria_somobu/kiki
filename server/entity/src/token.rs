use sea_orm::entity::prelude::*;
use super::user;

#[derive(Clone, Debug, PartialEq, DeriveEntityModel)]
#[sea_orm(table_name = "token")]
pub struct Model {
    #[sea_orm(primary_key, auto_increment = false)]
    pub token: String,
    pub user: i64,
    pub created: i64,
    pub expires: i64,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "user::Entity",
        from = "Column::User",
        to = "user::Column::Id",
        on_update = "Cascade",
        on_delete = "Restrict"
    )] 
    User
}

impl ActiveModelBehavior for ActiveModel {}
