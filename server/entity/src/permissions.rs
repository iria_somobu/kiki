use sea_orm::entity::prelude::*;

use super::user;
use super::network;
use super::board;

#[derive(Clone, Debug, PartialEq, DeriveEntityModel)]
#[sea_orm(table_name = "permissions")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub user: i64,
    #[sea_orm(primary_key)]
    pub network: String,
    #[sea_orm(primary_key)]
    pub board: String,
    pub perm_incl: i32,
    pub perm_excl: i32,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "user::Entity",
        from = "Column::User",
        to = "user::Column::Id",
        on_update = "Cascade",
        on_delete = "Cascade"
    )] 
    User,
    #[sea_orm(
        belongs_to = "network::Entity",
        from = "Column::Network",
        to = "network::Column::Id",
        on_update = "Cascade",
        on_delete = "Cascade"
    )]
    Network,
    #[sea_orm(
        belongs_to = "board::Entity",
        from = "(Column::Board, Column::Network)",
        to = "(board::Column::Name, board::Column::Network)",
        on_update = "Cascade",
        on_delete = "Cascade"
    )] 
    Board
}

impl ActiveModelBehavior for ActiveModel {}
