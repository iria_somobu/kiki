use sea_orm::entity::prelude::*;

use super::{network, user};

#[derive(Clone, Debug, PartialEq, DeriveEntityModel)]
#[sea_orm(table_name = "board")]
pub struct Model {
    #[sea_orm(primary_key, auto_increment = false)]
    pub network: String,
    #[sea_orm(primary_key, auto_increment = false)]
    pub name: String,
    pub position: i32,
    pub group: String,
    pub descr: String,
    pub creator: i64,
    pub owner: i64,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "network::Entity",
        from = "Column::Network",
        to = "network::Column::Id",
        on_update ="Cascade",
        on_delete = "Cascade"
    )]
    Network,
    #[sea_orm(
        belongs_to = "user::Entity",
        from = "Column::Creator",
        to = "user::Column::Id",
        on_update = "Cascade",
        on_delete = "Restrict"
    )]
    Creator,
    #[sea_orm(
        belongs_to = "user::Entity",
        from = "Column::Owner",
        to = "user::Column::Id",
        on_update = "Cascade",
        on_delete = "Restrict"
    )]
    Owner
}

impl ActiveModelBehavior for ActiveModel {}
