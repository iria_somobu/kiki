use sea_orm::entity::prelude::*;
use super::post;
#[derive(Copy, Clone, Default, Debug, DeriveEntity)]
pub struct Entity;


impl EntityName for Entity {
    fn table_name(&self) -> &str {
        "post_parents"
    }
}

#[derive(Clone, Debug, PartialEq, DeriveModel, DeriveActiveModel)]
pub struct Model {
    pub post: String,
    pub parent: String,
}


#[derive(Copy, Clone, Debug, EnumIter, DerivePrimaryKey)]
pub enum PrimaryKey {
    Post,
    Parent
}

impl PrimaryKeyTrait for PrimaryKey {
    type ValueType = (String, String);

    fn auto_increment() -> bool {
        false
    }
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveColumn)]
pub enum Column {
    Post,
    Parent
}

impl ColumnTrait for Column {
    type EntityName = Entity;

    fn def(&self) -> ColumnDef {
        match self {
            Column::Post => ColumnType::String(None).def(),
            Column::Parent => ColumnType::String(None).def(),
        }
    }
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "post::Entity",
        from = "Column::Post",
        to = "post::Column::Hash",
        on_update ="Cascade",
        on_delete = "Cascade"
    )]
    Post,
    #[sea_orm(
        belongs_to = "post::Entity",
        from = "Column::Parent",
        to = "post::Column::Hash",
        on_update ="Cascade",
        on_delete = "Cascade"
    )]
    Parent
}

impl ActiveModelBehavior for ActiveModel {}
