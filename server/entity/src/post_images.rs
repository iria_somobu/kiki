use sea_orm::entity::prelude::*;

use super::image;
use super::post;

#[derive(Copy, Clone, Default, Debug, DeriveEntity)]
pub struct Entity;


impl EntityName for Entity {
    fn table_name(&self) -> &str {
        "post_images"
    }
}

#[derive(Clone, Debug, PartialEq, DeriveModel, DeriveActiveModel)]
pub struct Model {
    pub post: String,
    pub img: String,
}


#[derive(Copy, Clone, Debug, EnumIter, DerivePrimaryKey)]
pub enum PrimaryKey {
    Post,
    Img
}

impl PrimaryKeyTrait for PrimaryKey {
    type ValueType = (String, String);

    fn auto_increment() -> bool {
        false
    }
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveColumn)]
pub enum Column {
    Post,
    Img
}

impl ColumnTrait for Column {
    type EntityName = Entity;

    fn def(&self) -> ColumnDef {
        match self {
            Column::Post => ColumnType::String(None).def(),
            Column::Img => ColumnType::String(None).def(),
        }
    }
}


#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "image::Entity",
        from = "Column::Img",
        to = "image::Column::Img",
        on_update = "Cascade",
        on_delete = "Restrict"
    )]
    Image,
    #[sea_orm(
        belongs_to = "post::Entity",
        from = "Column::Post",
        to = "post::Column::Hash",
        on_update = "Cascade",
        on_delete = "Restrict"
    )]
    Post,
}

impl Related<image::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Image.def()
    }
}

impl Related<post::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Post.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}
