use sea_orm::entity::prelude::*;
use super::{board, network, token};

#[derive(Clone, Debug, PartialEq, DeriveEntityModel)]
#[sea_orm(table_name = "post")]
pub struct Model {
    #[sea_orm(primary_key, auto_increment = false)]
    pub hash: String,
    pub root: String,
    pub network: Option<String>,
    pub board: Option<String>,
    pub poster: Option<String>,

    pub date: i64,
    pub title: Option<String>,
    pub message: String,

    pub hidden: bool,
    pub on_hold: bool,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "Entity",
        from = "Column::Root",
        to = "Column::Hash",
        on_update ="Cascade",
        on_delete = "Cascade"
    )]
    Root,
    #[sea_orm(
        belongs_to = "board::Entity",
        from = "(Column::Board, Column::Network)",
        to = "(board::Column::Name, board::Column::Network)",
        on_update ="Cascade",
        on_delete = "Cascade"
    )]
    Board,
    #[sea_orm(
        belongs_to = "network::Entity",
        from = "Column::Network",
        to = "network::Column::Id",
        on_update ="Cascade",
        on_delete = "Cascade"
    )]
    Network,
    #[sea_orm(
        belongs_to = "token::Entity",
        from = "Column::Poster",
        to = "token::Column::Token",
        on_update ="Cascade",
        on_delete = "SetNull"
    )]
    Poster
}

impl ActiveModelBehavior for ActiveModel {}
