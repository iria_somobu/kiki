use migration::{Migrator, MigratorTrait};
use sea_orm::ColumnTrait;
use sea_orm::QueryFilter;
use sea_orm::{Database, DbConn, EntityTrait};

use kitsuki_server::config;

async fn get_db() -> DbConn {
    let config: config::Cfg = config::read();
    println!("{}", &config.database_path);
    std::fs::remove_file(config.database_path.clone()).ok();

    let db: DbConn = Database::connect(config.database_uri).await.unwrap();
    Migrator::up(&db, None).await.unwrap();

    return db;
}

#[tokio::test]
async fn anonymous_user() {
    let db = get_db().await;

    let user = entity::user::Entity::find()
        .filter(entity::user::Column::Id.eq(0))
        .into_model::<entity::user::Model>()
        .one(&db)
        .await
        .unwrap()
        .expect("User id 0 must be present in DB");

    assert_eq!(
        user.name, "anonymous",
        "User id 0 must be named as 'anonymous'"
    );
    assert_eq!(
        user.is_admin, false,
        "Anonymous user must not have admin rights"
    );
    assert_eq!(user.secret, "", "Anonymous user must have empty password")
}
