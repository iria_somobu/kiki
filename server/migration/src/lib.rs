pub use sea_orm_migration::prelude::*;

mod m20220901_init_db;
mod m20221010_upload;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20220901_init_db::Migration),
            Box::new(m20221010_upload::Migration),
        ]
    }
}
