use sea_orm::Statement;
use sea_orm_migration::DbErr;
use sea_orm_migration::prelude::*;
use sea_orm_migration::sea_orm::ConnectionTrait;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        create_tables(manager).await;

        if cfg!(debug_assertions) {
            fill_debug(manager).await;
        }

        return Ok(());
    }

    async fn down(&self, _manager: &SchemaManager) -> Result<(), DbErr> {
        Err(DbErr::Custom("Sorry :/".to_string()))
    }
}

async fn create_tables(manager: &SchemaManager<'_>) {
    let sql = r#"
CREATE TABLE image (
    "img" text NOT NULL PRIMARY KEY,
    "ext" text NOT NULL
);

CREATE TABLE post_images (
    "post" text NOT NULL,
    "img" text NOT NULL,
    FOREIGN KEY ("post") REFERENCES "post" ("hash") ON DELETE RESTRICT ON UPDATE CASCADE
    FOREIGN KEY ("img") REFERENCES "image" ("img") ON DELETE RESTRICT ON UPDATE CASCADE
);

ALTER TABLE post DROP COLUMN images;

UPDATE network SET icon = '829cd79f-a17e-446c-84da-f915ba7e798e' WHERE icon = "";
    "#;

    let stmt = Statement::from_string(manager.get_database_backend(), sql.to_owned());
    manager.get_connection().execute(stmt).await.unwrap();
}

async fn fill_debug(manager: &SchemaManager<'_>) {
    let sql = r#"
INSERT INTO image (img, ext) VALUES ("829cd79f-a17e-446c-84da-f915ba7e798e", "png");

INSERT INTO post_images (post, img) VALUES ("101", "829cd79f-a17e-446c-84da-f915ba7e798e");
INSERT INTO post_images (post, img) VALUES ("105", "829cd79f-a17e-446c-84da-f915ba7e798e");
    "#;

    let stmt = Statement::from_string(manager.get_database_backend(), sql.to_owned());
    manager.get_connection().execute(stmt).await.unwrap();
}
