use sea_orm::Statement;
use sea_orm_migration::DbErr;
use sea_orm_migration::prelude::*;
use sea_orm_migration::sea_orm::ConnectionTrait;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        create_tables(manager).await;
        fill_data(manager).await;

        if cfg!(debug_assertions) {
            fill_debug(manager).await;
        }

        return Ok(());
    }

    async fn down(&self, _manager: &SchemaManager) -> Result<(), DbErr> {
        Err(DbErr::Custom("We cannot go deeper -- it is first migration already!".to_string()))
    }
}

async fn create_tables(manager: &SchemaManager<'_>) {
    let sql = r#"
CREATE TABLE "user" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" text NOT NULL,
    "is_admin" boolean NOT NULL DEFAULT false,
    "secret" text NOT NULL DEFAULT ""
);

CREATE TABLE "token" (
    "token" text NOT NULL PRIMARY KEY,
    "user" integer NOT NULL,
    "created" integer NOT NULL DEFAULT 0,
    "expires" integer NOT NULL DEFAULT 0,
    FOREIGN KEY ("user") REFERENCES "user" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE "post" (
    "hash" text NOT NULL PRIMARY KEY,
    "root" text NOT NULL,
    "network" text,
    "board" text,
    "poster" text,
    "date" integer NOT NULL,
    "title" text,
    "message" text NOT NULL,
    "images" text,
    "hidden" boolean NOT NULL DEFAULT false,
    "on_hold" boolean NOT NULL DEFAULT false,
    FOREIGN KEY ("root") REFERENCES "post" ("hash") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("board", "network") REFERENCES "board" ("name", "network") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("network") REFERENCES "network" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("poster") REFERENCES "token" ("token") ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE "permissions" (
    "user" integer NOT NULL,
    "network" text NOT NULL,
    "board" text NOT NULL,
    "perm_incl" integer NOT NULL,
    "perm_excl" integer NOT NULL,
    CONSTRAINT "pk-permissions" PRIMARY KEY ("user", "network", "board"),
    FOREIGN KEY ("user") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("network") REFERENCES "network" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("board", "network") REFERENCES "board" ("name", "network") ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE "network" (
    "id" text NOT NULL PRIMARY KEY,
    "title" text NOT NULL,
    "descr" text NOT NULL,
    "icon" text NOT NULL,
    "ext_info" text NOT NULL,
    "creator" integer NOT NULL,
    "owner" integer NOT NULL,
    "stoplist" text NOT NULL DEFAULT "",
    FOREIGN KEY ("creator") REFERENCES "user" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY ("owner") REFERENCES "user" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE "board" (
    "network" text NOT NULL,
    "name" text NOT NULL,
    "position" integer NOT NULL,
    "group" text NOT NULL,
    "descr" text NOT NULL,
    "creator" integer NOT NULL,
    "owner" integer NOT NULL,
    CONSTRAINT "pk-board" PRIMARY KEY ("network", "name"),
    FOREIGN KEY ("network") REFERENCES "network" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("creator") REFERENCES "user" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY ("owner") REFERENCES "user" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE "post_parents" (
    "post" text NOT NULL,
    "parent" text NOT NULL,
    FOREIGN KEY ("post") REFERENCES "post" ("hash") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("parent") REFERENCES "post" ("hash") ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE "auth_log" (
    "token" text NOT NULL PRIMARY KEY,
    FOREIGN KEY ("token") REFERENCES "token" ("token") ON DELETE RESTRICT ON UPDATE CASCADE
);"#;
    let stmt = Statement::from_string(manager.get_database_backend(), sql.to_owned());
    manager.get_connection().execute(stmt).await.unwrap();
}

async fn fill_data(manager: &SchemaManager<'_>) {
    let sql = r#"INSERT INTO user (id, name, is_admin, secret) VALUES (0, "anonymous", false, "");"#;
    let stmt = Statement::from_string(manager.get_database_backend(), sql.to_owned());
    manager.get_connection().execute(stmt).await.unwrap();
}

async fn fill_debug(manager: &SchemaManager<'_>) {
    let sql = r#"
INSERT INTO user (id, name, is_admin, secret) VALUES
    (1, "1", true, "1"),
    (6, "Iria.Somobu", true, "repwace me");

INSERT INTO network (id, title, descr, icon, ext_info, creator, owner, stoplist) VALUES
    ("main-ru", "Кицуки RU", "Основная русскоязычная сеть", "", "Extended network info here", 6, 6, "хуй;пизд;бляд"),
    ("rust-ru", "Rust RU", "Основная растоязычная сеть", "", "cannot borrow `numbers` as mutable because it is also borrowed as immutable", 6, 6, "C++;C;JavaScript;C#;JS");

INSERT INTO board (network, position, name, "group", descr, creator, owner) VALUES
    ("main-ru",  0, "general",  "general", "SFW без определенной темы", 6, 6),
    ("main-ru",  1, "anime",    "general", "Аниме, около-аниме",        6, 6),
    ("main-ru",  2, "memes",    "general", "Смешные картинки",          6, 6),
    ("main-ru",  3, "tech",     "general", "Железо и софт",             6, 6),
    ("main-ru",  4, "games",    "general", "Видеоигры",                 6, 6),
    ("rust-ru",  5, "nature",    "nature", "О глубинной природе раста",                 6, 6);

INSERT INTO post (network, board, hash, root, date, title, message) VALUES
    ("main-ru", "general",  "0", "0", 1000000000000, "Dev server warning", "All posts will be removed upon new commit on master"),
    ("main-ru", "anime",    "1", "1", 1000000000000, "Dev server warning", "All posts will be removed upon new commit on master"),
    ("main-ru", "memes",    "2", "2", 1000000000000, "Dev server warning", "All posts will be removed upon new commit on master"),
    ("main-ru", "tech",     "3", "3", 1000000000000, "Dev server warning", "All posts will be removed upon new commit on master"),
    ("main-ru", "games",    "4", "4", 1000000000000, "Dev server warning", "All posts will be removed upon new commit on master");

INSERT INTO post (network, board, hash, root, date, title, message) VALUES
    ("rust-ru", "nature",  "rust101", "rust101", 0, "О глубинной природе раста и почему раст создан богами", "Когда-то давно... Был раст..."),
    (null, null,  "rust102", "rust101", 0, "", "Что ты несёшь! Остановись!");

INSERT INTO post (network, board, hash, root, date, title, message) VALUES
    ("main-ru", "general",  "101", "101", 0, "Тестовый тред специально для Сани", "Дадададада"),
    (null, null, "102", "101", 0, "", "Ето такой довольно короткий ответ чтобы не писать много букв"),
    (null, null, "103", "101", 0, "", "Ну и третий ответ еще немного совсем напишу чисто чтобы текст был"),
    (null, null, "104", "101", 0, "", "Определённо, мне нужно ещё больше этих замечательных постов, потому что их не бывает мало"),
    (null, null, "105", "101", 0, "", "И каждый, кто считает иначе - это глупец, который ничего не понимает в этой жизни!"),
    (null, null, "106", "101", 0, "", "Просто я люблю тестовые данные"),
    (null, null, "107", "101", 0, "", "Сегодня, этим утром я понял, что нам нужно добавить чуть больше этих"),
    (null, null, "108", "101", 0, "", "Объективно прекрасных и замечательных сообщений"),
    (null, null, "109", "101", 0, "", "The are many programming languages"),
    (null, null, "110", "101", 0, "", "But only few of them are really good"),
    (null, null, "111", "101", 0, "", "Очередное странное сообщение, в котором нет смысла"),
    (null, null, "112", "101", 0, "", "Смысла нет, но вы держитесь!");

INSERT INTO "post_parents" ("post", "parent") VALUES
    ("102", "101"),
    ("103", "101"),
    ("103", "102"),
    ("104", "101"),
    ("104", "102"),
    ("104", "103"),
    ("105", "101"),
    ("105", "102"),
    ("105", "103"),
    ("105", "104"),
    ("106", "101"),
    ("106", "102"),
    ("106", "103"),
    ("106", "104"),
    ("107", "105"),
    ("108", "106"),
    ("109", "106"),
    ("110", "106"),
    ("111", "106"),
    ("112", "106");
"#;
    let stmt = Statement::from_string(manager.get_database_backend(), sql.to_owned());
    manager.get_connection().execute(stmt).await.unwrap();
}
