//! Holds `Subscription` trait that describes all possible graphql `subscription` requests

#![allow(non_snake_case)] // Important: write graphql impl functions in camelCase please

use std::pin::Pin;

use futures::Stream;

use crate::structures::{*};

pub type ThreadsStream = Pin<Box<dyn Stream<Item=Thread> + Send>>;
pub type PostsStream = Pin<Box<dyn Stream<Item=Post> + Send>>;

/// Yup, this trait describes `subscription` requests
pub trait Subscription {
    /// All new threads
    fn thread(token: String, net: String, board: String) -> ThreadsStream;

    /// All new posts (new thread posts included)
    fn post(token: String, rootHash: String) -> PostsStream;
}