//! Holds `Mutation` trait that describes all possible graphql `mutation` requests

#![allow(non_snake_case)] // Important: write graphql impl functions in camelCase please

use juniper::FieldResult;

use crate::structures::{*};

/// Yup, this trait describes `mutation` requests
pub trait Mutation {
    /// TODO: describe rate-limits
    fn newToken() -> Token;

    /// Check if current token is still valid
    ///
    /// `returns` current or new token
    fn validateToken(token: String) -> FieldResult<Token>;

    /// Temporary login
    fn login(name: String, pwd: String) -> FieldResult<Token>;

    /// Check `postPost` for details
    fn postThread(
        token: String,
        net: String,
        board: String,
        message: String,
        title: String,
        images: Vec<String>,
    ) -> FieldResult<Thread>;


    /// Post a new message
    ///
    /// `net`: network name to post to
    ///
    /// `to` is array of parent hashes. Limitations:
    ///  - must contain at least one item;
    ///  - all hashes must be valid (i.e. present in our database);
    ///
    /// `message` must not be empty.
    ///
    /// `images` may be empty.
    ///
    /// `images` is array of filenames. Limitations:
    ///  - max 7 items;
    ///  - all items must be valid (i.e. all images must be present on server).
    ///
    /// Process of loading images on server will be discussed later.
    ///
    /// `returns` newly-created post
    fn postPost(
        token: String,
        net: String,
        to: Vec<String>,
        message: String,
        images: Vec<String>,
    ) -> FieldResult<Post>;

    ///
    fn modPost(token: String, post: String, hidden: bool, on_hold: bool) -> FieldResult<String>;
}