//! Holds `Query` trait that describes all possible graphql `query` requests

#![allow(non_snake_case)] // Important: write graphql impl functions in camelCase please

use juniper::FieldResult;

use crate::structures::{*};

/// Yup, this trait describes `query` requests
pub trait Query {
    /// List all networks available to current user
    fn networks(token: String) -> FieldResult<Vec<Net>>;

    /// Get single network
    fn network(token: String, net: String) -> FieldResult<Net>;

    fn extendedNetworkInfo(token: String, net: String) -> String;

    /// List all boards on given network
    fn boardGroups(token: String, net: String) -> Vec<BoardGroup>;

    /// Get certain board
    fn board(token: String, net: String, board: String) -> FieldResult<Board>;

    /// TODO: replace with paginated version
    ///
    /// badgeTime, seconds: all messages after this time will be counted as unread
    fn threads(token: String, net: String, board: String, badgeTime: i32) -> FieldResult<Vec<Thread>>;

    /// TODO: replace with paginated version
    fn posts(token: String, thread: String) -> Vec<Post>;

    /// Get single post
    fn post(token: String, hash: String) -> FieldResult<Post>;

    /// Get network statistics
    fn netStats(token: String) -> NetStats;

    /// Returns current user when logged in and error if not
    fn me(token: String) -> FieldResult<User>;
}