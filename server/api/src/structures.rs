//! Api structures shared between client and server are described in this module.

#![allow(non_snake_case)] // Important: write graphql functions and objects names in camelCase please

/// Token flag. Means "this token is a named user"
pub const FLAG_NAMED: i32 = 1 << 0;

/// Token flag. Means "this token can perform moderator actions"
pub const FLAG_MODER: i32 = 1 << 1;

pub const MAX_TITLE_LEN: usize = 80;
pub const MAX_CONTENT_LEN: usize = 6900;
pub const MAX_PARENTS_LEN: usize = 7;
pub const MAX_IMGS_LEN: usize = 7;

/// Generic database error
pub const ERR_DB_FAIL: &'static str = "DB_ERR";

pub const ERR_NOT_FOUND: &'static str = "NOT_FOUND";

pub const ERR_AUTH_WRONG_CREDS: &'static str = "WRONG_CREDS";

pub const ERR_AUTH_NOT_A_MOD: &'static str = "NOT_A_MOD";

/// Duplicate post
pub const ERR_POST_DUPE: &'static str = "DUPLICATE_POST";

pub const ERR_POST_TOO_BIG: &'static str = "TOO_BIG";

pub const ERR_POST_BAD_ROOT: &'static str = "INVALID_ROOT";

/// Post on hold (will be inspected by mod)
pub const ERR_POST_ON_HOLD: &'static str = "POST_ON_HOLD";

/// `Board` is a named group of `Thread`s
#[derive(juniper::GraphQLObject, sea_orm::FromQueryResult, Clone)]
pub struct Board {
    /// Should be relatively short (up to 8 chars?), latin letters and digits only
    pub name: String,

    /// Human-readable description
    pub descr: String,

    /// Board sorting position, global across all boards
    pub position: i32,

    /// Group identifier, same limitations as for name
    pub group: String,

    /// Time of the last thread created on this board, in seconds
    pub lastUpdateTime: Option<i32>
}

/// For client's convenience, boards are grouped by `BoardGroup`s
#[derive(juniper::GraphQLObject)]
pub struct BoardGroup {
    /// Should be relatively short (up to 8 chars?), latin letters and digits only
    pub name: String,

    /// Position is a minimal position of contained boards
    pub position: i32,

    /// Yup, this is boards list of this group, sorted by board's `position`
    pub boards: Vec<Board>,
}

/// Net, or Network, is a container for all the threads, posts and boards.
///
/// Think of it as an equivalent of Discord's server.
#[derive(juniper::GraphQLObject, sea_orm::FromQueryResult)]
pub struct Net {
    /// Network identifier. Relatively short (up to 12 chars?) latin letters, digits, dashes
    /// and underscores only.
    pub id: String,

    /// Human-readable network name
    pub title: String,

    /// Human-readable network description text
    pub descr: String,

    /// Network icon file name
    pub icon: String,
}

/// Network statistics data
#[derive(juniper::GraphQLObject)]
pub struct NetStats {
    /// Online counter
    pub online: i32,

    /// Post rate counter
    pub postRate: f64,
}

/// Any post across imageboard
#[derive(Clone, Debug, juniper::GraphQLObject)]
pub struct Post {
    /// Network name post belongs to
    pub net: Option<String>,

    /// Board name post belongs to
    pub board: Option<String>,

    /// Hash of this post.
    ///
    /// Details of how hashing is performed will be discussed later
    pub hash: String,

    /// Post creation date, in seconds since unix epoch.
    ///
    /// Assigned by server.
    pub date: i32,

    /// Post title, will be thisible on the first post in the thread and hidden for all responding
    /// posts.
    /// - Up to 80 symbols long;
    /// - May be empty (`""`, i.e. not contain any symbol)
    pub title: String,

    /// Post text
    /// - Up to 2500 symbols long;
    /// - May not be empty
    pub message: String,

    /// Post images file names
    /// - Max 7 items
    pub images: Vec<String>,

    /// Root parent, i.e. hash of thread's head post.
    ///
    /// Deducted by server based on `parents` value.
    pub root: String,

    /// Hashes of parent posts (i.e. which posts has been mentioned by this).
    /// - Max 7 items;
    /// - 50% + 1 item must share the same `root`. This `root` will be assigned as `root` of this
    /// post.
    pub parents: Vec<String>,

    /// Hashes of children posts (i.e. which posts mentioned this post).
    ///
    /// Calculated by server.
    pub children: Vec<String>,

    /// Marks this post as a hidden
    pub hidden: bool,

    /// Moderator should check this post and perform necessary actions
    pub on_hold: bool,
}

/// An object representing a `Thread`'s data and meta-data.
///
/// Thread is a group of posts shares a same `root` hash and referencing each other.
///
/// Note that thread has no ID: it shares identifier with its head post
#[derive(Clone, Debug, juniper::GraphQLObject)]
pub struct Thread {
    /// Total posts in this thread
    pub replies: i32,

    /// Total unreads (for the current user)
    pub unreads: i32,

    /// Is this thread is added to favorites by current user
    pub isFav: bool,

    /// Is this thread is marked as watched by current user
    pub isWatched: bool,

    /// Head post of this thread
    pub post: Post,
}

/// `Token` struct describes, well, a token - a relatively short string required for the most
/// interactions with server.
///
/// Client can obtain token via [mutation](../mutation/trait.Mutation.html#tymethod.newToken).
#[derive(juniper::GraphQLObject, sea_orm::FromQueryResult)]
pub struct Token {
    /// Token string itself
    pub token: String,

    /// Bit flags, unused for now
    pub flags: i32,

    /// Expiration date
    pub validTill: i32,
}

/// Corresponding user object
#[derive(juniper::GraphQLObject, sea_orm::FromQueryResult)]
pub struct User {
    pub id: i32,
    pub name: String,
    pub is_admin: bool,
}