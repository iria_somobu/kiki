//! The only purpose of current `kiki-server` implementation is to provide grapql API to web client.
//! This crate is here to describe this API.


pub mod mutation;
pub mod query;
pub mod structures;
pub mod subscription;
