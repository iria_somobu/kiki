#![allow(non_snake_case)] // Important: write graphql impl functions in camelCase please
#![allow(unused_variables)] // TODO: remove me later

use std::collections::HashMap;

use juniper::{FieldError, FieldResult};
use num_traits::cast::ToPrimitive;
use sea_orm::{ActiveModelTrait, ColumnTrait, QueryFilter, QuerySelect};
use sea_orm::ActiveValue::Set;
use sea_orm::EntityTrait;
use sha3::{Digest, Sha3_256};

use api::structures;
use api::structures::{MAX_IMGS_LEN, Post, Thread, Token};
use api::structures::{MAX_TITLE_LEN, MAX_CONTENT_LEN, MAX_PARENTS_LEN};

use crate::{Context, ctx};
use crate::err_map;
use crate::impl_query::get_imgs;
use crate::util;

#[derive(Clone, Copy, Debug)]
pub(crate) struct Mutation;

#[juniper::graphql_object(context = ctx::Context)]
impl api::Mutation for Mutation {
    fn newToken() -> Token {
        Token {
            token: "00a1ebc8-8498-4d77-a54b-6cf3611dce46".to_string(),
            flags: 0,
            validTill: 0,
        }
    }

    async fn validateToken(ctx: &ctx::Context, token: String) -> FieldResult<Token> {
        let token = entity::token::Entity::find()
            .filter(entity::token::Column::Token.eq(token))
            .into_model::<entity::token::Model>()
            .one(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?
            .ok_or_else(err_map::token_not_found)?;

        let user = entity::user::Entity::find()
            .filter(entity::user::Column::Id.eq(token.user))
            .into_model::<entity::user::Model>()
            .one(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?
            .ok_or_else(err_map::entity_not_found)?;

        return Ok(Token {
            token: token.token,
            flags: token_flags(&user),
            validTill: (token.expires / 1000).to_i32().expect("WE'ALL GONNA DIE"),
        });
    }

    async fn login(ctx: &ctx::Context, name: String, pwd: String) -> FieldResult<Token> {
        let user = entity::user::Entity::find()
            .filter(entity::user::Column::Id.eq(name))
            .filter(entity::user::Column::Secret.eq(pwd))
            .into_model::<entity::user::Model>()
            .one(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?
            .ok_or_else(err_map::wrong_creds)?;

        let id = uuid::Uuid::new_v4();

        let model = entity::token::ActiveModel {
            token: Set(id.to_string()),
            user: Set(user.id),
            created: Set(0),
            expires: Set(0),
        };

        model.insert(&ctx.db).await.map_err(err_map::generic_db_error)?;

        return Ok(Token {
            token: id.to_string(),
            flags: token_flags(&user),
            validTill: 0,
        });
    }

    async fn postThread(
        ctx: &ctx::Context,
        token: String,
        net: String,
        board: String,
        message: String,
        title: String,
        images: Vec<String>,
    ) -> FieldResult<Thread> {
        if title.len() > MAX_TITLE_LEN { return Err(err_map::content_too_big()); };
        if message.len() > MAX_CONTENT_LEN { return Err(err_map::content_too_big()); };
        if images.len() > MAX_IMGS_LEN { return Err(err_map::content_too_big()); };

        let hash = calc_hash(&message, &title, &Vec::new(), &Vec::new());

        let mut on_hold = should_go_on_hold(ctx, &net, &title).await?;
        on_hold |= should_go_on_hold(ctx, &net, &message).await?;

        let date = util::get_timestamp_ms();
        let model = entity::post::ActiveModel {
            hash: Set(hash.clone()),
            root: Set(hash.clone()),
            network: Set(Some(net.clone())),
            board: Set(Some(board.clone())),
            poster: Set(None),

            date: Set(date),
            title: Set(Some(title.clone())),
            message: Set(message.clone()),

            hidden: Set(on_hold),
            on_hold: Set(on_hold),
        };

        model.insert(&ctx.db).await.map_err(err_map::dupe_post)?;
        insert_imgs(&ctx, images, hash.clone()).await?;

        let thread = Thread {
            replies: 0,
            unreads: 0,
            isFav: false,
            isWatched: false,
            post: Post {
                hash: hash.clone(),
                root: hash.clone(),
                net: Option::from(net),
                board: Option::from(board),
                parents: vec![],
                children: vec![],

                date: convertServerDateToClientDate(date),
                title,
                message,
                images: get_imgs(&ctx, &hash.clone()).await?,

                hidden: on_hold,
                on_hold: on_hold,
            },
        };

        let _ = ctx.thread_tx.send(thread.clone());

        if on_hold {
            return Err(err_map::post_on_hold());
        } else {
            return Ok(thread);
        }
    }

    async fn postPost(
        ctx: &ctx::Context,
        token: String,
        net: String,
        to: Vec<String>,
        message: String,
        images: Vec<String>,
    ) -> FieldResult<Post> {
        if message.len() > MAX_CONTENT_LEN { return Err(err_map::content_too_big()); };
        if to.len() > MAX_PARENTS_LEN { return Err(err_map::content_too_big()); }
        if images.len() > MAX_IMGS_LEN { return Err(err_map::content_too_big()); };

        let hash = calc_hash(&message, &String::new(), &to, &Vec::new());

        let mut counter: HashMap<String, usize> = HashMap::new();
        for parent in &to {
            let post = entity::post::Entity::find_by_id(parent.clone())
                .one(&ctx.db)
                .await
                .map_err(err_map::generic_db_error)?
                .ok_or_else(err_map::entity_not_found)?;

            if counter.contains_key(&post.root) {
                let count = counter.get_mut(&post.root).unwrap();
                *count += 1;
            } else {
                counter.insert(post.root, 1);
            }
        }

        let half_plus_one = to.len() / 2 + 1;
        let valid_root = counter
            .iter()
            .find(|(root, count)| **count >= half_plus_one)
            .ok_or_else(err_map::invalid_root)?;

        let on_hold = should_go_on_hold(ctx, &net, &message).await?;

        let date = util::get_timestamp_ms();
        let model = entity::post::ActiveModel {
            hash: Set(hash.clone()),
            root: Set(valid_root.0.clone()),
            network: Set(None),
            board: Set(None),
            poster: Set(None),

            date: Set(date),
            title: Set(None),
            message: Set(message.clone()),

            hidden: Set(on_hold),
            on_hold: Set(on_hold),
        };

        model.insert(&ctx.db).await.map_err(err_map::dupe_post)?;

        let parents = to
            .iter()
            .map(|parent| entity::post_parents::ActiveModel {
                post: Set(hash.clone()),
                parent: Set(parent.clone()),
            })
            .collect::<Vec<_>>();

        entity::post_parents::Entity::insert_many(parents)
            .exec(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?;

        insert_imgs(&ctx, images, hash.clone()).await?;

        let post = Post {
            hash: hash.clone(),
            root: valid_root.0.clone(),
            net: None,
            board: None,
            parents: to,
            children: vec![],

            date: convertServerDateToClientDate(date),
            title: "".to_string(),
            message,
            images: get_imgs(&ctx, &hash.clone()).await?,

            hidden: on_hold,
            on_hold: on_hold,
        };

        let _ = ctx.post_tx.send(post.clone());

        if on_hold {
            return Err(err_map::post_on_hold());
        } else {
            return Ok(post);
        }
    }

    async fn modPost(
        ctx: &ctx::Context,
        token: String,
        post: String,
        hidden: bool,
        on_hold: bool,
    ) -> FieldResult<String> {
        let is_mod = util::is_admin(&ctx, &token).await;

        if !is_mod {
            return Err(err_map::not_a_mod());
        }

        let model = entity::post::ActiveModel {
            hash: Set(post.clone()),
            hidden: Set(hidden),
            on_hold: Set(on_hold),
            ..Default::default()
        };

        entity::post::Entity::update(model).exec(&ctx.db).await?;

        let np = entity::post::Entity::find_by_id(post.clone())
            .one(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?
            .ok_or_else(err_map::entity_not_found)?;

        let _ = ctx.post_tx.send(Post {
            hash: np.hash,
            root: np.root,
            net: np.network,
            board: np.board,
            parents: vec![],
            children: vec![],

            date: convertServerDateToClientDate(np.date),
            title: np.title.unwrap_or("".to_string()).to_string(),
            message: np.message,
            images: vec![],

            hidden: np.hidden,
            on_hold: np.on_hold,
        });

        return Ok("awoo~".to_string());
    }
}

pub fn convertServerDateToClientDate(date: i64) -> i32 {
    (date / 1000) as i32
}

fn calc_hash(message: &String, title: &String, to: &[String], imgs: &[String]) -> String {
    let mut hasher = Sha3_256::new();

    let s = message.clone() + title.as_str() + to.join("").as_str() + imgs.join("").as_str();
    hasher.update(&s);
    let hash: String = format!("{:X}", hasher.finalize()).to_lowercase();

    hash
}

fn token_flags(user: &entity::user::Model) -> i32 {
    let named_flag = if user.id != 0 {
        structures::FLAG_NAMED
    } else {
        0
    };
    let mod_flag = if user.id != 0 {
        structures::FLAG_MODER
    } else {
        0
    };

    return named_flag | mod_flag;
}

async fn should_go_on_hold(ctx: &Context, net: &String, text: &String) -> Result<bool, FieldError> {
    let l = entity::network::Entity::find()
        .filter(entity::network::Column::Id.eq(net.to_string()))
        .column(entity::network::Column::Stoplist)
        .one(&ctx.db)
        .await
        .map_err(err_map::generic_db_error)?
        .ok_or_else(err_map::entity_not_found)?;

    if l.stoplist.trim().len() == 0 { return Ok(false); }

    for item in l.stoplist.split(";") {
        if text.contains(item) {
            return Ok(true);
        }
    }

    return Ok(false);
}

async fn insert_imgs(ctx: &&Context, images: Vec<String>, hash: String) -> FieldResult<()> {
    if images.len() > 0 {
        let post_images = images
            .iter()
            .map(|img| entity::post_images::ActiveModel {
                post: Set(hash.clone()),
                img: Set(img.clone()),
            })
            .collect::<Vec<_>>();

        entity::post_images::Entity::insert_many(post_images)
            .exec(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?;
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn stupid_hashing() {
        let mut hash = calc_hash(
            &"".to_string(),
            &"".to_string(),
            &vec!["".to_string(), "".to_string()],
            &vec!["".to_string(), "".to_string()],
        );
        assert_eq!(
            hash,
            "a7ffc6f8bf1ed76651c14756a061d662f580ff4de43b49fa82d80a4b80f8434a"
        );

        hash = calc_hash(
            &"awoo".to_string(),
            &"awoo".to_string(),
            &vec!["awoo".to_string(), "awoo".to_string()],
            &vec!["awoo".to_string(), "awoo".to_string()],
        );
        assert_eq!(
            hash,
            "ab0d895582c19463347e7dfdb62a27d0bf9c238fe5aa536f32dbdc3ac3a6a8a5"
        );
    }
}
