#![allow(non_snake_case)] // Important: write graphql impl functions in camelCase please
#![allow(unused_variables)] // TODO: remove me later

use std::collections::HashSet;

use juniper::{FieldError, FieldResult};
use sea_orm::{ConnectionTrait};
use sea_orm::{ColumnTrait, DbConn, EntityTrait, QueryFilter, QueryOrder};
use sea_orm::{Condition, DbBackend, Order, QueryResult, Statement};
use num_traits::ToPrimitive;
use api::structures::{Board, BoardGroup, Net, NetStats, Post, Thread, User};

use super::impl_mutation::convertServerDateToClientDate;

use crate::{Context, ctx};
use crate::err_map;
use crate::util;

#[derive(Clone, Copy, Debug)]
pub(crate) struct Query;

#[juniper::graphql_object(context = ctx::Context)]
impl api::Query for Query {
    async fn networks(ctx: &ctx::Context, token: String) -> FieldResult<Vec<Net>> {
        let mut a: Vec<Net> = entity::network::Entity::find()
            .into_model::<Net>()
            .all(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?;

        let mut i = 0;
        while i < a.len() {
            a[i].icon = get_img(ctx, &a[i].icon).await?;
            i += 1;
        }

        Ok(a)
    }

    async fn network(ctx: &ctx::Context, token: String, net: String) -> FieldResult<Net> {
        let mut a = entity::network::Entity::find()
            .filter(entity::network::Column::Id.eq(net.clone()))
            .into_model::<Net>()
            .one(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?
            .ok_or_else(err_map::entity_not_found)?;

        a.icon = get_img(ctx, &a.icon).await?;

        Ok(a)
    }

    async fn extendedNetworkInfo(ctx: &ctx::Context, token: String, net: String) -> FieldResult<String> {
        let l = entity::network::Entity::find()
            .filter(entity::network::Column::Id.eq(net.to_string()))
            .one(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?
            .ok_or_else(err_map::entity_not_found)?;

        return Ok(l.ext_info.to_string());
    }

    async fn boardGroups(ctx: &ctx::Context, token: String, net: String) -> FieldResult<Vec<BoardGroup>> {
        let all_boards = entity::board::Entity::find()
            .from_raw_sql(getBoardStmt(net, "*"))
            .into_model::<Board>()
            .all(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?;

        let group_names: HashSet<&String> = all_boards.iter().map(|board| &board.group).collect();

        let mut board_groups: Vec<BoardGroup> = group_names
            .into_iter()
            .map(|group_name| {
                let boards: Vec<Board> = all_boards
                    .iter()
                    .filter(|board| board.group.eq(group_name))
                    .map(|model| (*model).clone())
                    .collect();

                let min_board = boards.iter().min_by_key(|board| board.position).unwrap();

                BoardGroup {
                    name: group_name.clone(),
                    position: min_board.position,
                    boards,
                }
            })
            .collect();

        board_groups.sort_by_key(|group| group.position);

        Ok(board_groups)
    }

    async fn board(
        ctx: &ctx::Context,
        token: String,
        net: String,
        board: String,
    ) -> FieldResult<Board> {
        let a = entity::board::Entity::find()
            .from_raw_sql(getBoardStmt(net, &board))
            .into_model::<Board>()
            .one(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?
            .ok_or_else(err_map::entity_not_found)?;
        Ok(a)
    }

    async fn threads(
        ctx: &ctx::Context,
        token: String,
        net: String,
        board: String,
        badgeTime: i32,
    ) -> FieldResult<Vec<Thread>> {
        let is_mod = util::is_admin(&ctx, &token).await;

        let mut filter = Condition::all()
            .add(entity::post::Column::Network.eq(net.clone()))
            .add(entity::post::Column::Board.eq(board.clone()));

        if !is_mod { filter = filter.add(entity::post::Column::Hidden.eq(false)); }

        let start_posts: Vec<entity::post::Model> = entity::post::Entity::find()
            .filter(filter)
            .order_by(entity::post::Column::Date, Order::Desc)
            .all(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?;

        let mut threads: Vec<Thread> = Vec::new();
        for start_post in start_posts {
            let (replies, children, unread) = get_thread_info(ctx, start_post.root.clone(), badgeTime).await?;
            let images: Vec<String> = get_imgs(ctx, &start_post.root.clone().into()).await?;

            threads.push(Thread {
                replies: (replies - 1) as i32,
                unreads: unread,
                isFav: false,
                isWatched: false,
                post: Post {
                    net: Option::from(net.to_string()),
                    board: Option::from(board.to_string()),
                    hash: start_post.hash,
                    date: convertServerDateToClientDate(start_post.date),
                    title: start_post.title.unwrap_or_default(),
                    message: start_post.message,
                    images,
                    root: start_post.root,
                    parents: vec![],
                    children: children,
                    hidden: start_post.hidden,
                    on_hold: start_post.on_hold,
                },
            });
        }

        Ok(threads)
    }

    async fn posts(ctx: &ctx::Context, token: String, thread: String) -> FieldResult<Vec<Post>> {
        let is_mod = util::is_admin(&ctx, &token).await;

        let mut filter = Condition::all().add(entity::post::Column::Root.eq(thread));
        if !is_mod { filter = filter.add(entity::post::Column::Hidden.eq(false)); }

        let posts = entity::post::Entity::find()
            .filter(filter)
            .all(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?;

        let mut results = vec![];
        for model in posts {
            let (children, parents) = get_children_and_parents(&ctx.db, &model.hash, is_mod).await;
            let images: Vec<String> = get_imgs(ctx, &model.hash).await?;

            results.push(Post {
                net: None,
                board: None,
                hash: model.hash,
                date: convertServerDateToClientDate(model.date),
                title: model.title.unwrap_or_default(),
                message: model.message,
                images,
                root: model.root,
                parents,
                children,
                hidden: model.hidden,
                on_hold: model.on_hold,
            });
        }

        Ok(results)
    }

    async fn post(ctx: &ctx::Context, token: String, hash: String) -> FieldResult<Post> {
        let is_mod = util::is_admin(&ctx, &token).await;

        let mut filter = Condition::all().add(entity::post::Column::Hash.eq(hash.clone()));
        if !is_mod { filter = filter.add(entity::post::Column::Hidden.eq(false)); }

        let np = entity::post::Entity::find()
            .filter(filter)
            .one(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?
            .ok_or_else(err_map::entity_not_found)?;

        let images: Vec<String> = get_imgs(ctx, &hash).await?;
        let (children, parents) = get_children_and_parents(&ctx.db, &hash, is_mod).await;

        Ok(Post {
            hash: np.hash,
            root: np.root,
            net: np.network,
            board: np.board,
            parents,
            children,

            date: convertServerDateToClientDate(np.date),
            title: np.title.unwrap_or("".to_string()).to_string(),
            message: np.message,
            images: images,

            hidden: np.hidden,
            on_hold: np.on_hold,
        })
    }

    async fn netStats(ctx: &ctx::Context, token: String) -> FieldResult<NetStats> {
        let date = util::get_timestamp_ms() - 10 * 60 * 1000;
        let query = ctx.db.query_one(Statement::from_sql_and_values(
            DbBackend::Sqlite,
            r#"
                SELECT COUNT(hash) AS posts
                FROM post
                WHERE post.date > $1
                "#,
            vec![date.into()],
        ))
            .await
            .map_err(err_map::generic_db_error)?
            .ok_or_else(err_map::entity_not_found)?;

        let posts: f64 = query.try_get::<i64>("", "posts").unwrap().to_f64().unwrap() / 10.0;

        Ok(NetStats {
            online: 0,
            postRate: posts,
        })
    }

    async fn me(ctx: &ctx::Context, token: String) -> FieldResult<User> {
        let user = entity::user::Entity::find()
            .from_raw_sql(Statement::from_sql_and_values(
                DbBackend::Sqlite,
                r#"
                SELECT id, name, is_admin FROM user
                INNER JOIN token ON user.id = token.user
                WHERE token.token = $1
                "#,
                vec![token.into()],
            ))
            .into_model::<User>()
            .one(&ctx.db)
            .await
            .map_err(err_map::generic_db_error)?
            .ok_or_else(err_map::entity_not_found)?;

        return Ok(user);
    }
}

fn split_column_data(query: &QueryResult, column_name: &str) -> Vec<String> {
    match query.try_get::<String>("", column_name) {
        Ok(rz) => rz.split(",").map(|s| s.to_string()).collect(),
        Err(_) => {
            vec![]
        }
    }
}

async fn get_children_and_parents(db: &DbConn, hash: &str, with_hidden: bool) -> (Vec<String>, Vec<String>) {
    let query = db
        .query_one(Statement::from_sql_and_values(
            DbBackend::Sqlite,
            format!(r#"
                SELECT (
                    SELECT GROUP_CONCAT(post_parents.parent)
                    FROM post_parents
                    INNER JOIN post ON post_parents.parent = post.hash
                    WHERE post_parents.post=$1 {a}
                    GROUP BY post_parents.post
                ) AS parents,
                (
                    SELECT GROUP_CONCAT(post_parents.post)
                    FROM post_parents
                    INNER JOIN post ON post_parents.post = post.hash
                    WHERE post_parents.parent=$1 {a}
                    GROUP BY post_parents.parent
                ) AS children
                "#, a = if with_hidden { "" } else { "AND post.hidden=false" }
            ).as_str(),
            vec![hash.into()],
        ))
        .await
        .expect("Database error!");

    match query {
        Some(bq) => (
            split_column_data(&bq, "children"),
            split_column_data(&bq, "parents"),
        ),
        None => (vec![], vec![]),
    }
}

async fn get_img(ctx: &Context, img_hash: &String) -> Result<String, FieldError> {
    let a = ctx.db.query_one(Statement::from_sql_and_values(
        DbBackend::Sqlite,
        r#"
                SELECT image.img || '.' || image.ext AS awoo
                FROM image
                WHERE image.img=$1
            "#,
        vec![img_hash.clone().into()],
    ))
        .await
        .map_err(err_map::generic_db_error)?
        .ok_or_else(err_map::entity_not_found)?;

    // Somobu: not sure if I should put it this way
    let rz = a.try_get::<String>("", "awoo").map_err(err_map::generic_db_error)?;
    return Ok(rz);
}

pub async fn get_imgs(ctx: &Context, hash: &String) -> Result<Vec<String>, FieldError> {
    let a = ctx.db
        .query_all(Statement::from_sql_and_values(
            DbBackend::Sqlite,
            r#"
                SELECT image.img || '.' || image.ext AS awoo
                FROM post_images
                INNER JOIN image ON post_images.img = image.img
                WHERE post_images.post=$1
            "#,
            vec![hash.clone().into()],
        ))
        .await
        .map_err(err_map::generic_db_error)?;

    let b = a.iter().map(|q: &QueryResult| {
        q.try_get::<String>("", "awoo").unwrap()
    }).collect();

    return Ok(b);
}

async fn get_thread_info(ctx: &Context, hash: String, unreadTime: i32) -> Result<(i64, Vec<String>, i32), FieldError> {
    let unreadMillis: i64 = (unreadTime as i64) * 1000;
    let query = ctx
        .db
        .query_one(Statement::from_sql_and_values(
            DbBackend::Sqlite,
            r#"
                SELECT
                (
                    SELECT COUNT(*)
                    FROM post
                    WHERE post.root=$1
                ) AS replies,
                (
                    SELECT GROUP_CONCAT(post)
                    FROM post_parents
                    WHERE parent=$1
                ) AS children,
                (
                    SELECT COUNT(*)
                    FROM post
                    WHERE post.root = $1 AND post.date > $2
                ) AS unread
            "#,
            vec![hash.into(), unreadMillis.into()],
        ))
        .await
        .map_err(err_map::generic_db_error)?
        .ok_or_else(err_map::entity_not_found)?;

    let replies: i64 = query.try_get("", "replies").unwrap();
    let unread: i32 = query.try_get("", "unread").unwrap();
    let children = split_column_data(&query, "children");

    return Ok((replies, children, unread));
}

fn getBoardStmt(net: String, name: &str) -> Statement{
    let query = format!(r#"
        SELECT name, descr, position, "group", mdate AS lastUpdateTime
        FROM board
        LEFT JOIN (
            SELECT mboard, MAX(p2.date)/1000 AS mdate
            FROM (
                SELECT board AS mboard, hash AS mhash
                FROM post
                WHERE hash = root
            )
            INNER JOIN post AS p2 ON p2.root = mhash
            GROUP BY mboard
        ) AS post ON mboard = board.name
        WHERE network = "main-ru" AND name = {}
        ORDER BY position ASC
        "#, if name.eq("*") { "name" } else {"$2"}
    );

    return Statement::from_sql_and_values(
        DbBackend::Sqlite,
        &query,
        vec![net.into(), name.into()],
    );
}