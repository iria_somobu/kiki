use std::time::SystemTime;
use sea_orm::ConnectionTrait;
use sea_orm::{DbBackend, Statement};

use crate::ctx;

pub async fn is_admin(ctx: &ctx::Context, token: &String) -> bool {
    let query = ctx.db.query_one(Statement::from_sql_and_values(
            DbBackend::Sqlite,
            r#"
                SELECT user.is_admin AS admin
                FROM token
                INNER JOIN user ON user.id = token.user
                WHERE token.token=$1
                "#,
            vec![token.clone().into()],
        ))
        .await
        .expect("Database error!");

    if query.is_none() {
        return false;
    } else {
        let is_admin: bool = query.unwrap().try_get("", "admin").unwrap();
        return is_admin;
    }
}

pub fn get_timestamp_ms() -> i64 {
    SystemTime::now()
        .duration_since(std::time::UNIX_EPOCH)
        .unwrap()
        .as_millis() as i64
}
