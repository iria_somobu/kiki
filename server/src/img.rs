use bytes::BufMut;
use futures::TryStreamExt;
use sea_orm::ActiveModelTrait;
use sea_orm::DbConn;
use uuid::Uuid;
use warp::{Rejection, Reply};
use warp::multipart::{FormData, Part};

use crate::config::Cfg;

pub async fn upload(config: Cfg, db: DbConn, form: FormData) -> Result<impl Reply, Rejection> {
    let parts: Vec<Part> = form.try_collect().await.map_err(|e| {
        eprintln!("form error: {}", e);
        warp::reject::reject()
    })?;

    let mut id: String = "".to_string();

    for p in parts {
        if p.name() == "file" {
            let content_type = p.content_type();
            let ext;
            match content_type {
                Some(file_type) => match file_type {
                    "image/png" => { ext = "png"; }
                    "image/jpg" => { ext = "jpeg"; }
                    "image/jpeg" => { ext = "jpeg"; }
                    "image/gif" => { ext = "gif"; }
                    "image/webp" => { ext = "webp"; }
                    v => {
                        eprintln!("invalid file type found: {}", v);
                        return Err(warp::reject::reject());
                    }
                },
                None => {
                    eprintln!("file type could not be determined");
                    return Err(warp::reject::reject());
                }
            }

            let value = p
                .stream()
                .try_fold(Vec::new(), |mut vec, data| {
                    vec.put(data);
                    async move { Ok(vec) }
                })
                .await
                .map_err(|e| {
                    eprintln!("reading file error: {}", e);
                    warp::reject::reject()
                })?;

            id = Uuid::new_v4().to_string();

            // Write on disk
            let file_name = format!("{}/{}.{}", config.img_dir, id, ext);
            tokio::fs::write(&file_name, value).await.map_err(|e| {
                eprint!("error writing file: {}", e);
                warp::reject::reject()
            })?;

            let model = entity::image::Model { img: id.clone(), ext: ext.to_string().clone() };
            let am: entity::image::ActiveModel = model.into();
            let _: entity::image::Model = am.insert(&db.clone()).await.map_err(|e| {
                eprintln!("db error: {}", e);
                warp::reject::reject()
            })?;
        }
    }

    Ok(id)
}