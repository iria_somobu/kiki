#![allow(non_snake_case)]

use api::structures::{Post, Thread};
use juniper::graphql_subscription;

use api::subscription::{PostsStream, ThreadsStream};

use crate::util;

use super::ctx::Context;

pub struct Subscription;

#[graphql_subscription(context = Context)]
impl api::Subscription for Subscription {
    /// All new threads
    async fn thread(ctx: &Context, token: String, net: String, board: String) -> ThreadsStream {
        let is_mod = util::is_admin(&ctx, &token).await;
        let mut rx = ctx.thread_tx.subscribe();

        let stream = async_stream::stream! {
            loop {
                let thread: Thread = rx.recv().await.unwrap();
                let post: Post = thread.post.clone();

                if !is_mod && (post.hidden || post.on_hold) { continue; }

                if post.root == post.hash {
                    // We can do unwrap here, because root post always has net and board fields
                    let post_net = post.net.as_ref().unwrap();
                    let post_board = post.board.as_ref().unwrap();

                    if post_net == &net && post_board == &board {
                        yield thread;
                    }
                }

            }
        };
        return Box::pin(stream);
    }

    /// All new posts (new thread posts included)
    async fn post(ctx: &Context, token: String, rootHash: String) -> PostsStream {
        let is_mod = util::is_admin(&ctx, &token).await;

        let mut rx = ctx.post_tx.subscribe();
        let stream = async_stream::stream! {
            loop {
                let post = rx.recv().await.unwrap();

                let mod_filter = is_mod | !post.hidden;
                let parent_filter = post.root == rootHash;

                if (mod_filter & parent_filter) {
                    yield post;
                }
            }
        };
        return Box::pin(stream);
    }
}
