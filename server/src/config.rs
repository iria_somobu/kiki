use path_absolutize::Absolutize;
use std::net::SocketAddr;
use std::path::Path;

#[derive(Clone)]
pub struct Cfg {
    pub database_path: String,
    pub database_uri: String,
    pub server_address: SocketAddr,

    pub img_dir: String,

    pub production: bool,
    pub board_page: String,
    pub static_dir: String,
}

pub fn read() -> Cfg {
    // Lets read our config file
    let settings = config::Config::builder()
        .add_source(config::File::with_name("config.yaml"))
        .build()
        .unwrap();

    // Parse server address
    let server_address_str = settings.get_string("address").unwrap();
    let server_address: SocketAddr = server_address_str
        .parse()
        .expect("Unable to parse socket address");

    // Parse db path and convert it to absulute
    let database_name = settings
        .get_string("database")
        .expect("'database' variable must be set!");
    let db_p = Path::new(&database_name);
    let database_path = db_p.absolutize().unwrap().to_str().unwrap().to_string();
    let database_uri = ["sqlite://", &database_path, "?mode=rwc"].join("");

    // Image upload/download dir
    let img_dir = settings.get_string("img_dir").unwrap();

    // "Production" switch
    let production = settings.get_bool("production").unwrap();

    let board_page: String;
    let static_dir: String;

    if !production {
        board_page = settings.get_string("board_page").unwrap();
        static_dir = settings.get_string("static_dir").unwrap();
    } else {
        board_page = "".to_string();
        static_dir = "".to_string();
    }

    return Cfg {
        database_path,
        database_uri,
        server_address,
        img_dir,
        production,
        board_page,
        static_dir,
    };
}
