use sea_orm::DbConn;
use tokio::sync::broadcast::Sender;

use api::structures::{Post, Thread};

#[derive(Debug)]
pub struct Context {
    pub db: DbConn,

    // Thread updates
    pub thread_tx: Sender<Thread>,

    // Post updates
    pub post_tx: Sender<Post>,
}

impl juniper::Context for Context {}
