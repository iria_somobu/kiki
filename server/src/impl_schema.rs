#![allow(non_snake_case)] // Important: write graphql impl functions in camelCase please
#![allow(unused_variables)] // TODO: remove me later

use juniper::RootNode;

use crate::impl_mutation::Mutation;
use crate::impl_query::Query;
use crate::impl_subscription::Subscription;

type Schema = RootNode<'static, Query, Mutation, Subscription>;

pub(crate) fn schema() -> Schema {
    Schema::new(Query, Mutation, Subscription)
}
