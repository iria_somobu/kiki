use juniper::{FieldError, graphql_value};
use sea_orm::DbErr;

use api::structures::{ERR_AUTH_NOT_A_MOD, ERR_AUTH_WRONG_CREDS, ERR_DB_FAIL, ERR_NOT_FOUND};
use api::structures::{ERR_POST_BAD_ROOT, ERR_POST_DUPE, ERR_POST_ON_HOLD, ERR_POST_TOO_BIG};

pub fn is_primary_key_error(err: &DbErr) -> bool {
    let err_str = err.to_string();
    err_str.contains("code: 1555")
}

pub fn token_not_found() -> FieldError {
    FieldError::new(
        format!("Token not found"),
        graphql_value!({"type": ERR_NOT_FOUND}),
    )
}

pub fn entity_not_found() -> FieldError {
    FieldError::new(
        format!("Entity not found"),
        graphql_value!({"type": ERR_NOT_FOUND}),
    )
}

pub fn wrong_creds() -> FieldError {
    FieldError::new(
        format!("Wrong credentials"),
        graphql_value!({"type": ERR_AUTH_WRONG_CREDS}),
    )
}

pub fn not_a_mod() -> FieldError {
    FieldError::new(
        format!("Mod permissions required"),
        graphql_value!({"type": ERR_AUTH_NOT_A_MOD}),
    )
}

pub fn invalid_root() -> FieldError {
    FieldError::new(
        format!("Invalid post root"),
        graphql_value!({"type": ERR_POST_BAD_ROOT}),
    )
}

pub fn content_too_big() -> FieldError {
    FieldError::new(
        format!("Content too big"),
        graphql_value!({"type": ERR_POST_TOO_BIG}),
    )
}

pub fn dupe_post(err: DbErr) -> FieldError {
    if is_primary_key_error(&err) {
        FieldError::new(
            "Duplicate post".to_string(),
            graphql_value!({"type": ERR_POST_DUPE}),
        )
    } else {
        generic_db_error(err)
    }
}

pub fn post_on_hold() -> FieldError {
    FieldError::new(
        format!("Post on hold"),
        graphql_value!({"type": ERR_POST_ON_HOLD}),
    )
}

pub fn generic_db_error(err: DbErr) -> FieldError {
    // panic!();
    eprintln!("{}", err);
    FieldError::new(
        "Database error".to_string(),
        graphql_value!({ "type": ERR_DB_FAIL }),
    )
}
