use std::sync::Arc;

use juniper_graphql_ws::ConnectionConfig;
use juniper_warp::{playground_filter, subscriptions::serve_graphql_ws};
use migration::{Migrator, MigratorTrait};
pub use sea_orm::{Database, DbConn, query::*};
use tokio::sync::broadcast;
use warp::Filter;
use warp::multipart::FormData;

use api::structures::{Post, Thread};
use ctx::Context;

mod config;
mod ctx;
mod err_map;
mod img;
mod impl_mutation;
mod impl_query;
mod impl_schema;
mod impl_subscription;
mod util;

#[tokio::main]
async fn main() {
    let config: config::Cfg = config::read();

    if cfg!(debug_assertions) {
        println!("Dude I'm deleting kitsuki.db rn");
        std::fs::remove_file(config.database_path.clone()).ok();
    }

    // Set up database
    let db: DbConn = Database::connect(config.database_uri.clone())
        .await
        .expect("Couldn't connect to database!");

    Migrator::up(&db, None)
        .await
        .expect("Couldn't run database migrations!");

    // New posts queue (needed by subscription api)
    let (post_tx, _) = broadcast::channel::<Post>(64);
    let (thread_tx, _) = broadcast::channel::<Thread>(64);

    // GraphQL api filter
    let db_clone = db.clone();
    let post_c1 = post_tx.clone();
    let thread_c1 = thread_tx.clone();
    let state = warp::any().map(move || Context {
        db: db_clone.clone(),
        post_tx: post_c1.clone(),
        thread_tx: thread_c1.clone(),
    });
    let graphql_filter = juniper_warp::make_graphql_filter(impl_schema::schema(), state.boxed());

    // GraphQL subscriptions stuff
    let db_clone = db.clone();
    let post_c2 = post_tx.clone();
    let thread_c2 = thread_tx.clone();
    let root_node = Arc::new(impl_schema::schema());

    let websocket_handler = move |ws: warp::ws::Ws| {
        let root_node = root_node.clone();
        let c = db_clone.clone();
        let post_c3 = post_c2.clone();
        let thread_c3 = thread_c2.clone();
        ws.on_upgrade(move |websocket| async move {
            let a = serve_graphql_ws(
                websocket,
                root_node,
                ConnectionConfig::new(Context {
                    db: c,
                    post_tx: post_c3.clone(),
                    thread_tx: thread_c3.clone(),
                }),
            ).await;

            let _ = a.map_err(|r| {
                if r.to_string().contains("Connection reset without closing handshake") {
                    return;
                }

                eprintln!("Websocket serde error! {r:?}");
            });

            return ();
        })
    };

    // File upload route
    let cfg_clone = config.clone();
    let db_c2 = db.clone();
    let upload_route = warp::path("upload")
        .and(warp::post())
        .and(warp::multipart::form().max_length(5_000_000))
        .and_then(move |form: FormData| { img::upload(cfg_clone.clone(), db_c2.clone(), form) });

    // Production switch
    if config.production {
        let set_handshake_header = |reply| {
            return warp::reply::with_header(reply, "Sec-WebSocket-Protocol", "graphql-ws");
        };

        warp::serve(
            warp::get()
                // Just to make warp work
                .and(warp::path::end().map(|| ""))
                // GraphQL api endpoint
                .or(warp::path("api").and(graphql_filter))
                // GraphQL subscriptions over websockets
                .or(warp::path("subscriptions")
                    .and(warp::ws())
                    .map(websocket_handler))
                // Img api endpoint (upload only)
                .or(upload_route)
                .map(set_handshake_header),
        )
            .run(config.server_address)
            .await
    } else {
        // This is fucking CORS headers
        let cors = warp::cors()
            .allow_any_origin()
            .allow_headers(vec![
                "User-Agent",
                "Sec-Fetch-Mode",
                "Referer",
                "Origin",
                "Content-Type",
                "Access-Control-Request-Method",
                "Access-Control-Request-Headers",
            ])
            .allow_methods(vec!["POST", "GET"]);

        let set_handshake_header = |reply| {
            return warp::reply::with_header(reply, "Sec-WebSocket-Protocol", "graphql-ws");
        };

        warp::serve(
            warp::get()
                // Board page
                .and(warp::path::end().and(warp::fs::file(config.board_page.clone())))
                .or(warp::path("index").and(warp::fs::file(config.board_page.clone())))
                .or(warp::path("index.html").and(warp::fs::file(config.board_page.clone())))
                // GraphQL api endpoint
                .or(warp::path("api").and(graphql_filter))
                // Img api endpoint
                .or(upload_route)
                .or(warp::path("img").and(warp::fs::dir(config.img_dir.clone())))
                // Static files
                .or(warp::path("static").and(warp::fs::dir(config.static_dir.clone())))
                // GraphQL subscriptions over websockets
                .or(warp::path("subscriptions")
                    .and(warp::ws())
                    .map(websocket_handler))
                .map(set_handshake_header)
                // Playground
                .or(warp::path("playground").and(playground_filter("/api", Some("/subscriptions"))))
                .with(cors),
        )
            .run(config.server_address)
            .await
    }
}
