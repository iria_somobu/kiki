use std::fs::File;
use std::io::{BufRead, BufReader};

use config::Config;

#[tokio::main]
async fn main() {
    // Lets read our config file
    let settings = Config::builder()
        .add_source(config::File::with_name("notifier-config.yaml"))
        .build()
        .unwrap();

    let logfile = settings.get_string("log_pipe").unwrap();
    let webhook = settings.get_string("notifier_webhook").expect("Set `notifier_webhook` in `config.yaml` plz");

    // Setup log reader
    let file = File::open(logfile).unwrap();
    let reader = BufReader::new(file);

    for line in reader.lines() {
        let better_line = line.unwrap();

        let client = reqwest::Client::new();
        let res = client.post(&webhook)
            .header("Content-Type", "application/json")
            .body(["{\"content\":\"", &better_line, "\"}"].join(""))
            .send().await
            .unwrap();

        if !&res.status().is_success() {
            println!("I've failed you");
            println!("{:?}", res.text().await.unwrap())
        }
    }
}