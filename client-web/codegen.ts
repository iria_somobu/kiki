import type { CodegenConfig } from "@graphql-codegen/cli";

const config: CodegenConfig = {
  schema: "http://127.0.0.1:8080/api",
  documents: ["src/**/*.tsx", "src/**/*.ts"],
  generates: {
    "./src/gql/generated/": {
      preset: "client",
      plugins: [],
    },
  },
};
export default config;
