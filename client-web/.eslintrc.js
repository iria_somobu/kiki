const isProd = process.env.NODE_ENV === "production";

module.exports = {
  extends: ["react-app", "plugin:@typescript-eslint/recommended"],
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint"],
  rules: {
    camelcase: ["error", {}],
    "prefer-const": ["error", {}],
    "no-unused-vars": [isProd ? "error" : "warn", {}],
  },
  ignorePatterns: ["src/gql"],
  overrides: [
    {
      files: ["**/*.ts?(x)"],
      rules: {
        "@typescript-eslint/naming-convention": [
          "error",
          {
            selector: "typeAlias",
            format: ["PascalCase"],
            prefix: ["T"],
          },
        ],
      },
    },
  ],
};
