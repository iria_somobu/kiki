import {
  OperationVariables,
  QueryHookOptions,
  QueryResult,
  TypedDocumentNode,
  useQuery
} from "@apollo/client";
import { DocumentNode } from "graphql";
import { useEffect, useState } from "react";

import {
  TypedUseSelectorHook,
  useDispatch as defaultUseDispatch,
  useSelector as defaultUseSelector
} from "react-redux";
import { useParams } from "react-router-dom";
import { store, TRootState } from "store/indexStore";

type TResult<TData, TVariables> = QueryResult<TData, TVariables> & {
  data: TData;
};

/***
 * We use this hook to guarantee that data field always contains some value
 ***/
// eslint-disable-next-line @typescript-eslint/no-explicit-any 
export const useQueryWithoutUndefined = <TData = any,
  TVariables = OperationVariables>(
  query: DocumentNode | TypedDocumentNode<TData, TVariables>,
  options?: QueryHookOptions<TData, TVariables>
) => {
  const result = useQuery(query, options);
  if (result.data && !Object.keys(result.data).length) {
    delete result.data;
  }
  return result as TResult<TData, TVariables>;
};

export const useSelector: TypedUseSelectorHook<TRootState> = defaultUseSelector;

export type TAppDispatch = typeof store.dispatch;
export const useDispatch: () => TAppDispatch = defaultUseDispatch;

/***
 * I am not sure if this hooks is a good solution...
 * Its logic depends on TokenValidator's logic which now 
 * blocks our app until client gets token
 * 
 ***/
export const useToken = () => {
  const token = defaultUseSelector((state: TRootState) => state.token.token)
  if (token === undefined) {
    throw new Error("Token cannot be undefined!")
  }

  return token;
};

/***
 * This hook takes URL parameters as arguments
 * and makes compile-time check that they cannot be undefined
 *
 * Example:
 *  You want to get "net" and "board" params from URL
 *
 *  const { net, board } = useParamsWithoutUndefined();
 *
 *  This is the same as
 *
 *  const { net, board } = useParams(); // From react-router-dom
 ***/
export const useParamsWithoutUndefined = (): {
  [key: string]: string;
} => {
  return useParams() as { [key: string]: string };
};

export const useWindowSizeSubscribe = () => {
  const [size, setSize] = useState([
    window.innerWidth,
    window.innerHeight
  ]);

  useEffect(() => {
    const callback = () => {
      setSize([
        window.innerWidth, 
        window.innerHeight,
      ]);
    }

    window.addEventListener('resize', callback);

    return () => {
      window.removeEventListener('resize', callback);
    }
  }, []);

  return size;
}