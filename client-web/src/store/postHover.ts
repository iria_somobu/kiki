import { PayloadAction, createSlice } from "@reduxjs/toolkit";

type TPos = {
  x: number;
  y: number;
};

type TPostParent = {
  parent: number | null;
};

type TPostSizeLimits = {
  maxWidth: number | null;
  maxHeight: number | null;
};

type TPostHash = {
  hash: string;
};

type TPostIdentity = TPostParent &
  TPostHash & {
    id: number;
    preferTop: boolean; // Prefer to place post hover above target
    isLoaded: boolean;
    triggerRect: {
      x: number;
      y: number;
      height: number;
      width: number;
    };
  };

export type TPostEntry = TPos & TPostIdentity & TPostSizeLimits;

export type TState = {
  posts: TPostEntry[];
};

const initialState: TState = {
  posts: [],
};

type TAddPostPayload = PayloadAction<TPostEntry>;

type THidePostPayload = PayloadAction<TPostHash>;

type TUpdateAllPayload = PayloadAction<{
  posts: TPostEntry[];
}>;

export const postHoverSlice = createSlice({
  name: "postHover",
  initialState,
  reducers: {
    add: (state, { payload }: TAddPostPayload) => {
      const post = state.posts.find((post) => post.hash === payload.hash);
      if (post === undefined) {
        state.posts.push({
          ...payload,
        });
      } else {
        const newPosts = state.posts.filter(
          (post) => post.hash !== payload.hash
        );
        newPosts.push({
          ...post,
          ...payload,
        });
        state.posts = newPosts;
      }
    },
    updateAll: (state, { payload }: TUpdateAllPayload) => {
      state.posts = payload.posts;
    },
    redrawAll: (state) => {
      state.posts = [...state.posts];
    },
    removePost: (state, action: THidePostPayload) => {
      state.posts = state.posts.filter(
        (post) => post.hash !== action.payload.hash
      );
    },
    oneLoaded: (state, { payload: id }: PayloadAction<number>) => {
      const index = state.posts.findIndex((p) => p.id === id);
      if (index !== -1) {
        state.posts[index].isLoaded = true;
      }
    },
    hideAll: (state) => {
      state.posts = [];
    },
  },
});
