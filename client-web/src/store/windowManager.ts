import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { TPresetName } from "App/WindowManager/FloatingWindow";
import { TLoginWindow } from "windows/LoginWindow/logic";
import { TNetInfoWindow } from "windows/NetInfoWindow/logic";
import { TNewPostWindow } from "windows/NewPostWindow/logic";
import { TNewThreadWindow } from "windows/NewThreadWindow/logic";

export type TWindowType = "newPost" | "newThread" | "netInfo" | "login";

export type TWindow =
  | TNewPostWindow
  | TNewThreadWindow
  | TNetInfoWindow
  | TLoginWindow;

export type TWindowId = number;

type TWindowInitialPos = {
  initialX?: number;
  initialY?: number;
};

export type TWindowBase<
  TWinType extends TWindowType,
  TData = null
> = TWindowInitialPos & {
  type: TWinType;
  id: TWindowId;
  title?: string;
  presetName?: TPresetName;
  data: TData;
};

export type TState = {
  windows: TWindow[];
  highlighted: TWindowId | null;
};

export type TOpenWindowBasePayload<T = null> = TWindowInitialPos & {
  data: T;
};

type TOpenPayload = PayloadAction<TWindow>;

type TFocusAndHighlightPayload = PayloadAction<{
  id: TWindowId;
}>;

type TClosePayload = PayloadAction<{
  id: TWindowId;
}>;

type TFocusPayload = PayloadAction<{
  id: TWindowId;
}>;

const initialState: TState = {
  windows: [],
  highlighted: null,
};

let counter = 0;

const focusOnWindow = (windows: TWindow[], id: TWindowId) => {
  if (windows.length > 0) {
    const focusedWindow = windows[windows.length - 1];
    if (focusedWindow.id === id) {
      return windows;
    }

    const unfocusedWindows = windows.filter((window) => window.id !== id);
    const windowToFocus = windows.find((window) => window.id === id);
    return windowToFocus ? [...unfocusedWindows, windowToFocus] : windows;
  }
  return windows;
};

export const wmSlice = createSlice({
  name: "windowManager",
  initialState,
  reducers: {
    close: (state, { payload }: TClosePayload) => {
      state.windows = state.windows.filter(
        (window) => window.id !== payload.id
      );
    },
    focusWindow: (state, { payload }: TFocusPayload) => {
      state.windows = focusOnWindow(state.windows, payload.id);
    },
    openNewWindow: (state, { payload }: TOpenPayload) => {
      state.windows.push(payload);
    },
    setWindowFileds: (state, { payload }: PayloadAction<TWindow>) => {
      const windowIndex = state.windows.findIndex(
        (window) => window.id === payload.id
      );
      if (windowIndex !== -1) {
        state.windows[windowIndex] = payload;
      }
    },
    focusAndHighlightWindow: (
      state,
      { payload }: TFocusAndHighlightPayload
    ) => {
      state.windows = focusOnWindow(state.windows, payload.id);
      state.highlighted = payload.id;
    },
    cleanHighlight: (state) => {
      state.highlighted = null;
    },
  },
});

export const getId = () => {
  return ++counter;
};

export const focusAndHighlightWindow = createAsyncThunk(
  "windowManager/focusAndHighlightWindowThunk",
  async (id: TWindowId, thunkApi) => {
    thunkApi.dispatch(
      wmSlice.actions.focusAndHighlightWindow({
        id,
      })
    );

    setTimeout(() => {
      thunkApi.dispatch(wmSlice.actions.cleanHighlight());
    }, 1000);
  }
);

export {};

export { openLoginWindow, closeLoginWindow } from "windows/LoginWindow/logic";
export { openNetInfoWindow } from "windows/NetInfoWindow/logic";
export { openNewThreadWindow } from "windows/NewThreadWindow/logic";
export {
  openNewPostWindow,
  addReply as addReplyToNewPostWindow,
  removeReply as removeReplyFromNewPostWindow,
} from "windows/NewPostWindow/logic";
