import { createSlice } from "@reduxjs/toolkit";

type TMode = "left" | "right" | "none";

export type TState = {
  mode: TMode;
};

const initialState: TState = {
  mode: "none",
};

export const sidebarSlice = createSlice({
  name: "switchSidebar",
  initialState,
  reducers: {
    toggleLeft: (state) => {
      state.mode = state.mode === "left" ? "none" : "left";
    },
    toggleRight: (state) => {
      state.mode = state.mode === "right" ? "none" : "right";
    },
    hide: (state) => {
      state.mode = "none";
    },
  },
});
