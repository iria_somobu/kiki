import { PayloadAction, createSlice } from "@reduxjs/toolkit";

type TState = {
  postsInViewport: string[];
  highlightedPost: string | null;
};

const initialState: TState = {
  postsInViewport: [],
  highlightedPost: null,
};

type TAddPostInViewportPayload = PayloadAction<{
  hash: string;
}>;

type TPostHighlightPayload = PayloadAction<{
  hash: string;
}>;

export const centralThreadSlice = createSlice({
  name: "centralThread",
  initialState,
  reducers: {
    addPostInViewport: (
      state,
      { payload: { hash } }: TAddPostInViewportPayload
    ) => {
      const isAlreadyAdded = state.postsInViewport.includes(hash);
      if (!isAlreadyAdded) {
        state.postsInViewport.push(hash);
      }
    },
    removePostInViewport: (
      state,
      { payload: { hash } }: TAddPostInViewportPayload
    ) => {
      state.postsInViewport = state.postsInViewport.filter((h) => h !== hash);
    },
    highlightPost: (state, { payload: { hash } }: TPostHighlightPayload) => {
      state.highlightedPost = hash;
    },
    unHighlightPost: (state) => {
      state.highlightedPost = null;
    },
  },
});
