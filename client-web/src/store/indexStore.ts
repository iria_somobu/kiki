import { combineReducers, configureStore } from "@reduxjs/toolkit";

import { persistReducer, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { tokenSlice } from "store/tokenHolder";

import { centralThreadSlice } from "./centralThread";
import { postHoverSlice } from "./postHover";
import { sidebarSlice } from "./sidebarSwitcher";
import { wmSlice } from "./windowManager";

const persistConfig = {
  key: "tokenHolder",
  whitelist: ["token"], // or any other field name from combineReducers below
  storage,
};

export type TRootState = ReturnType<typeof store.getState>;

const rootReducer = combineReducers({
  sidebar: sidebarSlice.reducer,
  token: tokenSlice.reducer,
  postHover: postHoverSlice.reducer,
  windowManager: wmSlice.reducer,
  centralThread: centralThreadSlice.reducer,
});

const reducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: reducer,
  middleware(getDefaultMiddleware) {
    return getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: ["persist/PERSIST"],
      },
    });
  },
});

export const persistor = persistStore(store);
