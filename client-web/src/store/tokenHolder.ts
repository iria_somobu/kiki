import {
  AnyAction,
  PayloadAction,
  ThunkDispatch,
  createAsyncThunk,
  createSlice,
} from "@reduxjs/toolkit";

import { client, graphql } from "gql";

import { TRootState } from "./indexStore";

type TStatus = "authorized" | "loading" | "failed";

export type TState = {
  token?: string;
  isNamed: boolean;
  isModer: boolean;
  status: TStatus;
};

const initialState: TState = {
  token: undefined,
  isNamed: false,
  isModer: false,
  status: "loading",
};

export type TTokenPayload = PayloadAction<{
  token: string;
  flags: number;
}>;

const FLAG_NAMED = 1 << 0;
const FLAG_MODER = 1 << 1;

export const tokenSlice = createSlice({
  name: "tokenHolder",
  initialState,
  reducers: {
    updateToken: (state, { payload }: TTokenPayload) => {
      state.token = payload.token;
      state.isNamed = (payload.flags & FLAG_NAMED) > 0;
      state.isModer = (payload.flags & FLAG_MODER) > 0;
      state.status = "authorized";
    },
    setStatus: (state, { payload }: PayloadAction<TStatus>) => {
      state.status = payload;
    },
    clearToken: (state) => {
      /* eslint-disable no-unused-vars */
      state = initialState;
    },
  },
});

const GQL_NEW_TOKEN = graphql(`
  mutation GetNewToken {
    newToken {
      token
      flags
    }
  }
`);

const GQL_VALIDATE_TOKEN = graphql(`
  mutation ValidateToken($token: String!) {
    validateToken(token: $token) {
      token
      flags
    }
  }
`);

const queryNewToken = async (
  dispatch: ThunkDispatch<unknown, unknown, AnyAction>
) => {
  const result = await client.mutate({
    mutation: GQL_NEW_TOKEN,
  });

  if (result.errors === undefined && result.data) {
    dispatch(tokenSlice.actions.updateToken(result.data.newToken));
  }

  return;
};

const validateToken = async (token: string) => {
  try {
    const result = await client.mutate({
      mutation: GQL_VALIDATE_TOKEN,
      variables: {
        token,
      },
    });
    return result.errors === undefined && result.data;
  } catch (e) {}

  return false;
};

export const auth = createAsyncThunk(
  "tokenHolder/auth",
  async (_, { getState, dispatch }) => {
    const state = getState() as TRootState;
    const token = state.token.token;

    dispatch(tokenSlice.actions.setStatus("loading"));

    if (token === undefined) {
      await queryNewToken(dispatch);
      return;
    }

    const isTokenValid = await validateToken(token);
    if (isTokenValid === false) {
      console.warn("Current token is not valid! Getting new one...");
      await queryNewToken(dispatch);
    }
  }
);
