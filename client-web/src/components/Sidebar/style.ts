import styled, { css } from "styled-components";
import { ZIndices } from "utils/constants";

type TProps = {
  visible?: boolean;
  persistent?: boolean;
  placement: "right" | "left";
};

const NotPersistentVisible = css<TProps>`
  position: fixed;
  top: 0;
  ${({ placement }) => (placement === "right" ? "right: 0px" : "left: 0px")};
  height: 100vh;
  background-color: var(--totally-white);
`;

export const Container = styled.aside<TProps>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: var(--sidebar-width);
  z-index: ${ZIndices.sidebars};

  ${({ visible, persistent }) =>
    !persistent && visible ? NotPersistentVisible : ""};
`;

export const OutSurface = styled.div`
  position: fixed;
  width: 100vw;
  height: 100vh;
  top: 0;
  z-index: ${ZIndices.sidebars - 1};
  background-color: var(--sidebar-transparent);
  cursor: pointer;
`;
