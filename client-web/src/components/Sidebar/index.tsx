import { Container, OutSurface } from "./style";

type TProps = {
  children: React.ReactNode;
  onClose: () => void;
  className?: string;
  persistent?: boolean;
  visible?: boolean;
  placement: "right" | "left";
};

const Sidebar: React.FC<TProps> = ({
  className,
  children,
  persistent,
  visible,
  placement,
  onClose,
}) => {
  const isSidebarVisible = persistent || visible;
  const isOutSurfaceVisible = !persistent && visible;
  return (
    <>
      {isSidebarVisible ? (
        <Container
          className={className}
          persistent={persistent}
          visible={visible}
          placement={placement}
        >
          {children}
        </Container>
      ) : null}
      {isOutSurfaceVisible ? <OutSurface onClick={onClose} /> : null}
    </>
  );
};

export default Sidebar;
