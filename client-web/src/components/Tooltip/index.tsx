import RcTooltip from "rc-tooltip";
import "rc-tooltip/assets/bootstrap.css";
import { isTouch } from "utils/constants";

type TInheritProps = Pick<
  React.ComponentProps<typeof RcTooltip>,
  | "children"
  | "overlay"
  | "placement"
  | "showArrow"
  | "destroyTooltipOnHide"
  | "visible"
  | "align"
  | "overlayStyle"
>;

type TProps = TInheritProps & {
  overlayInnerStyle?: React.CSSProperties;
  hideOnOverlayHover?: boolean;
};

const Tooltip: React.FC<TProps> = ({
  children,
  overlayInnerStyle = {},
  hideOnOverlayHover,
  overlayStyle,
  ...props
}) => {
  const baseOverlayStyle: React.CSSProperties = {
    pointerEvents: hideOnOverlayHover ? "none" : "auto",
    display: isTouch() ? "none" : "initial",
  };

  return (
    <RcTooltip
      {...props}
      overlayInnerStyle={{
        padding: "8px",
        borderRadius: "8px",
        backgroundColor: "var(--dk-blue)",
        boxShadow: "0 4px 16px rgba(0, 0, 0, 0.25)",
        ...overlayInnerStyle,
      }}
      overlayStyle={{
        ...baseOverlayStyle,
        ...overlayStyle,
      }}
      mouseEnterDelay={0.3}
      destroyTooltipOnHide={{ keepParent: false }}
    >
      {children}
    </RcTooltip>
  );
};

export default Tooltip;
