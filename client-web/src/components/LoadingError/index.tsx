import React from "react";
import { useTranslation } from "react-i18next";

import { ApolloError } from "@apollo/client";

import { Container, ForceSizeContainer } from "./style";

type TProps = {
  error?: ApolloError;
  short?: boolean;
  fsize?: boolean;
  errMsg?: string;
};

/**
 * Reusable placeholder that shows "Loading" or "Error" message
 *
 * @param error just pass ref to apollo error
 * @param errMsg just pass some text if you don't have apollo error
 * @param short if short message needed
 * @param fsize workaround that forces placeholder to take 100% width and height
 */
const LoadingError: React.FC<TProps> = ({ error, short, fsize, errMsg }) => {
  const { t } = useTranslation();

  const isForceSize = fsize == null ? false : fsize;
  const isShortMsg = short == null ? false : short;

  let content;
  if (error == null) {
    content = <>{t(isShortMsg ? "loading.short" : "loading.long")}</>;
  } else {
    content = (
      <>
        {isShortMsg
          ? error.message
          : t("error", { message: error.message || errMsg })}
      </>
    );
  }

  if (isForceSize) return <ForceSizeContainer>{content}</ForceSizeContainer>;
  else return <Container>{content}</Container>;
};

export default LoadingError;
