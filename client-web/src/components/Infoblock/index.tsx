import { useState } from "react";

import { t } from "i18next";
import NavPlusIcon from "img/close.svg";
import Markdown from "marked-react";
import { isTouch } from "utils/constants";

import { Block, CloseCross, Md } from "./style";

type TProps = {
  blockName: string;
  translationKey: string;
};

const localStoragePrefix = "infoblock::";

const Infoblock: React.FC<TProps> = ({ blockName: key, translationKey }) => {
  const [isHidden, setIsHidden] = useState<boolean>(() => {
    return localStorage.getItem(localStoragePrefix + key) === "hidden";
  });

  const crossMobileOverride = {
    display: isTouch() ? "block" : "",
  };

  const mdMobileOverride = {
    marginRight: isTouch() ? "16px" : "",
  };

  const onClose = () => {
    setIsHidden(true);
    localStorage.setItem(localStoragePrefix + key, "hidden");
  };

  if (isHidden) {
    return <></>;
  } else {
    return (
      <Block>
        <CloseCross onClick={onClose} style={crossMobileOverride}>
          <img src={NavPlusIcon} width="100%" height="100%" alt="close" />
        </CloseCross>
        <Md style={mdMobileOverride}>
          <Markdown value={t(translationKey)} />
        </Md>
      </Block>
    );
  }
};

export default Infoblock;
