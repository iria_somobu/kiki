import styled from "styled-components";

export const Block = styled.div`
  position: relative;

  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;

  padding: 8px 12px;

  background: var(--card-white);
  border-radius: 4px;

  font-size: 14px;

  & div:first-child {
    display: none;
  }

  &:hover div:first-child {
    display: block;
  }
`;

export const CloseCross = styled.div`
  display: block;
  position: absolute;
  right: 4px;
  top: 4px;

  width: 24px;
  height: 24px;

  padding: 4px;

  cursor: pointer;

  background: var(--card-grey);
  border-radius: 24px;
`;

export const Md = styled.div`
  align-items: start;
  text-align: start;
`;
