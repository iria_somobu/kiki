import { TPost } from "utils/types";

import ModBtn from "./ModBtn";

type TProps = {
  post: TPost;
};

const ModButtons: React.FC<TProps> = ({ post }) => {
  return (
    <>
      {post.onHold ? (
        <ModBtn md={{ post: post.hash, hidden: post.hidden, onHold: false }}>
          Rm hold
        </ModBtn>
      ) : (
        <></>
      )}

      {post.hidden ? (
        <ModBtn md={{ post: post.hash, hidden: false, onHold: post.onHold }}>
          Un-hide
        </ModBtn>
      ) : (
        <ModBtn md={{ post: post.hash, hidden: true, onHold: post.onHold }}>
          Hide
        </ModBtn>
      )}
    </>
  );
};

export default ModButtons;
