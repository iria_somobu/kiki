import { useMutation } from "@apollo/client";

import { graphql } from "gql";
import { useToken } from "hooks";

import { ReplyBtn } from "../style";

export type TProps = {
  children: React.ReactNode;
  md: {
    post: string;
    hidden: boolean;
    onHold: boolean;
  };
};

const GQL_MOD_POST = graphql(`
  mutation ModPost(
    $token: String!
    $post: String!
    $hidden: Boolean!
    $onHold: Boolean!
  ) {
    modPost(token: $token, post: $post, hidden: $hidden, onHold: $onHold)
  }
`);

const ModBtn: React.FC<TProps> = ({ children, md }) => {
  const [mutate, { data, loading, error }] = useMutation(GQL_MOD_POST);

  const token = useToken();
  const variables = { ...md, token };

  if (data) {
    return <ReplyBtn>Success</ReplyBtn>;
  } else if (loading) {
    return <ReplyBtn>Loading</ReplyBtn>;
  } else if (error) {
    return <ReplyBtn>Error</ReplyBtn>;
  } else {
    return (
      <ReplyBtn onClick={() => mutate({ variables })}>{children}</ReplyBtn>
    );
  }
};

export default ModBtn;
