import styled from "styled-components";

export const Container = styled.span`
  display: inline-block;
  position: relative;

  font-size: 12px;

  cursor: pointer;
  color: var(--grey);

  &:hover {
    color: var(--lt-blue);
  }
`;
