import React from "react";

import { trimHash } from "utils";

import { Container } from "./style";

type TProps = {
  hash: string;
  className?: string;
  style?: React.CSSProperties;
  isReply?: boolean;
};

const PostHash: React.FC<TProps> = ({ hash, className, style, isReply }) => {
  return (
    <Container
      data-post-hash={hash}
      style={style}
      className={className}
      data-post-hash-reply={isReply ? true : false}
    >
      {trimHash(hash)}
    </Container>
  );
};

export default PostHash;
