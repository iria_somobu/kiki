import { Link } from "react-router-dom";

import { BaseIcon } from "App/style";
import styled from "styled-components";
import { Breakpoints } from "utils/constants";

import PostHash from "./PostHash";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  @media (min-width: ${Breakpoints.extraMobile.px}) {
    max-width: 698px;
  }
`;

export const ThreadHeadContainer = styled.div`
  position: relative; /* Unread badge position workaround */
  display: flex;
  align-items: center;

  min-width: 0;

  height: 36px;
  padding: 0 16px;

  background: var(--dk-blue);
  border-radius: 8px 8px 0 0;

  @media (min-width: ${Breakpoints.extraMobile.px}) {
    height: 48px;
    border-radius: 8px 8px 0 0;
  }

  & > * {
    font-weight: 500;
    font-size: 16px;
    line-height: 120%;

    color: var(--totally-white);
  }
`;

export const ThreadHeadButton = styled.img`
  width: 16px;
  height: 16px;
  padding: 8px;
  cursor: pointer;

  :hover {
    border-radius: 6px;
    background-color: var(--lt-blue);
  }
`;

export const ThreadHeadIcon = BaseIcon;

export const ThreadTitle = styled(Link)`
  flex-grow: 1;
  min-width: 0;
  height: 24px;
  line-height: 24px;
  text-decoration: none;
  word-wrap: break-word;

  overflow: hidden;
  text-overflow: ellipsis ellipsis;

  color: var(--totally-white);

  &:hover {
    color: var(--card-white);
  }
`;

export const ThreadMsgsCnt = styled.span`
  font-size: 14px;
`;

type TThreadPostContainerProps = {
  isOP: boolean;
  isHold: boolean;
  isHidden: boolean;
  bordered?: boolean;
  isHighlighted?: boolean;
};

export const ThreadPostContainer = styled.div<TThreadPostContainerProps>`
  padding: 16px 8px;

  display: flex;
  align-items: flex-start;
  flex-direction: column;
  overflow: hidden;

  margin-top: auto;

  border: ${({ bordered }) => (bordered ? "solid black 1px" : "unset")};
  border-radius: ${({ isOP }) => (isOP ? "0 0 8px 8px" : "8px")};

  background-color: ${({ isHold, isHidden, isHighlighted }) => {
    if (isHold) {
      return "#f2dfdf";
    } else if (isHidden) {
      return "#d5d5d5";
    } else if (isHighlighted) {
      return "var(--col-post-highlight)";
    } else {
      return "var(--card-white)";
    }
  }};

  @media (min-width: ${Breakpoints.extraMobile.px}) {
    padding: 16px;
  }
`;

export const PostInfo = styled.div`
  width: 100%;

  display: flex;
  align-items: center;
  gap: 10px;

  padding-bottom: 12px;

  & > * {
    font-size: 12px;
    line-height: 120%;
  }
`;

export const PostText = styled.div`
  overflow: hidden;
  overflow-y: scroll;
  flex-grow: 1;

  -ms-overflow-style: none;
  scrollbar-width: none;
  &::-webkit-scrollbar {
    display: none;
  }

  font-size: 16px;
  line-height: 150%;
  word-wrap: break-word;
  overflow-wrap: break-word;

  color: var(--totally-black);
  text-align: left;
`;

export const PostTextLimiter = styled(PostText)`
  max-height: 400px;
  overflow-y: clip;
`;

export const PostHeader = styled.div`
  margin-bottom: 16px;
  display: flex;
`;

export const PostFooter = styled.div`
  width: 100%;
  margin-top: 16px;
  display: flex;
  align-items: flex-end;
`;

export const Replies = styled.span`
  display: inline-block;

  font-size: 12px;

  color: var(--grey);
`;

export const ReplyBtn = styled.span`
  display: inline-block;

  padding-left: 8px;

  height: 16px;
  font-size: 12px;

  cursor: pointer;
  color: var(--grey);

  &:hover {
    color: var(--lt-blue);
  }
`;

export const PostLinkHash = styled(PostHash)`
  padding-left: 4px;
`;

export const PostInnerLinkHash = styled(PostHash)`
  padding: 2px;
  background-color: var(--card-grey);
  font-size: 14px;
`;

export const ImgsRoot = styled.div`
  position: relative;
  display: inline-flex;
  max-width: 100%;

  flex-direction: row;

  overflow-x: clip;

  border-radius: 8px;
  margin-bottom: 12px;
`;
