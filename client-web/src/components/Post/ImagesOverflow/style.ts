import styled from "styled-components";

export const ImgsOverflow = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;

  display: flex;
  justify-content: center;
  align-items: center;

  width: 64px;
  background-color: rgba(13, 13, 27, 0.6);
  transition: background-color 0.2s;

  cursor: pointer;

  & img {
    width: 32px;
    height: 32px;
    transition: width 0.2s, height 0.2s;
  }

  &:hover {
    background-color: rgba(13, 13, 27, 0.75);
    transition: background-color 0.2s;
  }

  &:hover img {
    width: 36px;
    height: 36px;
    transition: width 0.2s, height 0.2s;
  }
`;
