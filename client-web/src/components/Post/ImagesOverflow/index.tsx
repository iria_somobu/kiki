import OpenInNew from "img/open_in_new_white.svg";

import { ImgsOverflow } from "./style";

type TProps = {
  toggleExpand: () => void;
};

const ImagesOverflow: React.FC<TProps> = ({ toggleExpand }) => {
  return (
    <ImgsOverflow onClick={toggleExpand}>
      <img src={OpenInNew} alt={""} />
    </ImgsOverflow>
  );
};

export default ImagesOverflow;
