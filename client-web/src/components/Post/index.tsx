import { useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";

import { UnreadBadge } from "App/CentralBoard/style";
import { Spacer } from "App/style";
import { useSelector } from "hooks";
import FavorIcon from "img/favor.svg";
import MessageWhite from "img/message_white.svg";
import MuteIcon from "img/mute.svg";
import Markdown from "marked-react";
import { CustomReactRenderer } from "marked-react/dist/ReactRenderer";
import { getUnreadDelta, trimHash } from "utils";
import { TPost, TThread } from "utils/types";

import ImagesOverflow from "./ImagesOverflow";
import ImgEnlarge from "./ImgEnlarge";
import ModButtons from "./ModButtons";
import {
  Container,
  ImgsRoot,
  PostFooter,
  PostLinkHash as PostHash,
  PostHeader,
  PostInfo,
  PostInnerLinkHash,
  PostText,
  PostTextLimiter,
  Replies,
  ReplyBtn,
  ThreadHeadButton,
  ThreadHeadContainer,
  ThreadHeadIcon,
  ThreadMsgsCnt,
  ThreadPostContainer,
  ThreadTitle,
} from "./style";

const renderer: CustomReactRenderer = {
  image() {
    return <></>;
  },
  link(href: string, text: React.ReactNode) {
    if (href.startsWith("link://")) {
      return <PostInnerLinkHash hash={href.replace("link://", "")} />;
    } else {
      return (
        <a
          target="_blank"
          href={
            /^[a-zA-Z][a-zA-Z0-9+\-.]*:/.test(href) ? href : "http://" + href
          }
          rel="noreferrer"
        >
          {text}
        </a>
      );
    }
  },
};

type TProps = {
  net: string;
  board: string;
  thread?: TThread;
  post: TPost;
  isOP: boolean;
  isLimited: boolean;
  bordered?: boolean;
  isHighlighted?: boolean;
  onInViewportVisibilityChange?: (isVisible: boolean) => void;
  onReplyClick?: (
    hash: string,
    root: string,
    clientX: number,
    clientY: number
  ) => void;
};

const Post: React.FC<TProps> = ({
  net,
  board,
  thread,
  post,
  isOP,
  isLimited,
  bordered,
  onReplyClick,
  isHighlighted,
  onInViewportVisibilityChange,
}) => {
  const { t } = useTranslation();
  const [overflow, setOverflow] = useState(false);
  const [imgExpand, setImgExpand] = useState(false);
  const toggleImgExpand = () => setImgExpand(!imgExpand);

  const isNamed = useSelector((state) => state.token.isNamed);
  const isModer = useSelector((state) => state.token.isModer);

  const imagesRef = useRef<HTMLDivElement>(null);
  const selfRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    function handleResize() {
      if (imagesRef.current != null) {
        const sc = imagesRef.current.scrollWidth;
        const cw = imagesRef.current.clientWidth;
        setOverflow(sc > cw);
      }
    }

    window.addEventListener("resize", handleResize);
    handleResize();
    return () => window.removeEventListener("resize", handleResize);
  });

  let images = null;
  if (post.images && post.images.length >= 1) {
    images = (
      <ImgsRoot
        ref={imagesRef}
        style={{ flexWrap: imgExpand ? "wrap" : "nowrap" }}
      >
        {post.images.map((item) => (
          <ImgEnlarge key={item} imageId={item} />
        ))}
        {overflow || imgExpand ? (
          <ImagesOverflow toggleExpand={toggleImgExpand} />
        ) : (
          <></>
        )}
      </ImgsRoot>
    );
  }

  let unreadBadge = <></>;
  if (thread !== undefined) {
    const validUnread = thread.unreads - getUnreadDelta(post.hash);
    if (validUnread > 0) {
      unreadBadge = <UnreadBadge>{validUnread}</UnreadBadge>;
    }
  }

  const onReplyBtnClick = (e: React.MouseEvent) => {
    onReplyClick?.(post.hash, post.root, e.clientX, e.clientY);
  };

  const refLinks = post.parents.reduce((accum, curr) => {
    return accum + "\n[" + curr.substr(0, 7) + "]: link://" + curr;
  }, "\n\n");

  const fullMessage = post.message + refLinks;

  useEffect(() => {
    const el = selfRef.current;
    if (el === null) {
      return;
    }

    const callback: IntersectionObserverCallback = (e) => {
      const first = e[0];
      onInViewportVisibilityChange?.(first.isIntersecting);
    };

    const observer = new IntersectionObserver(callback, { threshold: 0.1 });
    observer.observe(el);

    return () => {
      observer.disconnect();
    };
  }, [onInViewportVisibilityChange]);

  return (
    <Container ref={selfRef}>
      {isOP ? (
        <ThreadHeadContainer>
          <ThreadTitle to={"/" + net + "/" + board + "/" + post.root}>
            {post.title}
          </ThreadTitle>
          {isNamed ? (
            <>
              <ThreadHeadButton src={MuteIcon} alt={t("post.mute")} />
              <ThreadHeadButton src={FavorIcon} alt={t("post.fav")} />
            </>
          ) : (
            <></>
          )}
          {isLimited ? (
            <>
              <ThreadHeadIcon src={MessageWhite} alt={t("post.total")} />
              <ThreadMsgsCnt>{thread?.replies}</ThreadMsgsCnt>
            </>
          ) : (
            <></>
          )}
          {unreadBadge}
        </ThreadHeadContainer>
      ) : (
        <></>
      )}

      <ThreadPostContainer
        isOP={isOP}
        isHold={post.onHold}
        isHidden={post.hidden}
        bordered={bordered}
        isHighlighted={isHighlighted}
      >
        <PostInfo>
          <span>{trimHash(post.hash)}</span>
          <Spacer />
          <span>{dateTime(post.date)}</span>
        </PostInfo>
        {isLimited ? (
          <>
            {images}
            <PostTextLimiter>
              <Markdown value={fullMessage} renderer={renderer} />
            </PostTextLimiter>
          </>
        ) : (
          <>
            {post.parents.length > 0 ? (
              <PostHeader>
                <Replies>
                  {t("post.parents")}
                  {post.parents.map((hash) => {
                    return <PostHash key={hash} hash={hash} />;
                  })}
                </Replies>
              </PostHeader>
            ) : (
              <></>
            )}
            {images}
            <PostText>
              <Markdown value={fullMessage} renderer={renderer} />
            </PostText>
            <PostFooter>
              <Replies>
                {post.children.length > 0 ? (
                  <>
                    {t("post.children")}
                    {post.children.map((hash) => {
                      return <PostHash key={hash} hash={hash} isReply />;
                    })}
                  </>
                ) : (
                  <>{t("post.childfree")}</>
                )}
              </Replies>
              <Spacer />
              {isModer ? <ModButtons post={post} /> : null}
              <ReplyBtn onClick={onReplyBtnClick}>{t("post.reply")}</ReplyBtn>
            </PostFooter>
          </>
        )}
      </ThreadPostContainer>
    </Container>
  );
};

const dateTime = (timestamp: number) => {
  const date = new Date(timestamp * 1000);

  const day = "0" + date.getDate();
  const month = "0" + (date.getMonth() + 1);
  const year = date.getFullYear();

  const hours = date.getHours();
  const minutes = "0" + date.getMinutes();
  const seconds = "0" + date.getSeconds();

  const dt = day.substr(-2) + "." + month.substr(-2) + "." + year;
  const tm = hours + ":" + minutes.substr(-2) + ":" + seconds.substr(-2);

  return dt + " " + tm;
};

export default Post;
