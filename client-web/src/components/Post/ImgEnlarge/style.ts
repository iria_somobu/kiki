import styled from "styled-components";
import { ZIndices } from "utils/constants";

export const ImgLink = styled.a`
  display: block;
`;

export const ThumbnailImg = styled.img`
  display: block; /* fixes 4px bottom padding within 'a' tag */
  height: 96px;
  width: 96px;
  object-fit: cover;

  /* 
     Here flex-grow and flex-shrink are both required to:
     1. Prevent certain images from shrink/grow on post width change
     2. Make ImgsOverflow appear correctly on force page reload
   */
  flex-grow: 0;
  flex-shrink: 0;

  cursor: pointer;

  :hover {
    transition: 0.4s ease;
    box-shadow: 0 0 8px var(--lt-blue);
  }
`;

export const PreviewImg = styled.img`
  object-fit: cover;
  width: 100%;
  height: 100%;
`;

export const PreviewRoot = styled.div`
  display: block;
  position: absolute;

  overflow: hidden;
  pointer-events: none;

  border-radius: 8px;
  background-color: rgba(0, 0, 0, 0.65);
  border: 2px solid black;

  z-index: ${ZIndices.postHovers};
`;
