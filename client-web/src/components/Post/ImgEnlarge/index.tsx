import { useEffect, useRef, useState } from "react";
import { createPortal } from "react-dom";

import { isTouch } from "utils/constants";
import { imgToUrl, imgToUrlScaled } from "utils/routes";

import { ImgLink, PreviewImg, PreviewRoot, ThumbnailImg } from "./style";

type TProps = {
  imageId: string;
};

const ImgEnlarge: React.FC<TProps> = ({ imageId }) => {
  const imgRef = useRef<HTMLImageElement>(null);

  const [hovered, setHovered] = useState(false);
  const [showHover, setShowHover] = useState(false);

  useEffect(() => {
    if (hovered) {
      const timer1 = setTimeout(() => setShowHover(true), 600);
      return () => clearTimeout(timer1);
    } else {
      setShowHover(false);
    }
  }, [hovered]);

  let preview = <></>;
  if (!isTouch() && showHover && imgRef.current != null) {
    const element = imgRef.current;
    const rect = element.getBoundingClientRect();

    const lilOffset = 8;
    const padding = 0.95;
    const doublePadding = 1 - (1 - padding) * 2;

    const maxWidth = window.innerWidth;
    const maxHeight = window.innerHeight;

    const targetSide = Math.min(
      720,
      Math.min(maxWidth, maxHeight) * doublePadding
    );

    let width = element.naturalWidth;
    let height = element.naturalHeight;

    if (width > height) {
      const coeff = targetSide / width;
      width = targetSide;
      height *= coeff;
    } else {
      const coeff = targetSide / height;
      width *= coeff;
      height = targetSide;
    }

    let top = rect.top - lilOffset;
    if (top + height > maxHeight * padding) {
      top = maxHeight * padding - height - lilOffset;
    }

    let left = rect.left - lilOffset;
    if (left + width > maxWidth * padding) {
      left = maxWidth * padding - width - lilOffset;
    }

    const previewRoot = (
      <PreviewRoot
        style={{
          top,
          left,
          width,
          height,
        }}
      >
        <PreviewImg src={imgToUrl(imageId)} />
      </PreviewRoot>
    );
    preview = createPortal(previewRoot, document.body);
  }

  return (
    <ImgLink href={imgToUrl(imageId)} target={"_blank"}>
      <ThumbnailImg
        ref={imgRef}
        src={imgToUrlScaled(imageId, "240")}
        onMouseEnter={() => setHovered(true)}
        onMouseLeave={() => setHovered(false)}
      />
      {preview}
    </ImgLink>
  );
};

export default ImgEnlarge;
