import styled from "styled-components";

export const Container = styled.div`
  flex-grow: 1;
  align-self: stretch;
`;
