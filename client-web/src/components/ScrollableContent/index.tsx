import { WheelEvent, useRef } from "react";
import Scrollbars from "react-custom-scrollbars-2";

type TInheritProps = Pick<
  React.ComponentProps<typeof Scrollbars>,
  "autoHeight"
>;

type TProps = {
  children: React.ReactNode;
  isHorizontal?: boolean;
} & TInheritProps;

const ScrollableContent: React.FC<TProps> = ({
  children,
  isHorizontal,
  ...props
}) => {
  const scrollRef = useRef<Scrollbars>(null);

  const onWheel = (e: WheelEvent<Scrollbars>) => {
    if (isHorizontal) {
      const scroll = scrollRef.current;
      if (scroll) {
        scroll.scrollLeft(scroll.getScrollLeft() + e.deltaY);
      }
    }
  };

  return (
    <Scrollbars {...props} ref={scrollRef} onWheel={onWheel}>
      {children}
    </Scrollbars>
  );
};

export default ScrollableContent;
