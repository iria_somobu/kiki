import styled from "styled-components";

type TProps = {
  height?: number;
};

export const Input = styled.input<TProps>`
  min-height: ${({ height }) =>
    height === undefined ? "32px" : `${height}px`};
  max-height: ${({ height }) =>
    height === undefined ? "32px" : `${height}px`};

  flex: 1;
  padding: 6px 8px;
  min-height: 32px;

  border: 1px solid var(--card-grey);
  border-radius: 6px;
`;
