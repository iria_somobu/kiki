import { Input as StyledInput } from "./style";

type TProps = {
  value: string;
  placeholder?: string;
  onChange: (text: string) => void;
  style?: React.CSSProperties;
  className?: string;
  disabled?: boolean;
};

const TextInput: React.FC<TProps> = ({ onChange, ...props }) => {
  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.value);
  };
  return (
    <StyledInput {...props} onChange={onInputChange} type="text"></StyledInput>
  );
};

export default TextInput;
