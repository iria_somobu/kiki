import { useEffect, useRef } from "react";

import EasyMDE from "easymde";
import "easymde/dist/easymde.min.css";

import { Container, Textarea, Wrapper } from "./style";

type TProps = {
  value?: string;
  placeholder?: string;
  onChange: (str: string) => void;
  editorRef?: React.MutableRefObject<EasyMDE | undefined>;
  readonly?: boolean;
};

const Editor: React.FC<TProps> = ({ value, onChange, editorRef, readonly }) => {
  const textareaRef = useRef(null);
  const internalEditorRef = useRef<EasyMDE>();

  useEffect(() => {
    const mde = new EasyMDE({
      element: textareaRef.current as unknown as HTMLElement,
      spellChecker: false,
      toolbar: [
        "bold",
        "italic",
        "strikethrough",
        "|",
        "heading-1",
        "heading-2",
        "heading-3",
        "|",
        "unordered-list",
        "ordered-list",
        "|",
        "link",
        "quote",
        "code",
      ],
      minHeight: "100px",
      status: false,
    });

    internalEditorRef.current = mde;
    if (editorRef) {
      editorRef.current = mde;
    }

    return () => {
      internalEditorRef.current?.toTextArea();
      if (editorRef) {
        editorRef.current = undefined;
      }
    };
  }, [editorRef]);

  useEffect(() => {
    if (internalEditorRef.current) {
      internalEditorRef.current.codemirror.on("change", () =>
        onChange(internalEditorRef.current?.value() ?? "")
      );
    }
  }, [onChange]);

  useEffect(() => {
    if (internalEditorRef.current) {
      internalEditorRef.current.codemirror.setOption(
        "readOnly",
        Boolean(readonly)
      );
    }
  }, [readonly]);

  useEffect(() => {
    if (value !== internalEditorRef.current?.value()) {
      internalEditorRef.current?.value(value ?? "");
    }
  }, [value]);

  return (
    <Wrapper locked={readonly}>
      <Container>
        <Textarea ref={textareaRef} />
      </Container>
    </Wrapper>
  );
};

export default Editor;
