import styled from "styled-components";
import { Breakpoints } from "utils/constants";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;

  overflow: hidden;

  .EasyMDEContainer .editor-toolbar {
    padding: 2px 5px !important;
  }

  .EasyMDEContainer {
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    max-height: 100%;
    overflow: hidden;
  }

  .editor-toolbar {
    @media (max-width: ${Breakpoints.extraMobile.px}) {
      display: flex;
      flex-direction: row;
      overflow-y: scroll;
    }
  }

  .CodeMirror {
    flex-grow: 1;
  }

  .CodeMirror-wrap pre {
    word-break: break-word;
  }

  .CodeMirror-wrap {
    padding-top: 0;
  }
`;

export const Textarea = styled.textarea``;

type TWrapperProps = {
  locked?: boolean;
};

export const Wrapper = styled.div<TWrapperProps>`
  flex-direction: column;

  margin-top: 6px;

  flex-grow: 1;
  height: 100%;
  max-height: 100%;

  overflow: hidden;

  pointer-events: ${({ locked }) => (locked ? "none" : "auto")};
`;
