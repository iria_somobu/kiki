import { initReactI18next } from "react-i18next";

import i18next from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import HttpApi from "i18next-http-backend";

i18next
  .use(HttpApi)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: "en",
    debug: false,
    supportedLngs: ["en", "ru"],
    backend: {
      loadPath: "/static/locales/{{lng}}/{{ns}}.json",
    },
  });

export default i18next;
