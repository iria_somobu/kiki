import { ApolloError } from "@apollo/client";

import i18n from "i18next";

export const trimHash = (str: string) => {
  return ("" + str).substring(0, 7).toLowerCase();
};

export const getApolloErrorType = (err: ApolloError): string => {
  return err.graphQLErrors[0].extensions["type"] as string;
};

export const apolloErrorToString = (err: ApolloError): string => {
  if (err.graphQLErrors === undefined || err.graphQLErrors.length < 1) {
    return err.message;
  }

  const errType = getApolloErrorType(err);

  const key = "errorTypes." + errType;
  if (!i18n.exists(key)) {
    console.warn("Untranslated error! = ", JSON.stringify(err));
  }

  return i18n.t(key);
};

export const setBoardVisitedTime = (
  net: string,
  board: string,
  time: number
) => {
  localStorage.setItem("board-visit-" + net + "/" + board, "" + time);
};

export const getBoardVisitedTime = (net: string, board: string): number => {
  const item = localStorage.getItem("board-visit-" + net + "/" + board);

  let rz = new Date().getTime() / 1000;
  if (item == null) {
    setBoardVisitedTime(net, board, rz);
  } else {
    rz = parseInt(item);
  }

  return rz | 0;
};

export const setUnreadBaseline = (board: string, time: number) => {
  localStorage.setItem("unread-baseline-" + board, "" + time);
};

export const getUnreadBaseline = (board: string): number => {
  const item = localStorage.getItem("unread-baseline-" + board);

  let rz = new Date().getTime() / 1000;
  if (item == null) {
    setUnreadBaseline(board, rz);
  } else {
    rz = parseInt(item);
  }

  return rz | 0;
};

export const setUnreadDelta = (thread: string, delta: number) => {
  localStorage.setItem("unread-delta-" + thread, "" + delta);
};

export const getUnreadDelta = (thread: string): number => {
  const item = localStorage.getItem("unread-delta-" + thread);

  let rz = 0;
  if (item == null) {
    setUnreadDelta(thread, rz);
  } else {
    rz = parseInt(item);
  }

  return rz | 0;
};
