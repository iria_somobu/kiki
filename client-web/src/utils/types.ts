import { Post } from "gql/generated/graphql";

export type TToken = {
  token: string;
  flags: number;
};

export type TThread = {
  replies: number;
  unreads: number;
};

export type TPost = Post;
