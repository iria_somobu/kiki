const httpRoot = "http://" + document.location.hostname + ":8080/";
const wsRoot = "ws://" + document.location.hostname + ":8080/";

const isKitsuki = document.location.hostname.includes("kitsuki.cc");

export const getApiRoute = () => {
  if (isKitsuki) return "https://api.kitsuki.cc";
  else return httpRoot + "api";
};

export const getImgRoute = () => {
  if (isKitsuki) return "https://img.kitsuki.cc";
  else return httpRoot + "img";
};

export const getUploadRoute = () => {
  if (isKitsuki) return "https://upload.kitsuki.cc";
  else return httpRoot + "upload";
};

export const imgToUrl = (img: string) => {
  return getImgRoute() + "/" + img;
};

type TImgScale = "240" | "480";

export const imgToUrlScaled = (img: string, scale: TImgScale) => {
  if (isKitsuki) return getImgRoute() + "/scale/" + scale + "/" + img;
  else return imgToUrl(img);
};

export const getSubscriptionRoute = () => {
  if (isKitsuki) return `wss://ws.kitsuki.cc`;
  return wsRoot + "subscriptions";
};
