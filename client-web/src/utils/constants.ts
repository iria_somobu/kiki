export const Breakpoints = {
  extraMobile: {
    // width is so small that we should turn on mobile mode
    value: 550,
    px: "550px",
  },
  mobile: {
    // if width is lower that this - it is mobile device
    value: 800,
    px: "800px",
  },
  desktop: {
    // actually used as 'max site width'
    value: 1920,
    px: "1920px",
  },
};

export const MAX_REPLIES_COUNT = 7;
export const MAX_IMAGES_PER_POST = 7;

export const ZIndices = {
  sidebars: 2,
  postHovers: 3,
  windows: 4,
};

export const isMobileWidth = () => window.innerWidth < Breakpoints.mobile.value;

export const isMobileHeight = () => window.innerHeight < 800;

export const isTouch = () =>
  "ontouchstart" in window || navigator.maxTouchPoints > 0;
