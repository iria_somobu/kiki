import { ApolloError } from "@apollo/client";

import { getApolloErrorType } from "utils";

import PostOnModeration from "../components/PostOnModeration";

const useWithPostModerationView = (
  onClose: () => void,
  error?: ApolloError
): JSX.Element | null => {
  const isOnHoldState = error
    ? getApolloErrorType(error) === "POST_ON_HOLD"
    : false;

  return isOnHoldState ? (
    <PostOnModeration error={error} onClose={onClose} />
  ) : null;
};

export default useWithPostModerationView;
