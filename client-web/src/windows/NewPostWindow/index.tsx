import { useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import { matchPath } from "react-router-dom";

import { useMutation } from "@apollo/client";

import { Centered } from "App/style";
import Editor from "components/Editor";
import Infoblock from "components/Infoblock";
import EasyMDE from "easymde";
import { graphql } from "gql";
import { useDispatch, useToken } from "hooks";
import { removeReplyFromNewPostWindow, wmSlice } from "store/windowManager";
import { apolloErrorToString, trimHash } from "utils";
import { MAX_REPLIES_COUNT } from "utils/constants";
import { getUploadRoute } from "utils/routes";
import useWithPostModerationView from "windows/hooks/useWithPostModerationView";
import { PostButton } from "windows/style";

import ImgDndUpload, {
  FileDropZone,
  ImgPicker,
  useImgDndUploadContext,
} from "../components/ImgUploadDnd";
import PostDestination from "../components/PostDestination";
import Reply from "./Reply";
import { TNewPostWindow, setSubmittingStatus } from "./logic";
import {
  Container,
  RepliesOverflowError,
  ReplyList,
  WindowHeader,
} from "./style";

const GQL_ADD_POST = graphql(`
  mutation addPost(
    $token: String!
    $net: String!
    $to: [String!]!
    $message: String!
    $images: [String!]!
  ) {
    postPost(
      token: $token
      net: $net
      to: $to
      message: $message
      images: $images
    ) {
      message
      root
    }
  }
`);

type TFormData = {
  text: string;
  err: string | null;
};

type TProps = TNewPostWindow;

const NewPostWindow: React.FC<TProps> = ({
  id,
  data: { net, replies, areRepliesOverflowed: isToOverflowed },
}) => {
  const { t } = useTranslation();
  const token = useToken();
  const dispatch = useDispatch();
  const dndContext = useImgDndUploadContext();

  const [formData, setFormData] = useState<TFormData>({
    text: "",
    err: null,
  });

  const [showDestThread, setShowDestThread] = useState<string | null>(null);
  const editorRef = useRef<EasyMDE>();

  const [mutate, { data, loading, error }] = useMutation(GQL_ADD_POST);

  const closeThisWindow = () =>
    dispatch(
      wmSlice.actions.close({
        id,
      })
    );

  const onReplyRemove = (hash: string) => {
    dispatch(
      removeReplyFromNewPostWindow({
        id,
        hash,
      })
    );
  };

  const insertPostHashInEditor = (hash: string) => {
    const editor = editorRef.current;
    if (editor) {
      const cm = editor.codemirror;
      const pos = cm.getCursor();
      cm.setSelection(pos, pos);

      const prefix = t("wnd.post.mentionHashPrefix");
      const h = trimHash(hash);
      cm.replaceSelection(`[${prefix}${h}][${h}]`);

      cm.focus();
    }
  };

  const moderationView = useWithPostModerationView(closeThisWindow, error);
  const isSubmitting = loading || dndContext.isUploading;

  let bottomLine = <></>;
  if (dndContext.uploadError) {
    bottomLine = <>{t("errorCouldNotUploadImages")}</>;
  } else if (error && !moderationView) {
    const err = apolloErrorToString(error);
    bottomLine = <>{err}</>;
  } else if (dndContext.isUploading) {
    bottomLine = <>{t("wnd.imagesUploading")}</>;
  } else if (loading) {
    bottomLine = <>{t("wnd.submitting")}</>;
  }

  let subState = null;
  if (data && !moderationView) {
    subState = <Centered>{t("wnd.post.success")}</Centered>;
  } else if (moderationView) {
    subState = moderationView;
  }

  if (formData.err) {
    bottomLine = <>{t(formData.err)}</>;
  }

  if (showDestThread) {
    subState = (
      <PostDestination postHash={showDestThread} token={token} net={net} />
    );
  }

  const onTextChange = (text: string) => {
    setFormData({
      ...formData,
      text,
      err: null,
    });
  };

  const validateFormData = () => {
    if (formData.text.length === 0) {
      setFormData({
        ...formData,
        err: "wnd.post.formErrors.emptyText",
      });
      return false;
    }
    return true;
  };

  const onPostBtnClick = async () => {
    if (validateFormData() === false) {
      return;
    }

    const uploaded = await dndContext.uploadFiles(getUploadRoute());

    if (uploaded === null) {
      return;
    }

    const result = await mutate({
      variables: {
        token,
        net: net,
        to: replies.map((reply) => reply.hash),
        message: formData.text,
        images: uploaded,
      },
    });

    if (!result.errors) {
      const resultRoot = result.data?.postPost.root;
      const match = matchPath("/:net/:board/:thread", window.location.pathname);
      const thread = match?.params.thread;
      if (resultRoot && resultRoot !== thread) {
        setShowDestThread(resultRoot);
      } else {
        closeThisWindow();
      }
    }
  };

  useEffect(() => {
    dispatch(
      setSubmittingStatus({
        net,
        isSubmitting,
      })
    );
  }, [isSubmitting, dispatch, net]);

  return (
    <Container>
      {subState ? (
        subState
      ) : (
        <>
          <FileDropZone />
          <WindowHeader>
            <ReplyList>
              {replies.map((hash) => (
                <Reply
                  key={hash.hash}
                  hash={hash.hash}
                  isRemovable={!isSubmitting && replies.length > 1}
                  onRemove={onReplyRemove}
                  onClick={insertPostHashInEditor}
                />
              ))}
            </ReplyList>
            <PostButton onClick={onPostBtnClick} disabled={isSubmitting}>
              {t("wnd.postSend")}
            </PostButton>
          </WindowHeader>
          {isToOverflowed ? (
            <RepliesOverflowError>
              {t("wnd.post.repliesOverflow", { limit: MAX_REPLIES_COUNT })}
            </RepliesOverflowError>
          ) : null}
          <Infoblock
            blockName="post-read-rules"
            translationKey="info.postReadRules"
          />
          <Editor
            onChange={onTextChange}
            value={formData.text}
            editorRef={editorRef}
            readonly={isSubmitting}
          />
          <ImgPicker />
          {bottomLine}
        </>
      )}
    </Container>
  );
};

const Wrapper: React.FC<TProps> = (props) => {
  return (
    <ImgDndUpload>
      <NewPostWindow {...props} />
    </ImgDndUpload>
  );
};

export default Wrapper;
