import { createAsyncThunk } from "@reduxjs/toolkit";

import { client } from "gql";
import { GQL_POST_QUERY } from "gql/queries";
import i18next from "i18next";
import { TRootState } from "store/indexStore";
import {
  TOpenWindowBasePayload,
  TState,
  TWindowBase,
  focusAndHighlightWindow,
  getId,
  wmSlice,
} from "store/windowManager";
import { trimHash } from "utils";
import { MAX_REPLIES_COUNT } from "utils/constants";

type TWindowData = {
  net: string;
  board: string;
  thread: string;
  replies: {
    hash: string;
    root: string;
  }[];
  areRepliesOverflowed: boolean;
  isSubmitting: boolean;
};

export type TNewPostWindow = TWindowBase<"newPost", TWindowData>;

type TOpenPayload = TOpenWindowBasePayload<{
  net: string;
  board: string;
  thread: string;
  reply: {
    hash: string;
    root: string;
  };
}>;

type TAddReplyPayload = {
  net: string;
  board: string;
  thread: string;
  reply: {
    hash: string;
    root: string;
  };
};

const findSameWindow = (wm: TState, net: string) => {
  return wm.windows.find(
    ({ type, data }) => type === "newPost" && data.net === net
  );
};

const calculateThreadHash = (to: string[]) => {
  const counter = new Map<string, number>();

  to.forEach((root) => {
    const value = counter.get(root);
    if (value === undefined) {
      counter.set(root, 1);
    } else {
      counter.set(root, value + 1);
    }
  });

  const halfPlusOne = Math.floor(to.length / 2) + 1;

  let thread: string | null = null;
  counter.forEach((counter, key) => {
    if (counter >= halfPlusOne) {
      thread = key;
    }
  });

  return thread;
};

const queryBoardName = async (rootPost: string, token: string) => {
  return await client.query({
    query: GQL_POST_QUERY,
    variables: {
      token,
      hash: rootPost,
    },
  });
};

const getWindowTitle = async (
  roots: string[],
  board: string,
  net: string,
  token: string
) => {
  const thread = calculateThreadHash(roots);

  if (thread === null) {
    return i18next.t("wnd.post.cannotCalculateThread");
  } else {
    const boardName = await queryBoardName(thread, token);
    return i18next.t("wnd.post.replyIn", {
      board: boardName.data.post.board,
      hash: trimHash(thread),
      net: net,
    });
  }
};

type TSetSubmittingStatusPayload = {
  isSubmitting: boolean;
  net: string;
};

export const setSubmittingStatus = createAsyncThunk(
  "windowManager/newPostWindow/submittingStatus",
  async (
    { isSubmitting, net }: TSetSubmittingStatusPayload,

    { getState, dispatch }
  ) => {
    const state = (getState() as TRootState).windowManager;

    const sameWindow = findSameWindow(state, net);
    if (sameWindow === undefined || sameWindow.type !== "newPost") {
      return;
    }

    const stateNotChanged = sameWindow.data.isSubmitting === isSubmitting;
    if (stateNotChanged) {
      return;
    }

    dispatch(
      wmSlice.actions.setWindowFileds({
        ...sameWindow,
        data: {
          ...sameWindow.data,
          isSubmitting,
        },
      })
    );
  }
);

export const openNewPostWindow = createAsyncThunk(
  "windowManager/openNewPostWindowThunk",
  async ({ data, ...payload }: TOpenPayload, { getState, dispatch }) => {
    const state = getState() as TRootState;
    const wmState = state.windowManager;

    const sameWindow = findSameWindow(wmState, data.net);

    if (sameWindow === undefined) {
      dispatch(
        wmSlice.actions.openNewWindow({
          type: "newPost",
          id: getId(),
          initialX: payload.initialX,
          initialY: payload.initialY,
          title: await getWindowTitle(
            [data.reply.root],
            data.board,
            data.net,
            state.token.token ?? ""
          ),
          data: {
            ...data,
            replies: [
              {
                ...data.reply,
              },
            ],
            areRepliesOverflowed: false,
            isSubmitting: false,
          },
        })
      );
    } else {
      dispatch(focusAndHighlightWindow(sameWindow.id));
    }
  }
);

export const addReply = createAsyncThunk(
  "windowManager/openNewPostWindowThunk/addReply",
  async (payload: TAddReplyPayload, { getState, dispatch }) => {
    const state = getState() as TRootState;

    const window = findSameWindow(state.windowManager, payload.net);
    if (
      window === undefined ||
      window.type !== "newPost" ||
      window.data.isSubmitting === true
    ) {
      return;
    }

    const data = { ...window.data };
    const reply = data.replies.find(
      (reply) => reply.hash === payload.reply.hash
    );

    if (reply !== undefined) {
      return;
    }

    if (data.replies.length === MAX_REPLIES_COUNT) {
      data.areRepliesOverflowed = true;
    } else {
      data.replies = [
        ...data.replies,
        {
          ...payload.reply,
        },
      ];
    }

    const title = await getWindowTitle(
      data.replies.map((r) => r.root),
      data.board,
      data.net,
      state.token.token ?? ""
    );

    dispatch(
      wmSlice.actions.setWindowFileds({
        ...window,
        title,
        data,
      })
    );
  }
);

type TRemoveReplyPayload = {
  id: number;
  hash: string;
};

export const removeReply = createAsyncThunk(
  "windowManager/openNewPostWindowThunk/removeReply",
  async (payload: TRemoveReplyPayload, { getState, dispatch }) => {
    const state = getState() as TRootState;

    const window = state.windowManager.windows.find(
      (window) => window.id === payload.id
    );

    if (window === undefined || window.type !== "newPost") {
      return;
    }

    const newReplies = window.data.replies.filter(
      (reply) => reply.hash !== payload.hash
    );

    const data = window.data;
    const title = await getWindowTitle(
      newReplies.map((r) => r.root),
      data.board,
      data.net,
      state.token.token ?? ""
    );

    dispatch(
      wmSlice.actions.setWindowFileds({
        ...window,
        title,
        data: {
          ...window.data,
          replies: newReplies,
        },
      })
    );
  }
);
