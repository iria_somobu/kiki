import styled from "styled-components";
import { Breakpoints } from "utils/constants";
import { BaseWindowContainer } from "windows/style";

export const Container = styled(BaseWindowContainer)`
  padding: 8px 4px;

  @media (min-width: ${Breakpoints.extraMobile.px}) {
    padding: 16px;
  }
`;

export const WindowHeader = styled.div`
  display: flex;
  width: 100%;
  margin-bottom: 8px;
  flex-shrink: 0;
`;

export const ReplyList = styled.div`
  display: flex;
  flex-grow: 1;
  flex-wrap: wrap;
  align-items: center;
  gap: 8px;
  margin-right: 8px;
`;

export const RepliesOverflowError = styled.div`
  margin-bottom: 8px;
`;
