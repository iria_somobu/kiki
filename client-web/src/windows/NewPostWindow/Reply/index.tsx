import { trimHash } from "utils";

import { Container, Hash, RemoveIcon, RemoveIconContainer } from "./style";

type TProps = {
  hash: string;
  isRemovable: boolean;
  onRemove: (hash: string) => void;
  onClick: (hash: string) => void;
};

const Reply: React.FC<TProps> = ({ hash, onRemove, onClick, isRemovable }) => {
  return (
    <Container>
      <Hash onClick={() => onClick(hash)} isRemovable={isRemovable}>
        {trimHash(hash)}
      </Hash>
      {isRemovable ? (
        <RemoveIconContainer onClick={() => onRemove(hash)}>
          <RemoveIcon />
        </RemoveIconContainer>
      ) : null}
    </Container>
  );
};

export default Reply;
