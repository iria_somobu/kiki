import { ReactComponent as WndClose } from "img/wnd_close.svg";
import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  height: max-content;

  background-color: var(--card-grey);
  border-radius: 4px;

  &:hover {
    cursor: pointer;
  }
`;

type THashProps = {
  isRemovable: boolean;
};

export const Hash = styled.div<THashProps>`
  display: flex;
  align-items: center;
  padding: ${({ isRemovable }) =>
    isRemovable ? "4px 2px 4px 8px" : "4px 8px 4px 8px"};
  font-size: 11px;
`;

export const RemoveIconContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  align-self: stretch;

  border-radius: 4px;
  border-top-left-radius: 0px;
  border-bottom-left-radius: 0px;
  padding: 0 8px;

  &:hover {
    background-color: var(--dk-grey);
  }
`;

export const RemoveIcon = styled(WndClose)`
  width: 14px;
  height: 14px;

  path:last-child {
    fill: black;
  }
`;
