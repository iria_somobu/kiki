export type TDndFile = {
  file: File;
  url: string;
  status: TFileUploadStatus;
};

export type TFileUploadStatus = {
  progress: number;
  status: "added" | "uploading" | "uploaded";
};
