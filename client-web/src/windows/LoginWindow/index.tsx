import React, { useState } from "react";
import { useTranslation } from "react-i18next";

import { useMutation } from "@apollo/client";

import Infoblock from "components/Infoblock";
import { graphql } from "gql";
import { useDispatch } from "hooks";
import { tokenSlice } from "store/tokenHolder";
import { closeLoginWindow } from "store/windowManager";
import { apolloErrorToString } from "utils";

import {
  Centered,
  Container,
  LoginButton,
  StatusView,
  TextInput,
} from "./style";

const GQL_LOGIN = graphql(`
  mutation Login($name: String!, $pwd: String!) {
    login(name: $name, pwd: $pwd) {
      token
      flags
      validTill
    }
  }
`);

type TFormData = {
  name: string;
  pwd: string;
  err: string | null;
};

export const LoginWindow: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [mutate, { error, loading }] = useMutation(GQL_LOGIN);

  const [formData, setFormData] = useState<TFormData>({
    name: "",
    pwd: "",
    err: null,
  });

  const validateFormData = () => {
    if (formData.name.length === 0) {
      setFormData({
        ...formData,
        err: "wnd.login.formErrors.emptyLogin",
      });
      return false;
    } else if (formData.pwd.length === 0) {
      setFormData({
        ...formData,
        err: "wnd.login.formErrors.emptyPassword",
      });
      return false;
    }
    return true;
  };

  const onLoginButtonClick = () => {
    if (validateFormData() === false) {
      return;
    }

    mutate({ variables: formData }).then((value) => {
      if (value.data) {
        dispatch(
          tokenSlice.actions.updateToken({
            token: value.data.login.token,
            flags: value.data.login.flags,
          })
        );

        dispatch(closeLoginWindow(null));
      }
    });
  };

  const onNameChange = (name: string) => {
    setFormData({
      ...formData,
      name,
      err: null,
    });
  };

  const onPasswordChange = (pwd: string) => {
    setFormData({
      ...formData,
      pwd,
      err: null,
    });
  };

  let status = null;
  if (loading) {
    status = <Centered>{t("loading.short")}</Centered>;
  } else if (error) {
    const err = apolloErrorToString(error);
    status = <Centered>{err}</Centered>;
  }

  if (formData.err) {
    status = <Centered>{t(formData.err)}</Centered>;
  }

  return (
    <>
      <Infoblock blockName="login-block" translationKey="info.noReg" />
      <Container>
        <Centered>
          <TextInput
            onChange={onNameChange}
            value={formData.name}
            placeholder={t("wnd.login.login")}
          />
          <TextInput
            onChange={onPasswordChange}
            value={formData.pwd}
            placeholder={t("wnd.login.pwd")}
          />
          <LoginButton onClick={onLoginButtonClick}>
            {t("wnd.login.loginBtn")}
          </LoginButton>
        </Centered>
        <StatusView>{status}</StatusView>
      </Container>
    </>
  );
};
