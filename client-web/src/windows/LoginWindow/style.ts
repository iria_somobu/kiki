import BaseInput from "components/TextInput";
import styled from "styled-components";

import { BaseWindowContainer } from "../style";
import { CommonButton } from "../style";

export const Container = styled(BaseWindowContainer)`
  justify-content: center;
  align-items: center;
`;

export const TextInput = styled(BaseInput)`
  margin-top: 16px;

  &:first-of-type {
    margin-top: 0;
  }
`;

export const Centered = styled.div`
  display: flex;
  flex-direction: column;
`;

export const LoginButton = styled(CommonButton)`
  height: 32px;
  margin-top: 16px;
`;

export const StatusView = styled.div`
  margin-top: 16px;
  min-height: 19px;
`;
