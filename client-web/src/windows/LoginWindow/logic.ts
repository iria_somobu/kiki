import { createAsyncThunk } from "@reduxjs/toolkit";

import i18next from "i18next";
import { TRootState } from "store/indexStore";
import {
  TOpenWindowBasePayload,
  TWindowBase,
  focusAndHighlightWindow,
  getId,
  wmSlice,
} from "store/windowManager";

type TOpenPayload = TOpenWindowBasePayload;

export type TLoginWindow = TWindowBase<"login">;

export const openLoginWindow = createAsyncThunk(
  "windowManager/openLoginWindowThunk",
  async (payload: TOpenPayload, { getState, dispatch }) => {
    const state = (getState() as TRootState).windowManager;

    const sameWindow = state.windows.find(({ type }) => type === "login");

    if (sameWindow === undefined) {
      dispatch(
        wmSlice.actions.openNewWindow({
          type: "login",
          id: getId(),
          presetName: "Small",
          title: i18next.t("wnd.login.title"),
          ...payload,
          data: null,
        })
      );
    } else {
      dispatch(focusAndHighlightWindow(sameWindow.id));
    }
  }
);

export const closeLoginWindow = createAsyncThunk(
  "windowManager/loginWindowThunk/closeWindow",
  async (arg: null, { getState, dispatch }) => {
    const state = (getState() as TRootState).windowManager;

    const loginWindow = state.windows.find((window) => window.type === "login");

    if (loginWindow !== undefined) {
      dispatch(
        wmSlice.actions.close({
          id: loginWindow.id,
        })
      );
    }
  }
);
