import TextInput from "components/TextInput";
import styled from "styled-components";
import { Breakpoints } from "utils/constants";
import { BaseWindowContainer } from "windows/style";

export const PostTitle = styled(TextInput)`
  margin-right: 12px;
`;

export const Container = styled(BaseWindowContainer)`
  padding: 8px 4px;

  @media (min-width: ${Breakpoints.extraMobile.px}) {
    padding: 16px;
  }
`;

export const PostHeader = styled.div`
  display: flex;
  height: 34px;
  flex-shrink: 0;
  align-items: stretch;
  margin-bottom: 6px;
`;
