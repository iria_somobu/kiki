import { createAsyncThunk } from "@reduxjs/toolkit";

import { client } from "gql";
import { GQL_AVAILABLE_NETWORKS_QUERY } from "gql/queries";
import i18next from "i18next";
import { TRootState } from "store/indexStore";
import {
  TOpenWindowBasePayload,
  TWindowBase,
  focusAndHighlightWindow,
  getId,
  wmSlice,
} from "store/windowManager";

type TOpenPayload = TOpenWindowBasePayload<{
  netId: string;
  board: string;
}>;

type TWindowData = {
  netId: string;
  board: string;
};

export type TNewThreadWindow = TWindowBase<"newThread", TWindowData>;

export const openNewThreadWindow = createAsyncThunk(
  "windowManager/openNewThreadWindowThunk",
  async (payload: TOpenPayload, { getState, dispatch }) => {
    const state = getState() as TRootState;

    const wmState = state.windowManager;
    const token = state.token.token as string;

    const windowData = payload.data;

    const sameWindow = wmState.windows.find((wnd) => {
      const { type, data } = wnd;
      return (
        type === "newThread" &&
        data.netId === windowData.netId &&
        data.board === windowData.board
      );
    });

    if (sameWindow !== undefined) {
      dispatch(focusAndHighlightWindow(sameWindow.id));
      return;
    }

    const { data } = await client.query({
      query: GQL_AVAILABLE_NETWORKS_QUERY,
      variables: {
        token,
      },
    });

    if (data === undefined) {
      console.error("Couldn't load network list!");
      return;
    }

    const net = data.networks.find((net) => net.id === payload.data.netId);

    if (net === undefined) {
      console.error("Couldn't find network with this id!");
      return;
    }

    const title = i18next.t("wnd.thread.header", {
      board: payload.data.board,
      net: net.title,
    });

    dispatch(
      wmSlice.actions.openNewWindow({
        type: "newThread",
        id: getId(),
        title,
        presetName: "Default",
        data: {
          board: payload.data.board,
          netId: net.id,
        },
      })
    );
  }
);
