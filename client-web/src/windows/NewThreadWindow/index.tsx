import { useState } from "react";
import { useTranslation } from "react-i18next";

import { useMutation } from "@apollo/client";

import Editor from "components/Editor";
import Infoblock from "components/Infoblock";
import { graphql } from "gql";
import { useDispatch, useToken } from "hooks";
import { wmSlice } from "store/windowManager";
import { apolloErrorToString } from "utils";
import { getUploadRoute } from "utils/routes";

import ImgDndUpload, {
  FileDropZone,
  ImgPicker,
  useImgDndUploadContext,
} from "../components/ImgUploadDnd";
import useWithPostModerationView from "../hooks/useWithPostModerationView";
import { PostButton } from "../style";
import { TNewThreadWindow } from "./logic";
import { Container, PostHeader, PostTitle } from "./style";

const GQL_ADD_THREAD_MUTATION = graphql(`
  mutation addThread(
    $token: String!
    $net: String!
    $board: String!
    $message: String!
    $title: String!
    $images: [String!]!
  ) {
    postThread(
      token: $token
      net: $net
      board: $board
      message: $message
      title: $title
      images: $images
    ) {
      post {
        title
        message
      }
    }
  }
`);

type TFormData = {
  title: string;
  text: string;
  err: string | null;
};

type TProps = TNewThreadWindow;

const NewThreadWindow: React.FC<TProps> = ({ id, data: { netId, board } }) => {
  const { t } = useTranslation();
  const token = useToken();
  const dndContext = useImgDndUploadContext();
  const dispatch = useDispatch();

  const [formData, setFormData] = useState<TFormData>({
    title: "",
    text: "",
    err: null,
  });

  const [mutate, { loading, error }] = useMutation(GQL_ADD_THREAD_MUTATION);

  const closeThisWindow = () =>
    dispatch(
      wmSlice.actions.close({
        id,
      })
    );

  const validateFormData = () => {
    if (formData.title.length === 0) {
      setFormData({
        ...formData,
        err: "wnd.thread.formErrors.emptyTitle",
      });
      return false;
    } else if (formData.text.length === 0) {
      setFormData({
        ...formData,
        err: "wnd.thread.formErrors.emptyText",
      });
      return false;
    }
    return true;
  };

  const onPostBtnClick = async () => {
    if (validateFormData() === false) {
      return;
    }

    const uploaded = await dndContext.uploadFiles(getUploadRoute());

    if (uploaded === null) {
      return;
    }

    const result = await mutate({
      variables: {
        token,
        net: netId,
        board: board,
        message: formData.text,
        title: formData.title,
        images: uploaded,
      },
    });

    if (!result.errors) {
      closeThisWindow();
    }
  };

  const onTitleChange = (title: string) => {
    setFormData({
      ...formData,
      title,
      err: null,
    });
  };

  const onTextChange = (text: string) => {
    setFormData({
      ...formData,
      text,
      err: null,
    });
  };

  const moderationView = useWithPostModerationView(closeThisWindow, error);
  const isSubmitting = loading || dndContext.isUploading;

  let subStateView = <></>;
  if (dndContext.uploadError) {
    subStateView = <>{t("errorCouldNotUploadImages")}</>;
  } else if (error && !moderationView) {
    const err = apolloErrorToString(error);
    subStateView = <>{err}</>;
  } else if (dndContext.isUploading) {
    subStateView = <>{t("wnd.imagesUploading")}</>;
  } else if (loading) {
    subStateView = <>{t("wnd.submitting")}</>;
  }

  if (formData.err) {
    subStateView = <>{t(formData.err)}</>;
  }

  return (
    <Container>
      <FileDropZone />
      {moderationView ? (
        moderationView
      ) : (
        <>
          <PostHeader>
            <PostTitle
              value={formData.title}
              placeholder={t("wnd.thread.title")}
              onChange={onTitleChange}
              disabled={isSubmitting}
            />
            <PostButton onClick={onPostBtnClick} disabled={isSubmitting}>
              {t("wnd.postSend")}
            </PostButton>
          </PostHeader>

          <Infoblock
            blockName="post-read-rules"
            translationKey="info.postReadRules"
          />

          <Editor
            value={formData.text}
            placeholder={t("wnd.thread.postText")}
            onChange={onTextChange}
            readonly={isSubmitting}
          />
          <ImgPicker />
          {subStateView}
        </>
      )}
    </Container>
  );
};

const Wrapper: React.FC<TProps> = (props) => {
  return (
    <ImgDndUpload>
      <NewThreadWindow {...props} />
    </ImgDndUpload>
  );
};

export default Wrapper;
