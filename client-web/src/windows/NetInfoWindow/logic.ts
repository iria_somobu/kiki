import { createAsyncThunk } from "@reduxjs/toolkit";

import i18next from "i18next";
import { TRootState } from "store/indexStore";
import {
  TOpenWindowBasePayload,
  TWindowBase,
  focusAndHighlightWindow,
  getId,
  wmSlice,
} from "store/windowManager";

export type TWindowData = {
  net: string;
};

export type TNetInfoWindow = TWindowBase<"netInfo", TWindowData>;

type TOpenPayload = TOpenWindowBasePayload<{
  net: string;
}>;

export const openNetInfoWindow = createAsyncThunk(
  "windowManager/openNetInfoWindowThunk",
  async (payload: TOpenPayload, { getState, dispatch }) => {
    const state = (getState() as TRootState).windowManager;

    const sameWindow = state.windows.find(
      ({ data, type }) => type === "netInfo" && data.net === payload.data.net
    );

    if (sameWindow === undefined) {
      dispatch(
        wmSlice.actions.openNewWindow({
          type: "netInfo",
          id: getId(),
          title: i18next.t("wnd.netInfoTitle", { net: payload.data.net }),
          presetName: "Small",
          ...payload,
        })
      );
    } else {
      dispatch(focusAndHighlightWindow(sameWindow.id));
    }
  }
);
