import LoadingError from "components/LoadingError";
import { graphql } from "gql";
import { useQueryWithoutUndefined, useToken } from "hooks";

import { TWindowData } from "./logic";
import { Container } from "./style";

type TProps = TWindowData;

const GQL_QUERY = graphql(`
  query NetInfoWindowQuery($token: String!, $net: String!) {
    extendedNetworkInfo(token: $token, net: $net)
  }
`);

const NetInfoWindow: React.FC<TProps> = ({ net }) => {
  const token = useToken();

  const { data, loading, error } = useQueryWithoutUndefined(GQL_QUERY, {
    variables: {
      token,
      net,
    },
  });

  let content;
  if (loading || error) content = <LoadingError error={error} short={true} />;
  else content = <>{data.extendedNetworkInfo}</>;

  return <Container>{content}</Container>;
};

export default NetInfoWindow;
