import styled from "styled-components";
import { BaseWindowContainer } from "windows/style";

export const Container = styled(BaseWindowContainer)`
  text-align: center;
`;
