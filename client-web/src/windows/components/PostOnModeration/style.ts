import styled from "styled-components";

export const OkBtn = styled.button`
  margin-top: 8px;
  height: 32px;
`;
