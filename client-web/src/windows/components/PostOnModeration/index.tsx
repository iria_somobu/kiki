import { ApolloError } from "@apollo/client";

import { Centered } from "App/style";
import { apolloErrorToString } from "utils";

import { OkBtn } from "./style";

type TProps = {
  onClose: () => void;
  error?: ApolloError;
};

const PostOnModeration: React.FC<TProps> = ({ onClose, error }) => {
  return (
    <Centered>
      {apolloErrorToString(error as ApolloError)}
      <OkBtn onClick={onClose}>Ok</OkBtn>
    </Centered>
  );
};

export default PostOnModeration;
