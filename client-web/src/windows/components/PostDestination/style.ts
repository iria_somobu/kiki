import { Link as BaseLink } from "react-router-dom";

import styled from "styled-components";

export const Link = styled(BaseLink)`
  margin-top: 16px;
  margin-bottom: 16px;
`;

export const BoardLink = styled(Link)`
  display: inline;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  width: 100%;
  height: 100%;
  flex-grow: 1;
`;
