import { Trans, useTranslation } from "react-i18next";

import { GQL_POST_QUERY } from "gql/queries";
import { useQueryWithoutUndefined } from "hooks";

import { BoardLink, Container, Link } from "./style";

type TProps = {
  token: string;
  postHash: string;
  net: string;
};

const PostDestination: React.FC<TProps> = ({ token, postHash, net }) => {
  const { t } = useTranslation();

  const { data, loading } = useQueryWithoutUndefined(GQL_POST_QUERY, {
    variables: {
      token,
      hash: postHash,
    },
  });

  if (loading) {
    return <div>{t("loading.short")}</div>;
  }

  const board = <BoardLink to={`/${net}/${data.post.board}`}></BoardLink>;

  return (
    <Container>
      <div>{t("postDestination.header")}</div>
      <Link to={`/${net}/${data.post.board}/${postHash}`}>
        {data.post.title}
      </Link>
      <div>
        <Trans
          i18nKey={"postDestination.board"}
          values={{ board: data.post.board }}
          components={{ board: board }}
        />
      </div>
    </Container>
  );
};

export default PostDestination;
