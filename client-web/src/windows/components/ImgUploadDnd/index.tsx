import FileDropZone from "./FileDropZone";
import ImgPicker from "./ImgPicker";
import {
  ImgDndUploadContext,
  TImgDndUploadContext,
  useImgDndUploadContext,
} from "./context";
import { Container } from "./style";
import useDndFilesHandling from "./useDndFilesHandling";

type TProps = React.PropsWithChildren;

const ImgDndUpload: React.FC<TProps> = ({ children }) => {
  const { files, removeFile, addFiles, uploadFiles, uploadError } =
    useDndFilesHandling();

  const isSomeUploading = files.some(
    (file) => file.status.status === "uploading"
  );

  const contextValue: TImgDndUploadContext = {
    uploadFiles: uploadFiles,
    uploadError,
    addFiles,
    removeFile,
    files,
    isUploading: uploadError ? false : isSomeUploading,
    // isUploading: true,
  };

  return (
    <ImgDndUploadContext.Provider value={contextValue}>
      <Container>{children}</Container>
    </ImgDndUploadContext.Provider>
  );
};

export default ImgDndUpload;

export { ImgDndUploadContext, useImgDndUploadContext, ImgPicker, FileDropZone };
