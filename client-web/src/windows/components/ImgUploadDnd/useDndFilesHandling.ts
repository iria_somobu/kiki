import { useCallback, useEffect, useState } from "react";

import { MAX_IMAGES_PER_POST } from "utils/constants";

import { TDndFile, TFileUploadStatus } from "../../types";

type TFileAjaxResult = {
  status: "ok" | "err";
  error?: string;
  url?: string;
};

const useDndFilesHandling = () => {
  const [files, setFiles] = useState<TDndFile[]>([]);
  const [uploadError, setUploadError] = useState<string | null>(null);

  const addFiles = (newFiles: File[]) => {
    const newArr = [...files];

    for (const newFile of newFiles) {
      if (/\.(jpe?g|png|gif)$/i.test(newFile.name)) {
        const alreadyIs = newArr.find(({ file }) => file.name === newFile.name);

        if (alreadyIs === undefined) {
          const link = URL.createObjectURL(newFile);

          newArr.push({
            file: newFile,
            url: link,
            status: {
              progress: 0,
              status: "added",
            },
          });
        }
      }
    }

    const filesToAdd = newArr.slice(0, MAX_IMAGES_PER_POST);

    setFiles(filesToAdd);
  };

  const removeFile = (url: string) => {
    const newFiles = files.filter((file) => file.url !== url);
    URL.revokeObjectURL(url);
    setFiles(newFiles);
  };

  const updateFileStatus = useCallback(
    (newStatus: TFileUploadStatus, index: number) => {
      setFiles((files) => {
        const newFiles = [...files];
        const oldFile = newFiles[index];
        newFiles[index].status = {
          ...oldFile,
          ...newStatus,
        };
        return newFiles;
      });
    },
    []
  );

  const uploadFileAjax = useCallback(
    (url: string, formData: FormData, fileIndex: number) => {
      const promise: Promise<TFileAjaxResult> = new Promise((resolve) => {
        // Set initial progress as 0 to block user from removing images
        updateFileStatus({ progress: 0, status: "uploading" }, fileIndex);

        const xhr = new XMLHttpRequest();

        xhr.upload.addEventListener("progress", (event) => {
          if (event.lengthComputable) {
            const progress = event.loaded / event.total;
            updateFileStatus(
              {
                progress,
                status: "uploading",
              },
              fileIndex
            );
          }
        });

        xhr.addEventListener("progress", (event) => {
          if (event.lengthComputable) {
            const progress = event.loaded / event.total;
            updateFileStatus(
              {
                progress,
                status: "uploading",
              },
              fileIndex
            );
          }
        });

        xhr.addEventListener("loadend", () => {
          if (xhr.readyState === 4 && xhr.status === 200) {
            resolve({
              status: "ok",
              url: xhr.responseText,
            });
          } else {
            resolve({
              status: "err",
              error: `Couldn't load image! Response cod ${xhr.status}`,
            });
          }
          updateFileStatus(
            {
              progress: 1,
              status: "uploaded",
            },
            fileIndex
          );
        });

        xhr.open("POST", url, true);
        xhr.send(formData);
      });

      return promise;
    },
    [updateFileStatus]
  );

  const uploadFiles = async (url: string) => {
    setUploadError(null);

    const promises: Promise<TFileAjaxResult>[] = [];
    for (let i = 0; i < files.length; ++i) {
      const file = files[i];

      const formData = new FormData();
      formData.append("file", file.file);

      promises.push(uploadFileAjax(url, formData, i));
    }

    const result = await Promise.all(promises);
    const noErrors = result.every((r) => r.status === "ok");
    if (noErrors) {
      return result.map((r) => r.url as string);
    } else {
      setUploadError("Some error text");
      setFiles((files) =>
        files.map((file) => {
          return {
            ...file,
            status: {
              status: "added",
              progress: 0,
            },
          };
        })
      );
      return null;
    }
  };

  useEffect(() => {
    return () => {
      setFiles((files) => {
        files.forEach((file) => URL.revokeObjectURL(file.url));
        return [];
      });
    };
  }, []);

  return {
    files,
    addFiles,
    removeFile,
    uploadFiles,
    uploadError,
  };
};

export default useDndFilesHandling;
