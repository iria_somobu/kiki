import NavPlusIcon from "img/close.svg";
import { TFileUploadStatus } from "windows/types";

import {
  Img,
  ImgContainer,
  ProgressBar,
  ProgressLine,
  RemoveImgIcon,
} from "./style";

type TProps = {
  uploadingStatus: TFileUploadStatus;
  onRemove: () => void;
  url: string;
  withRemoveIcon?: boolean;
};

const Image: React.FC<TProps> = ({
  uploadingStatus,
  onRemove,
  url,
  withRemoveIcon,
}) => {
  const progress = uploadingStatus.progress;

  let statusBar = null;

  const isUploading = uploadingStatus.status === "uploading";
  const isUploaded = uploadingStatus.status === "uploaded";
  if (isUploading || isUploaded) {
    statusBar = (
      <ProgressBar>
        <ProgressLine value={progress} isComplete={isUploaded} />
      </ProgressBar>
    );
  }

  return (
    <ImgContainer key={url}>
      <Img src={url} />
      {withRemoveIcon ? (
        <RemoveImgIcon onClick={onRemove} src={NavPlusIcon} />
      ) : null}
      {statusBar}
    </ImgContainer>
  );
};

export default Image;
