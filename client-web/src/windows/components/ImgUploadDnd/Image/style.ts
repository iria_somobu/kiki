import styled from "styled-components";

export const ProgressBar = styled.div`
  position: absolute;
  bottom: 4px;
  left: 4px;

  display: flex;
  align-items: center;

  width: calc(100% - 8px);
  height: 12px;
  border-radius: 6px;
  overflow: hidden;

  background-color: white;
  border: 1px solid var(--totally-black);
`;

type TDoneLineProps = {
  value: number;
  isComplete?: boolean;
};

export const ProgressLine = styled.div<TDoneLineProps>`
  display: block;

  width: ${({ value }) => `${Math.ceil(Math.max(value * 100, 12))}%`};

  margin: 0 2px;
  padding: 0;

  height: 8px;
  border-radius: 4px;

  background-color: ${({ isComplete }) =>
    isComplete ? "var(--dk-grey)" : "var(--dk-blue)"};
`;

export const Img = styled.img`
  display: block;
  height: 64px;
  width: 64px;

  object-fit: cover;
`;

export const RemoveImgIcon = styled.img`
  visibility: hidden;

  position: absolute;
  top: 0;
  right: 0;

  width: 24px;
  height: 24px;
  padding: 4px;

  background: var(--card-grey);
  border-radius: 24px;
`;

export const ImgContainer = styled.div`
  position: relative;
  margin: 0 2px;

  &:first-of-type {
    margin-left: 0;
  }

  &:last-of-type {
    margin-right: 0;
  }

  &:hover {
    cursor: pointer;

    ${RemoveImgIcon} {
      visibility: visible;
    }
  }
`;
