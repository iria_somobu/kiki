import styled from "styled-components";

export const Container = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 1;

  display: flex;
  flex-direction: column;
  padding: 16px;

  background: white;
`;

export const Zone = styled.div`
  flex-grow: 1;
  border: 3px dashed lightblue;
  border-radius: 24px;
  background: white;

  display: flex;
  justify-content: center;
  align-items: center;
`;
