import { useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";

import { useImgDndUploadContext } from "../context";
import { Container, Zone } from "./style";

const FileDropZone: React.FC = () => {
  const [isOverWindow, setIsOverWindow] = useState(false);
  const [isOverDropzone, setIsOverDropzone] = useState(false);
  const entersLeavesCounter = useRef(0);
  const dropzoneRef = useRef(null);

  const { t } = useTranslation();
  const dndContext = useImgDndUploadContext();

  useEffect(() => {
    const el = window;

    const onDragOver = (e: DragEvent) => {
      if (isOverWindow === false) {
        setIsOverWindow(true);
      }
      e.preventDefault();
    };

    const onDragStart = () => {
      setIsOverWindow(true);
    };

    const onDragEnter = (e: DragEvent) => {
      if (e.target === dropzoneRef.current) {
        setIsOverDropzone(true);
      }

      if (e.currentTarget === el) {
        entersLeavesCounter.current += 1;
      }
      setIsOverWindow(true);
    };

    const onDragEnd = () => {
      setIsOverWindow(false);
    };

    const onDrop = (e: DragEvent) => {
      if (e.target === dropzoneRef.current) {
        if (e.dataTransfer && e.dataTransfer.files) {
          const files = e.dataTransfer.files;
          dndContext.addFiles(Array.from(files));
        }
      }
      setIsOverWindow(false);
    };

    const onDragLeave = (e: DragEvent) => {
      if (e.target === dropzoneRef.current) {
        setIsOverDropzone(false);
      }

      if (e.currentTarget === el) {
        entersLeavesCounter.current -= 1;

        if (entersLeavesCounter.current === 0) {
          setIsOverWindow(false);
        }
      }
    };

    el.addEventListener("dragover", onDragOver, true);
    el.addEventListener("dragstart", onDragStart, true);
    el.addEventListener("dragenter", onDragEnter, true);
    el.addEventListener("dragend", onDragEnd, true);
    el.addEventListener("dragleave", onDragLeave, true);
    el.addEventListener("drop", onDrop, true);

    return () => {
      el.removeEventListener("dragover", onDragOver, true);
      el.removeEventListener("dragstart", onDragStart, true);
      el.removeEventListener("dragenter", onDragEnter, true);
      el.removeEventListener("dragend", onDragEnd, true);
      el.removeEventListener("dragleave", onDragLeave, true);
      el.removeEventListener("drop", onDrop, true);
    };
  }, [isOverWindow, dndContext]);

  const text = isOverDropzone
    ? t("imgDnd.dropzone.dropHere")
    : t("imgDnd.dropzone.dragHere");

  if (!isOverWindow || dndContext.isUploading) {
    return null;
  }

  return (
    <Container>
      <Zone ref={dropzoneRef}>{text}</Zone>
    </Container>
  );
};

export default FileDropZone;
