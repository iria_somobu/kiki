import styled from "styled-components";

export const Container = styled.div`
  margin: 0 4px;
`;

export const ImgList = styled.div`
  display: flex;
  flex-direction: row;
  padding-bottom: 6px;
`;

export const HintBlock = styled.div`
  margin-top: 8px;
  margin-bottom: 4px;

  font-size: 14px;
`;

export const PseudoLink = styled.span`
  color: #2900ff;
  cursor: pointer;

  :hover {
    color: var(--dk-blue);
  }
`;
