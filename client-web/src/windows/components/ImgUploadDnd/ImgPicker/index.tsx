import { useMemo, useRef, useState } from "react";
import { useTranslation } from "react-i18next";

import ScrollableContent from "components/ScrollableContent";

import Image from "../Image";
import { useImgDndUploadContext } from "../context";
import { Container, HintBlock, ImgList, PseudoLink } from "./style";

const ImgPicker: React.FC = () => {
  const inputRef = useRef<HTMLInputElement>(null);
  const [isAttachVisible, setIsAttachVisible] = useState(true);

  const { t } = useTranslation();
  const dndContext = useImgDndUploadContext();

  const onInputChange = () => {
    if (inputRef.current !== null && inputRef.current.files) {
      dndContext.addFiles(Array.from(inputRef.current.files));
    }
  };

  const onFilePick = () => {
    if (inputRef.current != null) inputRef.current.click();
  };

  const onDisplayAttachments = () => {
    setIsAttachVisible(!isAttachVisible);
  };

  const files = useMemo(
    () =>
      dndContext.files.map(({ url, status }) => ({
        url,
        status,
      })),
    [dndContext.files]
  );

  const inputHint = (
    <>
      <PseudoLink onClick={onFilePick}>
        {t("imgDnd.upload.pickFiles.link")}
      </PseudoLink>{" "}
      {t("imgDnd.upload.pickFiles.rest")}
    </>
  );

  let attachmentsHint = null;
  if (files.length > 0) {
    attachmentsHint = (
      <>
        <PseudoLink onClick={onDisplayAttachments}>
          {isAttachVisible
            ? t("imgDnd.upload.hideAttach")
            : t("imgDnd.upload.showAttach")}
        </PseudoLink>
      </>
    );
  }

  let hintBlockContent = null;
  if (dndContext.isUploading) {
    hintBlockContent = files.length > 0 ? attachmentsHint : null;
  } else {
    hintBlockContent =
      files.length > 0 ? (
        <>
          {inputHint} | {attachmentsHint}
        </>
      ) : (
        inputHint
      );
  }

  return (
    <Container>
      <HintBlock>{hintBlockContent}</HintBlock>
      {isAttachVisible ? (
        <ScrollableContent isHorizontal autoHeight>
          <ImgList>
            {files.map((file) => {
              return (
                <Image
                  key={file.url}
                  uploadingStatus={file.status}
                  onRemove={() => dndContext.removeFile(file.url)}
                  url={file.url}
                  withRemoveIcon={file.status.status === "added"}
                />
              );
            })}
          </ImgList>
        </ScrollableContent>
      ) : null}
      <input
        type="file"
        ref={inputRef}
        style={{ display: "none" }}
        onChange={onInputChange}
        multiple
      />
    </Container>
  );
};

export default ImgPicker;
