import { createContext, useContext } from "react";

import { TDndFile } from "windows/types";

export type TImgDndUploadContext = {
  uploadFiles: (url: string) => Promise<string[] | null>;
  uploadError: string | null;
  addFiles: (files: File[]) => void;
  removeFile: (url: string) => void;
  files: TDndFile[];
  isUploading: boolean;
};

export const ImgDndUploadContext = createContext<TImgDndUploadContext | null>(
  null
);

export const useImgDndUploadContext = () => {
  const context = useContext(ImgDndUploadContext);
  if (context === null) {
    throw new Error(
      "Trying to use img dnd upload context outside its context provider!"
    );
  }
  return context;
};
