import styled from "styled-components";

export const CommonButton = styled.button`
  border: none;
  border-radius: 6px;
`;

export const BaseWindowContainer = styled.div`
  display: flex;
  flex-grow: 1;
  flex-direction: column;
  overflow: hidden;
`;

export const PostButton = styled(CommonButton)`
  padding: 6px 12px;
`;
