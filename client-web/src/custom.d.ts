declare module "*.svg" {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const content: any;
  export default content;

  export const ReactComponent: React.SFC<React.SVGProps<SVGSVGElement>>;
}
