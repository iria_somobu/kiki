import { useEffect } from "react";

import LoadingError from "components/LoadingError";
import { useDispatch, useSelector } from "hooks";
import { auth } from "store/tokenHolder";

type TProps = {
  children: React.ReactNode;
};

const TokenValidator: React.FC<TProps> = ({ children }) => {
  const dispatch = useDispatch();

  const status = useSelector((state) => state.token.status);

  useEffect(() => {
    dispatch(auth());
  }, [dispatch]);

  if (status === "authorized") {
    return <>{children}</>;
  }

  const error =
    status === "failed"
      ? "Can't get token from the server! This is critical error!"
      : undefined;

  return <LoadingError errMsg={error} fsize={true} />;
};

export default TokenValidator;
