import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import TokenValidator from "./TokenValidator";

const Greeting = React.lazy(() => import("App/CentralGreeting"));
const Layout = React.lazy(() => import("App/Layout"));
const PostHoverManager = React.lazy(() => import("App/PostHoverManager"));
const WindowManager = React.lazy(() => import("App/WindowManager"));

const App: React.FC = () => {
  return (
    <TokenValidator>
      <BrowserRouter basename={"/"}>
        <Routes>
          <Route path="/" element={<Greeting />} />
          <Route path="*" element={<Layout />} />
        </Routes>

        <Routes>
          <Route path="/:net/:board/:thread" element={<PostHoverManager />} />
          <Route path="*" element={<></>} />
        </Routes>

        <Routes>
          <Route path="/:net/*" element={<WindowManager />} />
          <Route path="*" element={null} />
        </Routes>
      </BrowserRouter>
    </TokenValidator>
  );
};

export default App;
