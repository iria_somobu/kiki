import { useTranslation } from "react-i18next";

import Infoblock from "components/Infoblock";
import LoadingError from "components/LoadingError";
import { GQL_AVAILABLE_NETWORKS_QUERY } from "gql/queries";
import { useQueryWithoutUndefined, useToken } from "hooks";
import { imgToUrlScaled } from "utils/routes";

import { Container, NetDarker, Network, Networks } from "./style";

const Greeting: React.FC = () => {
  const token = useToken();

  const { loading, error, data } = useQueryWithoutUndefined(
    GQL_AVAILABLE_NETWORKS_QUERY,
    {
      variables: {
        token,
      },
    }
  );

  const { t } = useTranslation();

  if (loading || error) return <LoadingError error={error} fsize={true} />;

  const networks = [...data.networks].sort((a, b) =>
    a.title.localeCompare(b.title)
  );

  return (
    <Container>
      <p>{t("greeting.welcome")}</p>
      <Infoblock
        blockName="central-quick-links"
        translationKey="info.quickLinks"
      />
      <p>{t("greeting.haveNets")}</p>
      <Networks>
        {networks.map((item) => (
          <Network
            key={item.id}
            to={item.id}
            style={{
              backgroundImage: "url(" + imgToUrlScaled(item.icon, "240"),
            }}
          >
            <NetDarker>
              <span>{item.title}</span>
            </NetDarker>
          </Network>
        ))}
      </Networks>
    </Container>
  );
};

export default Greeting;
