import { Link } from "react-router-dom";

import styled from "styled-components";

export const Container = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  row-gap: 18px;
`;

export const Networks = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  gap: 8px;
`;

export const NetDarker = styled.div`
  width: 100%;
  height: 100%;

  display: flex;
  justify-content: center;
  align-items: center;

  border-radius: 6px;
  background-color: rgba(255, 255, 255, 0.4);

  & > span {
    padding: 16px;
    font-weight: 500;
    color: var(--dk-blue);
  }
`;

export const Network = styled(Link)`
  width: 152px;
  height: 92px;
  text-decoration: none;
  cursor: pointer;

  border-radius: 8px;
  background-size: cover;
  background-position: center center;

  &:hover {
    border: 2px solid var(--dk-blue);
  }

  &:hover ${NetDarker} {
    background-color: rgba(255, 255, 255, 0.75);
  }
`;
