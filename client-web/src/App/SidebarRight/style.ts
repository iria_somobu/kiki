import BaseSidebar from "components/Sidebar";
import styled from "styled-components";

export const Sidebar = styled(BaseSidebar)`
  box-sizing: content-box;
  border-left: 1px solid var(--card-grey);
`;

export const LoginRef = styled.span`
  color: var(--col-link);
  cursor: pointer;

  :hover {
    color: var(--col-link-active);
  }
`;
