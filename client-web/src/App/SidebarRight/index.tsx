import React from "react";
import { useTranslation } from "react-i18next";

import { useDispatch, useSelector, useWindowSizeSubscribe } from "hooks";
import { sidebarSlice } from "store/sidebarSwitcher";
import { openLoginWindow } from "store/windowManager";
import { Breakpoints } from "utils/constants";

import { LoginRef, Sidebar } from "./style";

const RightSidebar: React.FC = () => {
  const barState = useSelector((state) => state.sidebar.mode);
  const isNamed = useSelector((state) => state.token.isNamed);

  const [windowWidth] = useWindowSizeSubscribe();

  const dispatch = useDispatch();
  const { t } = useTranslation();

  const onLoginRefClick = (e: React.MouseEvent) => {
    dispatch(
      openLoginWindow({
        initialX: e.clientX,
        initialY: e.clientY,
        data: null,
      })
    );
  };

  let content;
  if (isNamed) {
    content = (
      <>
        Sorry, post watcher
        <br />
        is not ready yet
      </>
    );
  } else {
    content = (
      <>
        <span>
          {t("rbar.signIn.pre")}{" "}
          <LoginRef onClick={onLoginRefClick}>{t("rbar.signIn.btn")}</LoginRef>
        </span>

        <span>{t("rbar.signIn.post")}</span>
      </>
    );
  }

  const shouldBePersistent = Breakpoints.mobile.value < windowWidth;

  return (
    <Sidebar
      visible={barState === "right"}
      placement="right"
      persistent={shouldBePersistent}
      onClose={() => dispatch(sidebarSlice.actions.hide())}
    >
      {content}
    </Sidebar>
  );
};

export default RightSidebar;
