import styled from "styled-components";
import { Breakpoints } from "utils/constants";

export const Root = styled.header`
  display: flex;
  justify-content: space-between;

  height: calc(var(--topbar-height) - 16px + 16px);
  background-color: var(--card-white);

  padding: 8px;

  @media (min-width: ${Breakpoints.mobile.px}) {
    display: none;
    left: var(--sidebar-width);
    right: var(--sidebar-width);
  }
`;

export const Btn = styled.button`
  width: 96px;
  height: 32px;

  padding: 8px;

  border: 0;
  border-radius: 8px;
  background: var(--card-grey);

  text-decoration: none;
  color: var(--dk-blue);

  font-weight: 500;
  font-size: 12px;
  text-align: center;

  cursor: pointer;

  &:hover {
    background: var(--grey);
  }
`;
