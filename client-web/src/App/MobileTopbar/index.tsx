import { useTranslation } from "react-i18next";

import { useDispatch } from "hooks";
import { sidebarSlice } from "store/sidebarSwitcher";

import { Btn, Root } from "./style";

const MobileTopbar: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  return (
    <Root>
      <Btn onClick={() => dispatch(sidebarSlice.actions.toggleLeft())}>
        {t("nav.toggleLeft")}
      </Btn>
      <Btn onClick={() => dispatch(sidebarSlice.actions.toggleRight())}>
        {t("nav.toggleRight")}
      </Btn>
    </Root>
  );
};

export default MobileTopbar;
