import React, { useLayoutEffect, useRef, useState } from "react";

import WndClose from "img/wnd_close.svg";
import WndFullScreen from "img/wnd_fullscreen.svg";
import WndMaximize from "img/wnd_maximize.svg";
import WndMinimize from "img/wnd_minimize.svg";
import { isMobileWidth } from "utils/constants";

import { Content, Header, HeaderButton, Wnd, WndControls } from "./style";

export type TPresetName = "Default" | "Small" | "Big" | "Fullscreen";

type TWindowSize = {
  width: number;
  height: number;
};

export const Presets: { [key in TPresetName]: TWindowSize } = {
  Default: {
    width: 480,
    height: 420,
  },
  Small: {
    width: 356,
    height: 320,
  },
  Big: {
    width: 720,
    height: 560,
  },
  Fullscreen: {
    width: 0,
    height: 0,
  },
};

type TProps = {
  title: string;
  onClose: () => void;
  children: React.ReactNode;
  initialX?: number;
  initialY?: number;
  onShouldFocus?: () => void;
  isHighlighted: boolean;
  presetName?: TPresetName;
};

type TState = {
  posX: number;
  posY: number;
  top: number;
  left: number;
  shown: boolean;
  height: number;
  width: number;
  presetName: TPresetName;
  fullscreen: boolean;
};

const calculateWindowPos = (
  left: number,
  top: number,
  windowWidth: number,
  windowHeight: number
) => {
  // Cap at left/top
  let cappedX = Math.max(0, left);
  let cappedY = Math.max(0, top);

  cappedX = Math.min(window.innerWidth - windowWidth, cappedX);
  cappedY = Math.min(window.innerHeight - windowHeight, cappedY);

  return {
    left: cappedX,
    top: cappedY,
  };
};

const FloatingWindow: React.FC<TProps> = ({
  title,
  children,
  onClose,
  onShouldFocus,
  initialX,
  initialY,
  isHighlighted,
  presetName = "Default",
}) => {
  const windowRef = useRef<HTMLDivElement>(null);
  const visibilityBtnRef = useRef<HTMLImageElement>(null);

  const [state, setState] = useState<TState>(() => {
    const defaultTop = 12;

    const windowWidth = Presets[presetName].width;
    const defaultLeft = window.innerWidth - windowWidth - 12;

    const top = initialY ? initialY : defaultTop;
    const left = initialX ? initialX : defaultLeft;

    return {
      posX: left,
      posY: top,
      top,
      left,
      shown: true,
      height: 0,
      width: 0,
      presetName,
      fullscreen: false,
    };
  });

  const windowWidth = Presets[state.presetName].width;
  const windowHeight = Presets[state.presetName].height;

  useLayoutEffect(() => {
    if (windowRef.current === null) {
      return;
    }

    const windowHeight = windowRef.current.offsetHeight;
    const windowWidth = windowRef.current.offsetWidth;

    if (state.height === windowHeight && state.width === windowWidth) {
      return;
    }

    const { top, left } = calculateWindowPos(
      state.left,
      state.top,
      windowWidth,
      windowHeight
    );

    setState({
      ...state,
      height: windowHeight,
      width: windowWidth,
      top,
      left,
    });
  }, [state]);

  const closeDragElement = () => {
    document.onmouseup = null;
    document.onmousemove = null;
  };

  const elementDrag = (event: MouseEvent) => {
    event = event || window.event;
    event.preventDefault();
    event.stopPropagation();

    if (state.fullscreen) {
      return;
    }

    requestAnimationFrame(() =>
      setState((prevState) => {
        const dX = prevState.posX - event.clientX;
        const dY = prevState.posY - event.clientY;

        const leftValue = prevState.left - dX;
        const topValue = prevState.top - dY;

        const { top, left } = calculateWindowPos(
          leftValue,
          topValue,
          prevState.width,
          prevState.height
        );

        return {
          ...prevState,
          posX: event.clientX,
          posY: event.clientY,
          left,
          top,
        };
      })
    );
  };

  const onWindowMouseDown = () => {
    onShouldFocus?.();
  };

  const onHeaderMouseDown = (event: React.MouseEvent<HTMLDivElement>) => {
    event = event || window.event;
    event.preventDefault();

    if (event.target === visibilityBtnRef.current) {
      return;
    }

    requestAnimationFrame(() =>
      setState((prevState) => {
        return {
          ...prevState,
          posX: event.clientX,
          posY: event.clientY,
        };
      })
    );

    document.onmouseup = closeDragElement;
    document.onmousemove = elementDrag;
  };

  const toggleVisible = () => {
    setState({ ...state, shown: !state.shown });
  };

  const toggleFullScreen = () => {
    if (state.shown) {
      setState({ ...state, fullscreen: !state.fullscreen });
    }
  };

  const icon = state.shown ? WndMinimize : WndMaximize;
  const display = state.shown ? "flex" : "none";

  const calculatedStyle = {
    transform: "translate( " + state.left + "px, " + state.top + "px)",
  };

  const isFullscreenStyle = isMobileWidth() || state.fullscreen;

  return (
    <Wnd
      style={calculatedStyle}
      ref={windowRef}
      isHighlighted={isHighlighted}
      shown={state.shown}
      width={windowWidth}
      height={windowHeight}
      fullscreen={isFullscreenStyle}
      onMouseDown={onWindowMouseDown}
    >
      <Header onMouseDown={onHeaderMouseDown} fullscreen={isFullscreenStyle}>
        <p>{title}</p>
        <WndControls>
          <HeaderButton
            src={icon}
            onClick={toggleVisible}
            ref={visibilityBtnRef}
          />
          <HeaderButton src={WndFullScreen} onClick={toggleFullScreen} />
          <HeaderButton src={WndClose} onClick={onClose} />
        </WndControls>
      </Header>
      <Content style={{ display }}>{children}</Content>
    </Wnd>
  );
};

export default FloatingWindow;
