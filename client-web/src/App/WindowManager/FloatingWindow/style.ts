import { BaseIcon } from "App/style";
import styled from "styled-components";
import { Breakpoints } from "utils/constants";

type TWndProps = {
  isHighlighted: boolean;
  shown: boolean;
  width: number;
  height: number;
  fullscreen: boolean;
};

export const Wnd = styled.div<TWndProps>`
  display: flex;
  flex-direction: column;

  position: absolute;
  background-color: var(--totally-white);
  pointer-events: all;
  ${({ isHighlighted }) =>
    isHighlighted
      ? "box-shadow: 0 4px 8px 0 var(--lt-blue), 0 6px 20px 0 var(--lt-blue);"
      : "box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"}};

  @media (max-width: ${Breakpoints.mobile.px}) {
    top: 0;
    left: 0;
    right: 0;

    transform: none !important;
    max-height: calc(100% - 1px);
    height: ${({ shown }) => {
      return shown ? "100%" : "fit-content";
    }};
  }

  @media (min-width: ${Breakpoints.mobile.px}) {
    width: ${({ fullscreen, width }) => (fullscreen ? "100%" : `${width}px`)};
    border: 1px solid black;
    border-radius: ${({ fullscreen }) =>
      fullscreen ? "none" : "9px 9px 6px 6px"};

    height: ${({ fullscreen, shown, height }) => {
      if (shown) {
        return fullscreen ? "100%" : `${height}px`;
      } else {
        return "fit-content";
      }
    }};
  }
`;

type THeaderProps = {
  fullscreen: boolean;
};

export const Header = styled.div<THeaderProps>`
  display: flex;
  justify-content: space-between;
  flex-shrink: 0;

  height: 48px;
  padding-left: 16px;
  padding-right: 8px;

  align-items: center;

  color: var(--totally-white);
  background-color: var(--dk-blue);
  cursor: move;

  border-top-left-radius: ${({ fullscreen }) => (fullscreen ? "0px" : "6px")};
  border-top-right-radius: ${({ fullscreen }) => (fullscreen ? "0px" : "6px")};

  & > p {
    display: block;
    height: 32px;

    font-weight: 700;
    text-overflow: ellipsis;
    overflow: hidden;
    line-height: 32px;
  }

  // Mobile mode quickfix
  @media (max-width: ${Breakpoints.mobile.px}) {
    height: 32px;
  }
`;

export const HeaderButton = styled(BaseIcon)`
  padding: 6px;
  cursor: pointer;

  :hover {
    border-radius: 6px;
    background-color: var(--lt-blue);
  }
`;

export const Content = styled.div`
  display: flex;
  flex-grow: 1;
  align-items: stretch;
  flex-direction: column;
  overflow: hidden;

  border-bottom-left-radius: 6px;
  border-bottom-right-radius: 6px;
`;

export const WndControls = styled.div`
  display: flex;
`;
