import { useSelector } from "hooks";

import Window from "./Window";
import { WndContainer } from "./style";

const WindowManager: React.FC = () => {
  const { windows, highlighted } = useSelector((state) => state.windowManager);

  return (
    <WndContainer>
      {windows.map((window) => (
        <Window
          key={window.id}
          window={window}
          isHighlighted={window.id === highlighted}
        />
      ))}
    </WndContainer>
  );
};

export default WindowManager;
