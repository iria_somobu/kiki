import styled from "styled-components";
import { ZIndices } from "utils/constants";

export const WndContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  z-index: ${ZIndices.windows};
  pointer-events: none;
`;
