import FloatingWindow from "App/WindowManager/FloatingWindow";
import { useDispatch } from "hooks";
import { TWindow, wmSlice } from "store/windowManager";
import { LoginWindow } from "windows/LoginWindow";
import NetInfoWindow from "windows/NetInfoWindow";
import NewPostWindow from "windows/NewPostWindow";
import NewThreadWindow from "windows/NewThreadWindow";

type TWindowProps = {
  window: TWindow;
  isHighlighted: boolean;
};

const Window: React.FC<TWindowProps> = (props) => {
  const { window, isHighlighted } = props;
  const { id, type, data, initialX, initialY } = window;
  const dispatch = useDispatch();

  let wnd;
  switch (type) {
    case "newPost":
      wnd = <NewPostWindow {...window} />;
      break;
    case "newThread":
      wnd = <NewThreadWindow {...window} />;
      break;
    case "netInfo":
      wnd = <NetInfoWindow {...data} />;
      break;
    case "login":
      wnd = <LoginWindow />;
      break;
    default:
      throw new Error("Unknown window type!");
  }

  const onWindowClose = () =>
    dispatch(
      wmSlice.actions.close({
        id,
      })
    );

  const focusWindow = () => {
    dispatch(
      wmSlice.actions.focusWindow({
        id,
      })
    );
  };

  return (
    <FloatingWindow
      title={window.title ?? "Default string"}
      onClose={onWindowClose}
      onShouldFocus={focusWindow}
      initialX={initialX}
      initialY={initialY}
      isHighlighted={isHighlighted}
      presetName={window.presetName ?? "Default"}
    >
      {wnd}
    </FloatingWindow>
  );
};

export default Window;
