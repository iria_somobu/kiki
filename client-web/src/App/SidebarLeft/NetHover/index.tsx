import { useTranslation } from "react-i18next";

import { graphql } from "gql";
import { useQueryWithoutUndefined, useToken } from "hooks";
import { imgToUrlScaled } from "utils/routes";

import { Container, Entry, EntryIcon, EntryText } from "./style";

const GQL_NETWORKS_QUERY = graphql(`
  query Networks($token: String!) {
    networks(token: $token) {
      id
      title
      icon
    }
  }
`);

const NetHover: React.FC = () => {
  const token = useToken();
  const { loading, error, data } = useQueryWithoutUndefined(
    GQL_NETWORKS_QUERY,
    {
      variables: {
        token,
      },
    }
  );

  const { t } = useTranslation();

  let content = null;
  if (loading) {
    content = t("loading.long");
  } else if (error) {
    content = t("error", { message: error.message });
  } else {
    const networks = [...data.networks].sort((a, b) =>
      a.title.localeCompare(b.title)
    );
    content = networks.map((item) => (
      <Entry key={item.id} to={"/" + item.id}>
        <EntryIcon src={imgToUrlScaled(item.icon, "240")} />
        <EntryText>{item.title}</EntryText>
      </Entry>
    ));
  }

  return <Container>{content}</Container>;
};

export default NetHover;
