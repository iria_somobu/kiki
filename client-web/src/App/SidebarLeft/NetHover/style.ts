import { Link } from "react-router-dom";

import styled from "styled-components";

export const Container = styled.div`
  cursor: pointer;
  width: 232px;
  color: var(--totally-white);
  text-align: center;
`;

export const Entry = styled(Link)`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 48px;

  padding: 4px 16px;

  text-decoration: none;
  cursor: pointer;

  :hover {
    background-color: var(--dk-blue-2);
  }
`;

export const EntryIcon = styled.img`
  width: 40px;
  height: 40px;

  border-radius: 20px;
  margin-right: 16px;
`;

export const EntryText = styled.span`
  font-weight: 500;
  font-size: 12px;

  color: #ffffff;
`;
