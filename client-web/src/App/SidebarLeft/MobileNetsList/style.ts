import { Link } from "react-router-dom";

import styled from "styled-components";

export const Container = styled.div`
  cursor: pointer;
  text-align: center;
  border-radius: 8px;
`;

export const Entry = styled(Link)`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 48px;

  padding: 4px 4px;

  text-decoration: none;
  cursor: pointer;

  :first-child {
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
  }

  :last-child {
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
  }

  background-color: var(--card-grey);

  :hover {
    background-color: var(--grey);
  }
`;

export const EntryIcon = styled.img`
  width: 40px;
  height: 40px;

  border-radius: 8px;
  margin-right: 12px;
`;

export const EntryText = styled.span`
  font-weight: 500;
  font-size: 12px;

  color: var(--dk-blue);
`;
