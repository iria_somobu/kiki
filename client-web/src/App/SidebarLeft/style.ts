import BaseSidebar from "components/Sidebar";
import styled from "styled-components";

export const Sidebar = styled(BaseSidebar)`
  box-sizing: content-box;
  align-items: stretch;
  border-right: 1px solid var(--card-grey);
`;
