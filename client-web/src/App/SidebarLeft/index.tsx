import React from "react";

import LoadingError from "components/LoadingError";
import { graphql } from "gql";
import {
  useDispatch,
  useParamsWithoutUndefined,
  useQueryWithoutUndefined,
  useSelector,
  useToken,
  useWindowSizeSubscribe
} from "hooks";
import { sidebarSlice } from "store/sidebarSwitcher";
import { Breakpoints } from "utils/constants";

import BoardsList from "./BoardsList";
import NetworkInfo from "./NetworkInfo";
import Stats from "./Stats";
import { Sidebar } from "./style";

const GQL_QUERY = graphql(`
  query SidebarLeftQuery($token: String!, $net: String!) {
    network(token: $token, net: $net) {
      id
      title
      descr
      icon
    }
    boardGroups(token: $token, net: $net) {
      name
      position
      boards {
        name
        position
        descr
        lastUpdateTime
      }
    }
  }
`);

const SidebarLeft: React.FC = () => {
  const { net } = useParamsWithoutUndefined();
  const token = useToken();

  const [windowWidth] = useWindowSizeSubscribe();

  const { loading, error, data } = useQueryWithoutUndefined(GQL_QUERY, {
    variables: {
      net,
      token,
    },
    pollInterval: 60 * 1000,
  });

  const barState = useSelector((state) => state.sidebar.mode);
  const dispatch = useDispatch();

  let content;
  if (loading || error) {
    content = <LoadingError error={error} short={true} />;
  } else {
    content = (
      <>
        <NetworkInfo
          net={net}
          src={data.network.icon}
          title={data.network.title}
          desc={data.network.descr}
        />
        <BoardsList groups={data.boardGroups} />
        <Stats />
      </>
    );
  }

  const shouldBePersistent = Breakpoints.mobile.value < windowWidth;

  return (
    <Sidebar
      visible={barState === "left"}
      placement="left"
      persistent={shouldBePersistent}
      onClose={() => dispatch(sidebarSlice.actions.hide())}
    >
      {content}
    </Sidebar>
  );
};

export default SidebarLeft;
