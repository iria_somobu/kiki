import Scrollbars from "react-custom-scrollbars-2";

import styled from "styled-components";

export const BoardsListRoot = styled(Scrollbars)`
  height: auto;
  flex: 1;
  overflow-y: scroll;
`;
