import BoardGroup, { TBoardGroupType } from "../BoardGroup";
import { BoardsListRoot } from "./style";

type TProps = {
  groups: TBoardGroupType[];
};

const BoardsList: React.FC<TProps> = ({ groups }) => {
  return (
    <BoardsListRoot>
      {groups.map((group) => (
        <BoardGroup key={group.name} group={group} />
      ))}
    </BoardsListRoot>
  );
};

export default BoardsList;
