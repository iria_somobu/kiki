import { useTranslation } from "react-i18next";

import Tooltip from "components/Tooltip";
import { graphql } from "gql";
import { useQueryWithoutUndefined, useToken } from "hooks";
import MessageIcon from "img/message.svg";
import NetMemb from "img/net_memb.svg";
import Settings from "img/settings.svg";

import {
  OnlineBlock,
  PostRateBlock,
  StatsBox,
  StatsIcon,
  StatsRoot,
  StatsText,
  StatsUnit,
} from "./style";

const GQL_QUERY = graphql(`
  query NetStats($token: String!) {
    netStats(token: $token) {
      online
      postRate
    }
  }
`);

const Stats: React.FC = () => {
  const token = useToken();

  const { t } = useTranslation();

  const { data } = useQueryWithoutUndefined(GQL_QUERY, {
    variables: { token },
    pollInterval: 60 * 1000,
  });

  let online = "-";
  let postRate = "-";

  if (data) {
    online = "" + data.netStats.online;
    postRate = "" + data.netStats.postRate;
  }

  const align = {
    points: ["bc", "tc"],
  };

  return (
    <StatsRoot>
      <StatsBox>
        <Tooltip
          overlay={<div>{t("leftSidebar.stats.online.hover")}</div>}
          overlayStyle={{
            paddingLeft: "8px",
          }}
          align={align}
          hideOnOverlayHover
        >
          <OnlineBlock>
            <StatsIcon src={NetMemb} />
            <StatsText>{online}</StatsText>
          </OnlineBlock>
        </Tooltip>
        <Tooltip
          overlay={<div>{t("leftSidebar.stats.postRate.hover")}</div>}
          align={align}
          hideOnOverlayHover
        >
          <PostRateBlock>
            <StatsIcon src={MessageIcon} />
            <StatsText>
              {postRate}{" "}
              <StatsUnit>/{t("leftSidebar.stats.postRate.unit")}</StatsUnit>
            </StatsText>
          </PostRateBlock>
        </Tooltip>
        <Tooltip
          overlay={<div>{t("leftSidebar.stats.noSettings")}</div>}
          align={align}
          hideOnOverlayHover
        >
          <StatsIcon src={Settings} />
        </Tooltip>
      </StatsBox>
    </StatsRoot>
  );
};

export default Stats;
