import { BaseIcon, RoundBox } from "App/style";
import styled from "styled-components";

export const StatsRoot = styled.div`
  padding: 8px;
  border-top: 1px solid var(--card-grey);
`;

export const OnlineBlock = styled.div`
  display: flex;
  align-items: center;
`;

export const StatsBox = styled(RoundBox)`
  display: flex;
  padding: 8px;
  justify-content: space-between;
  align-items: center;
`;

export const PostRateBlock = styled(OnlineBlock)`
  display: flex;
  align-items: centter;
`;

export const StatsIcon = BaseIcon;

export const StatsText = styled.span`
  flex-grow: 1;
  font-weight: 500;
  font-size: 14px;
  color: var(--dk-blue);
`;

export const StatsUnit = styled.span`
  font-weight: 500;
  font-size: 12px;
`;
