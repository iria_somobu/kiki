import { useState } from "react";

import { NetworkButton, Spacer } from "App/style";
import Tooltip from "components/Tooltip";
import { useDispatch } from "hooks";
import ArrowIcon from "img/arrow.svg";
import InfoIcon from "img/info.svg";
import { openNetInfoWindow } from "store/windowManager";
import { isMobileHeight, isTouch } from "utils/constants";
import { imgToUrlScaled } from "utils/routes";

import MobileNetsList from "../MobileNetsList";
import NetHover from "../NetHover";
import {
  NetworkAvatar,
  NetworkBox,
  NetworkBoxPadded,
  NetworkButtons,
  NetworkDesc,
  NetworkRoot,
  NetworkTitle,
  NetworkTitleUnfucked,
} from "./style";

type TProps = {
  net: string;
  src: string;
  title: string;
  desc: string;
};

const NetworkInfo: React.FC<TProps> = ({ net, src, title, desc }) => {
  const [isTooltipHidden, setIsTooltipHidden] = useState(false);
  const [isCollapsed, setIsCollapsed] = useState(isMobileHeight());
  const dispatch = useDispatch();

  const onNetInfoBtnClick = (e: React.MouseEvent<HTMLElement>) => {
    dispatch(
      openNetInfoWindow({
        data: {
          net,
        },
        initialX: e.clientX,
        initialY: e.clientY,
      })
    );
  };

  const onCollapseBtnClick = () => {
    setIsTooltipHidden(!isCollapsed);
    setIsCollapsed(!isCollapsed);
    setTimeout(() => {
      setIsTooltipHidden(false);
    }, 100);
  };

  const netInfoBtn = (
    <NetworkButton src={InfoIcon} onClick={onNetInfoBtnClick} />
  );

  const collapseBtn = (
    <NetworkButton
      src={ArrowIcon}
      style={{ transform: isCollapsed ? "rotate(180deg)" : "none" }}
      onClick={onCollapseBtnClick}
    />
  );

  const baseTooltipProps = {
    overlay: <NetHover />,
    align: {
      points: ["tl", "tr"],
      offset: [0, 8],
    },
  };

  const tooltipProps = isTooltipHidden
    ? {
        ...baseTooltipProps,
        visible: false,
      }
    : {
        ...baseTooltipProps,
      };

  return (
    <Tooltip {...tooltipProps}>
      <NetworkRoot>
        {isCollapsed ? (
          <NetworkBox>
            <NetworkButtons>
              {netInfoBtn}
              <NetworkTitleUnfucked>{title}</NetworkTitleUnfucked>
              {collapseBtn}
            </NetworkButtons>
          </NetworkBox>
        ) : (
          <NetworkBoxPadded>
            <NetworkAvatar src={imgToUrlScaled(src, "240")} />
            <NetworkTitle>{title}</NetworkTitle>
            {isCollapsed ? null : <NetworkDesc>{desc}</NetworkDesc>}
            <NetworkButtons>
              {netInfoBtn}
              <Spacer />
              {collapseBtn}
            </NetworkButtons>
            {isTouch() ? <MobileNetsList /> : <></>}
          </NetworkBoxPadded>
        )}
      </NetworkRoot>
    </Tooltip>
  );
};

export default NetworkInfo;
