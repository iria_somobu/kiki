import { RoundBox } from "App/style";
import styled from "styled-components";

export const NetworkRoot = styled.div`
  padding: 8px 8px 8px;
`;

export const NetworkAvatar = styled.img`
  position: absolute;
  top: -26px;
  left: 0;
  right: 0;
  height: 52px;
  width: 52px;
  margin-left: auto;
  margin-right: auto;

  background: var(--totally-white);
  border: 2px solid var(--dk-blue);
  border-radius: 50%;
`;

export const NetworkBox = styled(RoundBox)`
  position: relative;
  padding: 8px;
`;

export const NetworkBoxPadded = styled(NetworkBox)`
  margin-top: 32px;
`;

export const NetworkTitleUnfucked = styled.div`
  flex: 1 1;
  text-align: center;
  font-weight: 500;
  font-size: 16px;
  line-height: 120%;
  color: var(--dk-blue);
`;

export const NetworkTitle = styled(NetworkTitleUnfucked)`
  padding: 32px 8px 8px;
`;

export const NetworkDesc = styled.div`
  padding: 0 8px 8px;
  font-size: 12px;
  line-height: 120%;
  letter-spacing: -0.025em;
  color: var(--grey);
`;

export const NetworkButtons = styled.div`
  display: flex;
  align-items: center;
`;
