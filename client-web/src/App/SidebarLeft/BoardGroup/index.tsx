import { useState } from "react";
import { useParams } from "react-router-dom";

import { Spacer } from "App/style";
import ArrowIcon from "img/cat_arrow.svg";

import BoardButton from "./BoardButton";
import { BoardGroupRt, BoardList, BoardSection, BoardTriangle } from "./style";

export type TBoardGroupType = {
  name: string;
  position: number;
  boards: {
    name: string;
    position: number;
    descr: string;
    lastUpdateTime?: number | null | undefined;
  }[];
};

type TProps = {
  group: TBoardGroupType;
};

const BoardGroup: React.FC<TProps> = ({ group }) => {
  const { net, board } = useParams();
  const [open, setOpen] = useState(true);

  if (net === undefined) return <></>;

  let netDescTransform = "none";
  if (open) netDescTransform = "rotate(180deg)";

  const collapseBtn = (
    <BoardTriangle src={ArrowIcon} style={{ transform: netDescTransform }} />
  );

  return (
    <BoardGroupRt key={group.position} open onToggle={() => setOpen(!open)}>
      <BoardSection>
        {group.name}
        <Spacer />
        {collapseBtn}
      </BoardSection>
      <BoardList>
        {group.boards.map((boardItem) => {
          return (
            <BoardButton
              key={boardItem.position}
              active={boardItem.name === board}
              net={net}
              name={boardItem.name}
              descr={boardItem.descr}
              lastUpdate={boardItem.lastUpdateTime}
            />
          );
        })}
      </BoardList>
    </BoardGroupRt>
  );
};

export default BoardGroup;
