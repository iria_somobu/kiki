import Tooltip from "components/Tooltip";
import { getBoardVisitedTime, setBoardVisitedTime } from "utils/index";

import { Container, Overlay } from "./style";

type TProps = {
  net: string;
  active?: boolean;
  name: string;
  descr: string;
  lastUpdate?: number | null | undefined;
};

const BoardButton: React.FC<TProps> = ({
  active,
  name,
  descr,
  net,
  lastUpdate,
}) => {
  const lut = typeof lastUpdate === 'number' ? lastUpdate : 0;
  const isUnread = getBoardVisitedTime(net, name) < lut;

  const onClick = () => {
    setBoardVisitedTime(net, name, new Date().getTime() / 1000);
  };

  return (
    <Tooltip
      placement="right"
      overlay={<Overlay>{descr}</Overlay>}
      hideOnOverlayHover
    >
      <Container
        className={(active ? "isActive" : "") + (isUnread ? " unread" : "")}
        to={"/" + net + "/" + name}
        onClick={onClick}
      >
        {name}
      </Container>
    </Tooltip>
  );
};

export default BoardButton;
