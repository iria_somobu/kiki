import { Link } from "react-router-dom";

import styled, { css } from "styled-components";

const DefaultStyle = css`
  color: var(--dk-blue);
  background: var(--card-white);

  transition: 0.1s;

  &:hover {
    background: var(--card-grey);
  }
`;

const ActiveStyle = css`
  background: var(--dk-blue);
  color: var(--card-white);

  &:hover {
    background: var(--lt-blue);
    color: var(--card-white);
  }
`;

export const Container = styled(Link)`
  display: inline;
  width: 108px; /* found empirically */
  height: 32px;
  padding: 0 16px;

  border: 0;
  border-radius: 8px;

  text-decoration: none;

  font-weight: 500;
  font-size: 12px;
  text-align: center;
  line-height: 32px;

  & {
    ${DefaultStyle}
  }

  &:hover {
    color: var(--totally-black);
  }

  &.unread {
    font-weight: 900;
    text-shadow: #cbcbcb 1px 0 5px;
  }

  &.isActive {
    ${ActiveStyle}
  }
`;

export const Overlay = styled.div``;
