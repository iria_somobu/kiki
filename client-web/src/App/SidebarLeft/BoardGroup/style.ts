import styled from "styled-components";

export const BoardGroupRt = styled.details`
  padding: 0 8px 8px;
`;

export const BoardSection = styled.summary`
  display: flex;
  flex-direction: row;

  padding: 8px 0px 8px 8px;
  width: 100%;
  cursor: pointer;

  align-items: baseline;

  font-size: 12px;

  color: var(--grey);
  list-style: none;
`;

export const BoardList = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  row-gap: 8px;
  column-gap: 8px;
`;

export const BoardTriangle = styled.img`
  width: 10px;
  height: 10px;
`;
