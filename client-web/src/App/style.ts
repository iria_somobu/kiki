import styled from "styled-components";
import { Breakpoints } from "utils/constants";

export const FloatingButtonsContainer = styled.div`
  position: fixed;

  bottom: 8px;
  right: 8px;

  @media (min-width: ${Breakpoints.mobile.px}) {
    bottom: 32px;
    right: calc(32px + var(--sidebar-width));
  }

  @media (min-width: ${Breakpoints.desktop.px}) {
    display: inherit;

    right: calc(
      50% - ${Breakpoints.desktop.px} / 2 + 32px + var(--sidebar-width)
    );
  }
`;

export const FloatingButton = styled.img`
  display: block;
  height: 40px;
  width: 40px;
  padding: 12px;
  margin-top: 16px;

  object-fit: contain;

  border-radius: 20px;
  background-color: var(--totally-white);
  cursor: pointer;
  box-shadow: 0 0 8px rgba(0, 0, 0, 0.2);

  transition: 0.3s;

  &:hover {
    box-shadow: 0 0 16px rgba(0, 0, 0, 0.3);
  }
`;

export const BaseIcon = styled.img`
  width: 32px;
  height: 32px;
  padding: 8px;
`;

export const RoundBox = styled.div`
  padding: 0 16px;
  border-radius: 8px;
  background: var(--card-white);
`;

export const NetworkButton = styled(BaseIcon)`
  cursor: pointer;

  border-radius: 6px;
  transition: 0.15s;

  :hover {
    background-color: var(--col--grey);
  }
`;

export const Centered = styled.div`
  height: 100%;
  width: 100%;
  display: grid;
  text-align: center;
  justify-content: center;
  align-content: center;
`;

export const Spacer = styled.div`
  flex-grow: 1;
`;
