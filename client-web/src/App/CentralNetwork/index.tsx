import { useTranslation } from "react-i18next";

import Infoblock from "components/Infoblock";
import { useParamsWithoutUndefined } from "hooks";

import { Container, GreyText } from "./style";

const Network: React.FC = () => {
  const { net } = useParamsWithoutUndefined();
  const { t } = useTranslation();

  return (
    <Container>
      {t("net.a", { net })}
      <GreyText>{t("net.b")}</GreyText>
      <Infoblock
        blockName="central-quick-links"
        translationKey="info.quickLinks"
      />
    </Container>
  );
};

export default Network;
