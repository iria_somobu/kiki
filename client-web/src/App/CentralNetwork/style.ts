import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  align-items: center;
  justify-content: center;

  row-gap: 18px;

  self-align: stretch;
`;

export const GreyText = styled.span`
  color: var(--dk-grey);
`;
