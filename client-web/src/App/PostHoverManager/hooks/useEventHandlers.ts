import { useEffect } from "react";

import { useDispatch } from "hooks";
import { centralThreadSlice } from "store/centralThread";
import { store } from "store/indexStore";
import { TPostEntry, postHoverSlice } from "store/postHover";

import { TPostRefs } from "./usePostHoverLayout";

const isPointInsideRect = (
  px: number,
  py: number,
  x: number,
  y: number,
  width: number,
  height: number
) => {
  if (px > x && px < x + width) {
    if (py > y && py < y + height) {
      return true;
    }
  }
  return false;
};

let HoverId = 0;

const useEventHandlers = (
  postHoverRefs: TPostRefs,
  posts: TPostEntry[],
  dispatch: ReturnType<typeof useDispatch>
) => {
  useEffect(() => {
    const isCursorInsideNewestHover = (cursorX: number, cursorY: number) => {
      for (let i = posts.length - 1; i >= 0; --i) {
        const hash = posts[i].hash;
        const el = postHoverRefs.current[hash]?.el;
        if (!el) continue;

        const rect = el.getBoundingClientRect();
        const isInside = isPointInsideRect(
          cursorX,
          cursorY,
          rect.x,
          rect.y - 15,
          rect?.width,
          rect.height + 30
        );

        if (isInside) {
          return {
            hash,
            id: posts[i].id,
          };
        }
      }
    };

    const isTargetInsideHover = (target: HTMLElement) => {
      let el: HTMLElement | null = target;
      while (el !== null) {
        const hash = el.getAttribute("data-post-hover-hash");
        const id = el.getAttribute("data-post-hover-id");
        if (hash !== null && id !== null) {
          return {
            hash,
            id: Number(id),
          };
        }
        el = el.parentElement;
      }
      return null;
    };

    const removeChildren = (parent: number) => {
      const newPosts = posts.filter((post) => post.parent !== parent);
      if (newPosts.length !== posts.length) {
        dispatch(
          postHoverSlice.actions.updateAll({
            posts: newPosts,
          })
        );
      }
    };

    const handleOverPostHash = (target: HTMLElement, postHash: string) => {
      const activeHover = isTargetInsideHover(target);
      if (!activeHover) {
        dispatch(postHoverSlice.actions.hideAll());
      } else {
        removeChildren(activeHover.id);
      }

      const isPostInViewport = store
        .getState()
        .centralThread.postsInViewport.includes(postHash);
      if (isPostInViewport) {
        dispatch(centralThreadSlice.actions.highlightPost({ hash: postHash }));
        return;
      } else {
        dispatch(centralThreadSlice.actions.unHighlightPost());
      }

      const postHashRect = target.getBoundingClientRect();
      const isReply = target.getAttribute("data-post-hash-reply") === "true";

      dispatch(
        postHoverSlice.actions.add({
          hash: postHash,
          x: postHashRect.x,
          y: postHashRect.bottom,
          parent: activeHover ? activeHover.id : null,
          id: HoverId++,
          maxWidth: null,
          maxHeight: null,
          isLoaded: false,
          preferTop: !isReply,
          triggerRect: {
            x: postHashRect.x,
            y: postHashRect.y,
            width: postHashRect.width,
            height: postHashRect.height,
          },
        })
      );
    };

    const handleOutsidePostHash = (cursorX: number, cursorY: number) => {
      if (store.getState().centralThread.highlightedPost !== null) {
        dispatch(centralThreadSlice.actions.unHighlightPost());
      }

      if (posts.length === 0) {
        return;
      }

      const inHover = isCursorInsideNewestHover(cursorX, cursorY);
      if (inHover === undefined) {
        dispatch(postHoverSlice.actions.hideAll());
      } else {
        removeChildren(inHover.id);
      }
    };

    const mouseOverHandler = (e: MouseEvent) => {
      const target = e.target;
      if (!(target instanceof HTMLElement)) {
        return e;
      }

      const postHash = target.getAttribute("data-post-hash");
      if (postHash !== null) {
        handleOverPostHash(target, postHash);
      } else {
        handleOutsidePostHash(e.clientX, e.clientY);
      }
    };

    document.body.addEventListener("mouseover", mouseOverHandler);

    return () => {
      document.body.removeEventListener("mouseover", mouseOverHandler);
    };
  }, [dispatch, posts, postHoverRefs]);
};

export default useEventHandlers;
