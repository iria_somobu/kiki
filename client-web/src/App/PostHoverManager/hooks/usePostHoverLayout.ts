import { MutableRefObject, useLayoutEffect } from "react";

import { AnyAction, Dispatch } from "@reduxjs/toolkit";

import { TPostEntry, postHoverSlice } from "store/postHover";

export type TPostRefs = MutableRefObject<{
  [ket: string]: { el: HTMLElement | null };
}>;

const windowPadding = 16;

const getWindowSize = () => {
  const rect = window.document.body.getBoundingClientRect();

  return {
    windowWidth: rect.width,
    windowHeight: rect.height,
  };
};

type TVertPlace = {
  y: number;
  maxHeight: number;
  overflow: boolean;
};

const getTopPlace = ({ triggerRect }: TPostEntry, hoverRect: DOMRect) => {
  const availableHeight = triggerRect.y - windowPadding;
  const overflow = availableHeight < hoverRect.height;

  const y = triggerRect.y - hoverRect.height;

  return {
    maxHeight: availableHeight,
    y: overflow ? windowPadding : y,
    overflow,
  };
};

const getBottomPlace = ({ triggerRect }: TPostEntry, hoverRect: DOMRect) => {
  const { windowHeight } = getWindowSize();

  const availableHeight =
    windowHeight - windowPadding - (triggerRect.y + triggerRect.height);

  const overflow = availableHeight < hoverRect.height;

  return {
    y: triggerRect.y + triggerRect.height,
    maxHeight: Math.max(0, availableHeight),
    overflow: overflow,
  };
};

const selectBestPlace = (first: TVertPlace, second: TVertPlace) => {
  if (first.overflow === true && second.maxHeight > first.maxHeight) {
    return {
      newY: second.y,
      maxHeight: second.maxHeight,
    };
  }

  return {
    newY: first.y,
    maxHeight: first.maxHeight,
  };
};

const tryPlaceOnTop = (top: TVertPlace, bottom: TVertPlace) => {
  return selectBestPlace(top, bottom);
};

const tryPlaceOnBottom = (top: TVertPlace, bottom: TVertPlace) => {
  return selectBestPlace(bottom, top);
};

const getVertPos = (hover: TPostEntry, hoverRect: DOMRect) => {
  const top = getTopPlace(hover, hoverRect);
  const bottom = getBottomPlace(hover, hoverRect);

  let result = null;
  if (hover.preferTop) {
    result = tryPlaceOnTop(top, bottom);
  } else {
    result = tryPlaceOnBottom(top, bottom);
  }

  /*console.group("Variants:");
  console.log("hoverRect = ", hoverRect);
  console.log("top = ", top);
  console.log("bottom = ", bottom);
  console.log("result = ", result);
  console.groupEnd();*/

  return result;
};

const getHorzPos = (hover: TPostEntry, hoverRect: DOMRect) => {
  const { windowWidth } = getWindowSize();

  const horzRightOverflow = Math.max(hoverRect.right - windowWidth, 0);

  let newX = horzRightOverflow
    ? hoverRect.x - horzRightOverflow - windowPadding
    : Math.max(windowPadding, hover.x);

  let maxWidth = hover.maxWidth;
  if (hoverRect.width >= windowWidth) {
    maxWidth = windowWidth - windowPadding * 2;
    newX = windowPadding;
  }

  return {
    maxWidth,
    newX,
  };
};

const calculateHoverGeometry = (hover: TPostEntry, hoverRect: DOMRect) => {
  return {
    ...getHorzPos(hover, hoverRect),
    ...getVertPos(hover, hoverRect),
  };
};

const usePostHoverLayout = (
  postHoverRefs: TPostRefs,
  posts: TPostEntry[],
  dispatch: Dispatch<AnyAction>
) => {
  useLayoutEffect(() => {
    const hoverRefs = postHoverRefs.current;

    const mountedHovers: {
      [key: string]: TPostEntry;
    } = {};

    posts.forEach((post) => {
      if (hoverRefs[post.hash].el) {
        mountedHovers[post.hash] = { ...post };
      }
    });

    let shouldMerge = false;
    for (const hash in mountedHovers) {
      const hoverEl = hoverRefs[hash].el;
      const isLoaded = mountedHovers[hash].isLoaded;

      if (hoverEl === null) {
        continue;
      }

      const obj = mountedHovers[hash];

      const { newX, newY, maxWidth, maxHeight } = calculateHoverGeometry(
        obj,
        hoverEl.getBoundingClientRect()
      );

      const newMaxHeight = isLoaded ? maxHeight : null;

      shouldMerge =
        shouldMerge ||
        obj.x !== newX ||
        obj.y !== newY ||
        obj.maxHeight !== newMaxHeight ||
        obj.maxWidth !== maxWidth;

      obj.x = newX;
      obj.y = newY;
      obj.maxWidth = maxWidth;
      obj.maxHeight = newMaxHeight;
    }

    if (shouldMerge) {
      const newPosts = posts.map((hover) => {
        const newObj = mountedHovers[hover.hash];
        return {
          ...newObj,
          x: newObj !== undefined ? newObj.x : hover.x,
          y: newObj !== undefined ? newObj.y : hover.y,
          maxWidth: newObj !== undefined ? newObj.maxWidth : hover.maxWidth,
          maxHeight: newObj !== undefined ? newObj.maxHeight : hover.maxHeight,
        };
      });

      dispatch(
        postHoverSlice.actions.updateAll({
          posts: newPosts,
        })
      );
    }
  }, [posts, dispatch, postHoverRefs]);
};

export default usePostHoverLayout;
