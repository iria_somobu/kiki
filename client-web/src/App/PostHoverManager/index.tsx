import { useEffect, useRef } from "react";

import {
  useDispatch,
  useParamsWithoutUndefined,
  useSelector,
  useToken,
} from "hooks";
import { postHoverSlice } from "store/postHover";

import PostHover from "./PostHover";
import useEventHandlers from "./hooks/useEventHandlers";
import usePostHoverLayout, { TPostRefs } from "./hooks/usePostHoverLayout";
import { Container } from "./style";

const PostHoverManager: React.FC = () => {
  const { net, board, thread } = useParamsWithoutUndefined();

  const token = useToken();
  const dispatch = useDispatch();
  const posts = useSelector((store) => store.postHover.posts);
  const highlightedPost = useSelector(
    (store) => store.centralThread.highlightedPost
  );

  const postHoverRefs: TPostRefs = useRef({});

  usePostHoverLayout(postHoverRefs, posts, dispatch);
  useEventHandlers(postHoverRefs, posts, dispatch);

  const addToRef = (hash: string, el: HTMLElement | null) => {
    postHoverRefs.current[hash] = { el };
  };

  useEffect(() => {
    return () => {
      dispatch(postHoverSlice.actions.hideAll());
    };
  }, [dispatch]);

  return (
    <Container className="post-hover-manager">
      {posts.map((post) => (
        <PostHover
          board={board}
          net={net}
          thread={thread}
          ref={(el) => addToRef(post.hash, el)}
          key={post.hash}
          {...post}
          token={token}
          transparent={highlightedPost !== null}
        />
      ))}
    </Container>
  );
};

export default PostHoverManager;
