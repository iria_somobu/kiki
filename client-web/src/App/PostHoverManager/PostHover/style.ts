import styled from "styled-components";
import { ZIndices } from "utils/constants";

type TContainerProps = {
  x: number;
  y: number;
  maxWidth: number | null;
  maxHeight: number | null;
  transparent: boolean;
};

export const Container = styled.div<TContainerProps>`
  position: absolute;
  width: max-content;
  z-index: ${ZIndices.postHovers};
  overflow: hidden;

  opacity: ${({ transparent }) => (transparent ? "0.2" : "1.0")};

  display: flex;
  flex-direction: column;

  left: ${({ x }) => x}px;
  top: ${({ y }) => y}px;

  max-width: ${({ maxWidth }) =>
    maxWidth === null ? "unset" : `${maxWidth}px`};
  max-height: ${({ maxHeight }) =>
    maxHeight === null ? "unset" : `${maxHeight}px`};
`;
