import React, { useEffect } from "react";

import Post from "components/Post";
import { graphql } from "gql";
import { useDispatch, useQueryWithoutUndefined } from "hooks";
import { postHoverSlice } from "store/postHover";
import {
  addReplyToNewPostWindow,
  openNewPostWindow,
} from "store/windowManager";

import { Container } from "./style";

type TProps = {
  hash: string;
  token: string;
  x: number;
  y: number;
  id: number;
  maxWidth: number | null;
  maxHeight: number | null;
  net: string;
  board: string;
  thread: string;
  isLoaded: boolean;
  transparent?: boolean;
};

const GQL_POST_QUERY = graphql(`
  query PostQuery($hash: String!, $token: String!) {
    post(hash: $hash, token: $token) {
      hash
      date
      title
      message
      images
      root
      parents
      children
      hidden
      onHold
    }
  }
`);

const PostHover = React.forwardRef<HTMLDivElement, TProps>(
  (
    {
      maxHeight,
      maxWidth,
      hash,
      token,
      x,
      y,
      id,
      net,
      board,
      thread,
      isLoaded,
      transparent,
    },
    ref
  ) => {
    const dispatch = useDispatch();

    const { data, loading } = useQueryWithoutUndefined(GQL_POST_QUERY, {
      variables: {
        token,
        hash,
      },
    });

    useEffect(() => {
      if (Boolean(loading) === false && Boolean(data)) {
        dispatch(postHoverSlice.actions.oneLoaded(id));
      }
    }, [loading, data, dispatch, id]);

    const onReplyClick = (
      hash: string,
      root: string,
      clientX: number,
      clientY: number
    ) => {
      dispatch(
        addReplyToNewPostWindow({
          net,
          board,
          thread,
          reply: {
            hash,
            root,
          },
        })
      );

      dispatch(
        openNewPostWindow({
          data: {
            net,
            thread,
            board,
            reply: {
              hash,
              root,
            },
          },
          initialX: clientX,
          initialY: clientY,
        })
      );
    };

    return (
      <Container
        ref={ref}
        x={x}
        y={y}
        maxWidth={maxWidth}
        maxHeight={maxHeight}
        data-post-hover-hash={hash}
        data-post-hover-id={id}
        transparent={Boolean(transparent)}
      >
        {isLoaded ? (
          <Post
            net={net}
            board={board}
            isOP={false}
            isLimited={false}
            bordered
            post={{
              ...data.post,
            }}
            onReplyClick={onReplyClick}
          />
        ) : null}
      </Container>
    );
  }
);

export default PostHover;
