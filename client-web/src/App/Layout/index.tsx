import { Route, Routes } from "react-router-dom";

import Board from "App/CentralBoard";
import Network from "App/CentralNetwork";
import Thread from "App/CentralThread";
import MobileTopbar from "App/MobileTopbar";
import LeftSidebar from "App/SidebarLeft";
import RightSidebar from "App/SidebarRight";

import { Container, Main, View } from "./style";

const Layout: React.FC = () => {
  return (
    <Container>
      <MobileTopbar />

      <Main>
        <Routes>
          <Route path="/:net/*" element={<LeftSidebar />} />
          <Route path="/:net/:board/*" element={<LeftSidebar />} />
        </Routes>

        <View>
          <Routes>
            <Route path="/:net" element={<Network />} />
            <Route path="/:net/:board" element={<Board />} />
            <Route path="/:net/:board/:thread" element={<Thread />} />
          </Routes>
        </View>

        <RightSidebar />
      </Main>
    </Container>
  );
};

export default Layout;
