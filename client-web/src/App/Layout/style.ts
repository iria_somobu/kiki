import styled from "styled-components";

export const View = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;

  flex-grow: 1;
  align-self: stretch;
`;

export const Main = styled.main`
  display: flex;
  flex-direction: row;
  height: 100%;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;
