import styled from "styled-components";
import { Breakpoints } from "utils/constants";

export const PostList = styled.div`
  display: flex;
  flex-direction: column;
  padding: 4px;
  gap: 8px;

  @media (min-width: ${Breakpoints.extraMobile.px}) {
    padding: 16px;
    gap: 16px;
  }
`;

export const BottomDiv = styled.div`
  height: 48px;
  max-width: 698px;
  margin-bottom: 96px;

  display: flex;
  justify-content: center;
  align-items: center;

  color: var(--grey);
  font-size: 14px;
`;
