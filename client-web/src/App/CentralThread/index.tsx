import { Fragment, useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";

import { FloatingButton, FloatingButtonsContainer } from "App/style";
import LoadingError from "components/LoadingError";
import Post from "components/Post";
import ScrollableContent from "components/ScrollableContent";
import { graphql } from "gql";
import {
  useDispatch,
  useParamsWithoutUndefined,
  useQueryWithoutUndefined,
  useSelector,
  useToken,
} from "hooks";
import NavArrow from "img/nav_arrow.svg";
import { centralThreadSlice as slice } from "store/centralThread";
import {
  addReplyToNewPostWindow,
  openNewPostWindow,
} from "store/windowManager";
import { getUnreadBaseline, getUnreadDelta, setUnreadDelta } from "utils";

import UnreadDivider from "./UnreadDivider";
import { BottomDiv, PostList } from "./style";

const GQL_POST_SUBSCRIPTION = graphql(`
  subscription NewPostSubscription($token: String!, $rootHash: String!) {
    post(token: $token, rootHash: $rootHash) {
      hash
      date
      title
      message
      root
      parents
      children
      hidden
      onHold
      images
    }
  }
`);

const GQL_POSTS_QUERY = graphql(`
  query Posts($token: String!, $thread: String!) {
    posts(token: $token, thread: $thread) {
      hash
      date
      title
      message
      root
      parents
      children
      hidden
      onHold
      images
    }
  }
`);

const Thread: React.FC = () => {
  const { net, board, thread } = useParamsWithoutUndefined();

  // Divider is a line says 'new messages' above first new unread message
  const [dividerHash, setDivider] = useState<string | null>(null);

  const token = useToken();
  const highlightedPost = useSelector(
    (store) => store.centralThread.highlightedPost
  );

  const { t } = useTranslation();

  const dispatch = useDispatch();
  const bottomDivRef = useRef<HTMLDivElement>(null);

  const { loading, error, data, subscribeToMore } = useQueryWithoutUndefined(
    GQL_POSTS_QUERY,
    {
      variables: {
        token,
        thread,
      },
      fetchPolicy: "cache-and-network",
    }
  );

  useEffect(() => {
    subscribeToMore({
      document: GQL_POST_SUBSCRIPTION,
      variables: { rootHash: thread, token: token },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;

        const oldPosts = prev.posts;
        const newPost = subscriptionData.data.post;

        const newPosts = oldPosts.map((post) => {
          const children = [...post.children];

          const isParentPost = newPost.parents.includes(post.hash);
          if (isParentPost) {
            children.push(newPost.hash);
          }

          return {
            ...post,
            children,
          };
        });

        const postIndex = oldPosts.findIndex((p) => newPost.hash === p.hash);
        if (postIndex === -1) {
          newPosts.push(subscriptionData.data.post);
          newPosts.sort((a, b) => {
            // We can do this cuz there can be only one OP (hash == root)
            if (a.hash === a.root) return -1;
            if (b.hash === b.root) return 1;

            return a.date - b.date;
          });
        } else {
          newPosts[postIndex] = newPost;
        }

        return {
          posts: [...newPosts],
        };
      },
    });
  }, [thread, subscribeToMore, token]);

  useEffect(() => {
    if (data) {
      const baseline = getUnreadBaseline(board);
      let oldDelta = getUnreadDelta(thread);

      let delta = 0;
      for (const dp of data.posts) {
        if (dp.date >= baseline) delta++;
      }

      if (dividerHash == null) {
        // Let's find before which post we should show divider
        for (const p of data.posts) {
          if (p.date >= baseline) oldDelta--;

          if (oldDelta < 0) {
            // So, we finally found first unread post
            if (p.hash !== data.posts[0].hash) {
              // Make sure this is not OP
              setDivider(p.hash);
            }
            break;
          }
        }

        // If no new messages found - do not show divider at all
        if (oldDelta >= 0) {
          setDivider("");
        }
      }

      setUnreadDelta(thread, delta);
    }
  }, [data, board, thread, dividerHash]);

  if (loading || error) return <LoadingError error={error} />;

  const onReplyClick = (
    hash: string,
    root: string,
    clientX: number,
    clientY: number
  ) => {
    dispatch(
      addReplyToNewPostWindow({
        net,
        board,
        thread: data.posts[0].hash,
        reply: {
          hash,
          root,
        },
      })
    );

    dispatch(
      openNewPostWindow({
        data: {
          net,
          thread,
          board,
          reply: { hash, root },
        },
        initialX: clientX,
        initialY: clientY,
      })
    );
  };

  const onScrollClick = () => {
    const ref = bottomDivRef.current;
    if (ref != null) ref.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <ScrollableContent>
      <PostList>
        {data.posts.map((item) => {
          return (
            <Fragment key={item.hash}>
              {item.hash === dividerHash ? <UnreadDivider /> : <></>}
              <Post
                net={net}
                board={board}
                post={item}
                isHighlighted={item.hash === highlightedPost}
                isOP={item.hash === item.root}
                isLimited={false}
                onReplyClick={onReplyClick}
                onInViewportVisibilityChange={(isVisible) => {
                  if (isVisible) {
                    dispatch(
                      slice.actions.addPostInViewport({
                        hash: item.hash,
                      })
                    );
                  } else {
                    dispatch(
                      slice.actions.removePostInViewport({
                        hash: item.hash,
                      })
                    );
                  }
                }}
              />
            </Fragment>
          );
        })}
        <BottomDiv ref={bottomDivRef}>{t("thread.newMsgHere")}</BottomDiv>
      </PostList>

      <FloatingButtonsContainer>
        {data.posts.length > 10 ? (
          <FloatingButton src={NavArrow} onClick={onScrollClick} />
        ) : (
          <></>
        )}
      </FloatingButtonsContainer>
    </ScrollableContent>
  );
};

export default Thread;
