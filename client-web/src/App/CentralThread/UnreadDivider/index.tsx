import { MiniHr, UnreadDividerDiv } from "./style";

const UnreadDivider: React.FC = () => {
  return (
    <UnreadDividerDiv>
      <MiniHr />
      New messages
      <MiniHr />
    </UnreadDividerDiv>
  );
};

export default UnreadDivider;
