import styled from "styled-components";

export const UnreadDividerDiv = styled.div`
  height: 24px;
  max-width: 698px;

  display: flex;
  justify-content: center;
  align-items: center;
  gap: 12px;

  padding: 0 12px;

  color: var(--pink);
  font-size: 14px;
  font-weight: 500;
`;

export const MiniHr = styled.hr`
  display: block;
  flex-grow: 1;

  margin: 1em 0;

  height: 1px;
  border: 0;
  border-top: 1px solid var(--pink);
`;
