import { useEffect, useRef } from "react";

import { FloatingButton, FloatingButtonsContainer } from "App/style";
import LoadingError from "components/LoadingError";
import MultiPost from "components/Post";
import ScrollableContent from "components/ScrollableContent";
import { graphql } from "gql";
import {
  useDispatch,
  useParamsWithoutUndefined,
  useQueryWithoutUndefined,
  useToken,
} from "hooks";
import NavArrowIcon from "img/nav_arrow.svg";
import NavPlusIcon from "img/nav_plus.svg";
import { openNewThreadWindow } from "store/windowManager";
import { getUnreadBaseline } from "utils";

import {
  DescBlockRoot,
  DescBlockText,
  DescBlockTitle,
  StyledMasonry,
} from "./style";

const GQL_BOARD_QUERY = graphql(`
  query CentralsBoard(
    $token: String!
    $net: String!
    $board: String!
    $badgeTime: Int!
  ) {
    board(token: $token, net: $net, board: $board) {
      name
      descr
    }

    threads(token: $token, net: $net, board: $board, badgeTime: $badgeTime) {
      replies
      unreads
      post {
        hash
        date
        title
        message
        images
        root
        hidden
        onHold
        parents
        children
      }
    }
  }
`);

const GQL_THREAD_SUBSCRIPTION = graphql(`
  subscription NewThreadSubscription(
    $token: String!
    $net: String!
    $board: String!
  ) {
    thread(token: $token, net: $net, board: $board) {
      replies
      unreads
      post {
        hash
        date
        title
        message
        images
        root
        hidden
        onHold
        parents
        children
      }
    }
  }
`);

const Board: React.FC = () => {
  const { net, board } = useParamsWithoutUndefined();

  const dispatch = useDispatch();
  const token = useToken();
  const bottomDivRef = useRef<HTMLDivElement>(null);

  const { loading, error, data, subscribeToMore } = useQueryWithoutUndefined(
    GQL_BOARD_QUERY,
    {
      variables: {
        token,
        net: net,
        board: board,
        badgeTime: getUnreadBaseline(board),
      },
      fetchPolicy: "cache-and-network",
    }
  );

  useEffect(() => {
    subscribeToMore({
      document: GQL_THREAD_SUBSCRIPTION,
      variables: { token: token, net: net, board: board },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const newThread = subscriptionData.data.thread;

        const threadIndex = prev.threads.findIndex(
          (t) => t.post.hash === newThread.post.hash
        );

        let threads;
        if (threadIndex === -1) {
          threads = [...prev.threads, newThread];
          threads.sort((a, b) => b.post.date - a.post.date);
        } else {
          threads = prev.threads;
          threads[threadIndex] = newThread;
        }

        return {
          ...prev,
          threads,
        };
      },
    });
  }, [board, net, subscribeToMore, token]);

  if (loading || error) return <LoadingError error={error} />;

  const onScrollClick = () => {
    const awoo = bottomDivRef.current;
    if (awoo !== null) awoo.scrollIntoView({ behavior: "smooth" });
  };

  const onNewThreadBtnClick = (e: React.MouseEvent) => {
    dispatch(
      openNewThreadWindow({
        initialX: e.clientX,
        initialY: e.clientY,
        data: {
          netId: net,
          board,
        },
      })
    );
  };

  return (
    <ScrollableContent>
      <StyledMasonry
        breakpoints={{
          mobile: 0,
          tablet: 1150,
          desktop: 1600,
        }}
        columns={{
          mobile: 1,
          tablet: 2,
          desktop: 3,
        }}
        gap={16}
      >
        <DescBlockRoot>
          <DescBlockTitle>{data.board.name}</DescBlockTitle>
          <DescBlockText>{data.board.descr}</DescBlockText>
        </DescBlockRoot>

        {data.threads.map((thread) => (
          <MultiPost
            key={thread.post.hash}
            net={net}
            board={board}
            thread={thread}
            post={thread.post}
            isOP={true}
            isLimited={true}
          />
        ))}

        <div ref={bottomDivRef} />
      </StyledMasonry>

      <FloatingButtonsContainer>
        <FloatingButton src={NavPlusIcon} onClick={onNewThreadBtnClick} />
        {data.threads.length > 25 ? (
          <FloatingButton src={NavArrowIcon} onClick={onScrollClick} />
        ) : (
          <></>
        )}
      </FloatingButtonsContainer>
    </ScrollableContent>
  );
};

export default Board;
