import Masonry from "react-smart-masonry";

import styled from "styled-components";
import { Breakpoints } from "utils/constants";

export const DescBlockRoot = styled.div`
  padding: 4px 16px 16px;

  background: var(--totally-white);
  border: 2px solid var(--dk-blue);
  border-radius: 8px;
`;

export const StyledMasonry = styled(Masonry)`
  padding: 4px;

  @media (min-width: ${Breakpoints.extraMobile.px}) {
    padding: 16px;
  }

  & > div {
    min-width: 0;
  }
`;

export const DescBlockTitle = styled.h1`
  font-family: "IBM Plex Sans", sans-serif;
  font-style: normal;
  font-weight: 700;
  font-size: 64px;
  line-height: 120%;
  color: var(--dk-blue);
`;

export const DescBlockText = styled.summary`
  font-size: 16px;
  line-height: 120%;
  color: var(--dk-blue);
`;

export const UnreadBadge = styled.div`
  position: absolute;
  transform: translate(-18px, -20px);
  padding: 2px 8px;

  border-radius: 8px;
  background: var(--pink);
  color: #ffffff;
  font-size: 12px;
`;
