/* eslint-disable */
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

/** `Board` is a named group of `Thread`s */
export type Board = {
  __typename?: 'Board';
  /** Human-readable description */
  descr: Scalars['String'];
  /** Group identifier, same limitations as for name */
  group: Scalars['String'];
  /** Time of the last thread created on this board, in seconds */
  lastUpdateTime?: Maybe<Scalars['Int']>;
  /** Should be relatively short (up to 8 chars?), latin letters and digits only */
  name: Scalars['String'];
  /** Board sorting position, global across all boards */
  position: Scalars['Int'];
};

/** For client's convenience, boards are grouped by `BoardGroup`s */
export type BoardGroup = {
  __typename?: 'BoardGroup';
  /** Yup, this is boards list of this group, sorted by board's `position` */
  boards: Array<Board>;
  /** Should be relatively short (up to 8 chars?), latin letters and digits only */
  name: Scalars['String'];
  /** Position is a minimal position of contained boards */
  position: Scalars['Int'];
};

export type Mutation = {
  __typename?: 'Mutation';
  login: Token;
  modPost: Scalars['String'];
  newToken: Token;
  postPost: Post;
  postThread: Thread;
  validateToken: Token;
};


export type MutationLoginArgs = {
  name: Scalars['String'];
  pwd: Scalars['String'];
};


export type MutationModPostArgs = {
  hidden: Scalars['Boolean'];
  onHold: Scalars['Boolean'];
  post: Scalars['String'];
  token: Scalars['String'];
};


export type MutationPostPostArgs = {
  images: Array<Scalars['String']>;
  message: Scalars['String'];
  net: Scalars['String'];
  to: Array<Scalars['String']>;
  token: Scalars['String'];
};


export type MutationPostThreadArgs = {
  board: Scalars['String'];
  images: Array<Scalars['String']>;
  message: Scalars['String'];
  net: Scalars['String'];
  title: Scalars['String'];
  token: Scalars['String'];
};


export type MutationValidateTokenArgs = {
  token: Scalars['String'];
};

/**
 * Net, or Network, is a container for all the threads, posts and boards.
 *
 * Think of it as an equivalent of Discord's server.
 */
export type Net = {
  __typename?: 'Net';
  /** Human-readable network description text */
  descr: Scalars['String'];
  /** Network icon file name */
  icon: Scalars['String'];
  /**
   * Network identifier. Relatively short (up to 12 chars?) latin letters, digits, dashes
   * and underscores only.
   */
  id: Scalars['String'];
  /** Human-readable network name */
  title: Scalars['String'];
};

/** Network statistics data */
export type NetStats = {
  __typename?: 'NetStats';
  /** Online counter */
  online: Scalars['Int'];
  /** Post rate counter */
  postRate: Scalars['Float'];
};

/** Any post across imageboard */
export type Post = {
  __typename?: 'Post';
  /** Board name post belongs to */
  board?: Maybe<Scalars['String']>;
  /**
   * Hashes of children posts (i.e. which posts mentioned this post).
   *
   * Calculated by server.
   */
  children: Array<Scalars['String']>;
  /**
   * Post creation date, in seconds since unix epoch.
   *
   * Assigned by server.
   */
  date: Scalars['Int'];
  /**
   * Hash of this post.
   *
   * Details of how hashing is performed will be discussed later
   */
  hash: Scalars['String'];
  /** Marks this post as a hidden */
  hidden: Scalars['Boolean'];
  /**
   * Post images file names
   * - Max 7 items
   */
  images: Array<Scalars['String']>;
  /**
   * Post text
   * - Up to 2500 symbols long;
   * - May not be empty
   */
  message: Scalars['String'];
  /** Network name post belongs to */
  net?: Maybe<Scalars['String']>;
  /** Moderator should check this post and perform necessary actions */
  onHold: Scalars['Boolean'];
  /**
   * Hashes of parent posts (i.e. which posts has been mentioned by this).
   * - Max 7 items;
   * - 50% + 1 item must share the same `root`. This `root` will be assigned as `root` of this
   * post.
   */
  parents: Array<Scalars['String']>;
  /**
   * Root parent, i.e. hash of thread's head post.
   *
   * Deducted by server based on `parents` value.
   */
  root: Scalars['String'];
  /**
   * Post title, will be thisible on the first post in the thread and hidden for all responding
   * posts.
   * - Up to 80 symbols long;
   * - May be empty (`""`, i.e. not contain any symbol)
   */
  title: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  board: Board;
  boardGroups: Array<BoardGroup>;
  extendedNetworkInfo: Scalars['String'];
  me: User;
  netStats: NetStats;
  network: Net;
  networks: Array<Net>;
  post: Post;
  posts: Array<Post>;
  threads: Array<Thread>;
};


export type QueryBoardArgs = {
  board: Scalars['String'];
  net: Scalars['String'];
  token: Scalars['String'];
};


export type QueryBoardGroupsArgs = {
  net: Scalars['String'];
  token: Scalars['String'];
};


export type QueryExtendedNetworkInfoArgs = {
  net: Scalars['String'];
  token: Scalars['String'];
};


export type QueryMeArgs = {
  token: Scalars['String'];
};


export type QueryNetStatsArgs = {
  token: Scalars['String'];
};


export type QueryNetworkArgs = {
  net: Scalars['String'];
  token: Scalars['String'];
};


export type QueryNetworksArgs = {
  token: Scalars['String'];
};


export type QueryPostArgs = {
  hash: Scalars['String'];
  token: Scalars['String'];
};


export type QueryPostsArgs = {
  thread: Scalars['String'];
  token: Scalars['String'];
};


export type QueryThreadsArgs = {
  badgeTime: Scalars['Int'];
  board: Scalars['String'];
  net: Scalars['String'];
  token: Scalars['String'];
};

export type Subscription = {
  __typename?: 'Subscription';
  /** All new posts (new thread posts included) */
  post: Post;
  /** All new threads */
  thread: Thread;
};


export type SubscriptionPostArgs = {
  rootHash: Scalars['String'];
  token: Scalars['String'];
};


export type SubscriptionThreadArgs = {
  board: Scalars['String'];
  net: Scalars['String'];
  token: Scalars['String'];
};

/**
 * An object representing a `Thread`'s data and meta-data.
 *
 * Thread is a group of posts shares a same `root` hash and referencing each other.
 *
 * Note that thread has no ID: it shares identifier with its head post
 */
export type Thread = {
  __typename?: 'Thread';
  /** Is this thread is added to favorites by current user */
  isFav: Scalars['Boolean'];
  /** Is this thread is marked as watched by current user */
  isWatched: Scalars['Boolean'];
  /** Head post of this thread */
  post: Post;
  /** Total posts in this thread */
  replies: Scalars['Int'];
  /** Total unreads (for the current user) */
  unreads: Scalars['Int'];
};

/**
 * `Token` struct describes, well, a token - a relatively short string required for the most
 * interactions with server.
 *
 * Client can obtain token via [mutation](../mutation/trait.Mutation.html#tymethod.newToken).
 */
export type Token = {
  __typename?: 'Token';
  /** Bit flags, unused for now */
  flags: Scalars['Int'];
  /** Token string itself */
  token: Scalars['String'];
  /** Expiration date */
  validTill: Scalars['Int'];
};

/** Corresponding user object */
export type User = {
  __typename?: 'User';
  id: Scalars['Int'];
  isAdmin: Scalars['Boolean'];
  name: Scalars['String'];
};

export type CentralsBoardQueryVariables = Exact<{
  token: Scalars['String'];
  net: Scalars['String'];
  board: Scalars['String'];
  badgeTime: Scalars['Int'];
}>;


export type CentralsBoardQuery = { __typename?: 'Query', board: { __typename?: 'Board', name: string, descr: string }, threads: Array<{ __typename?: 'Thread', replies: number, unreads: number, post: { __typename?: 'Post', hash: string, date: number, title: string, message: string, images: Array<string>, root: string, hidden: boolean, onHold: boolean, parents: Array<string>, children: Array<string> } }> };

export type NewThreadSubscriptionSubscriptionVariables = Exact<{
  token: Scalars['String'];
  net: Scalars['String'];
  board: Scalars['String'];
}>;


export type NewThreadSubscriptionSubscription = { __typename?: 'Subscription', thread: { __typename?: 'Thread', replies: number, unreads: number, post: { __typename?: 'Post', hash: string, date: number, title: string, message: string, images: Array<string>, root: string, hidden: boolean, onHold: boolean, parents: Array<string>, children: Array<string> } } };

export type NewPostSubscriptionSubscriptionVariables = Exact<{
  token: Scalars['String'];
  rootHash: Scalars['String'];
}>;


export type NewPostSubscriptionSubscription = { __typename?: 'Subscription', post: { __typename?: 'Post', hash: string, date: number, title: string, message: string, root: string, parents: Array<string>, children: Array<string>, hidden: boolean, onHold: boolean, images: Array<string> } };

export type PostsQueryVariables = Exact<{
  token: Scalars['String'];
  thread: Scalars['String'];
}>;


export type PostsQuery = { __typename?: 'Query', posts: Array<{ __typename?: 'Post', hash: string, date: number, title: string, message: string, root: string, parents: Array<string>, children: Array<string>, hidden: boolean, onHold: boolean, images: Array<string> }> };

export type PostQueryQueryVariables = Exact<{
  hash: Scalars['String'];
  token: Scalars['String'];
}>;


export type PostQueryQuery = { __typename?: 'Query', post: { __typename?: 'Post', hash: string, date: number, title: string, message: string, images: Array<string>, root: string, parents: Array<string>, children: Array<string>, hidden: boolean, onHold: boolean } };

export type NetworksQueryVariables = Exact<{
  token: Scalars['String'];
}>;


export type NetworksQuery = { __typename?: 'Query', networks: Array<{ __typename?: 'Net', id: string, title: string, icon: string }> };

export type NetStatsQueryVariables = Exact<{
  token: Scalars['String'];
}>;


export type NetStatsQuery = { __typename?: 'Query', netStats: { __typename?: 'NetStats', online: number, postRate: number } };

export type SidebarLeftQueryQueryVariables = Exact<{
  token: Scalars['String'];
  net: Scalars['String'];
}>;


export type SidebarLeftQueryQuery = { __typename?: 'Query', network: { __typename?: 'Net', id: string, title: string, descr: string, icon: string }, boardGroups: Array<{ __typename?: 'BoardGroup', name: string, position: number, boards: Array<{ __typename?: 'Board', name: string, position: number, descr: string, lastUpdateTime?: number | null }> }> };

export type GetNewTokenMutationVariables = Exact<{ [key: string]: never; }>;


export type GetNewTokenMutation = { __typename?: 'Mutation', newToken: { __typename?: 'Token', token: string, flags: number } };

export type ValidateTokenMutationVariables = Exact<{
  token: Scalars['String'];
}>;


export type ValidateTokenMutation = { __typename?: 'Mutation', validateToken: { __typename?: 'Token', token: string, flags: number } };

export type ModPostMutationVariables = Exact<{
  token: Scalars['String'];
  post: Scalars['String'];
  hidden: Scalars['Boolean'];
  onHold: Scalars['Boolean'];
}>;


export type ModPostMutation = { __typename?: 'Mutation', modPost: string };

export type PostRootThreadDataQueryVariables = Exact<{
  hash: Scalars['String'];
  token: Scalars['String'];
}>;


export type PostRootThreadDataQuery = { __typename?: 'Query', post: { __typename?: 'Post', hash: string, title: string, root: string, board?: string | null } };

export type LoginMutationVariables = Exact<{
  name: Scalars['String'];
  pwd: Scalars['String'];
}>;


export type LoginMutation = { __typename?: 'Mutation', login: { __typename?: 'Token', token: string, flags: number, validTill: number } };

export type NetInfoWindowQueryQueryVariables = Exact<{
  token: Scalars['String'];
  net: Scalars['String'];
}>;


export type NetInfoWindowQueryQuery = { __typename?: 'Query', extendedNetworkInfo: string };

export type AddPostMutationVariables = Exact<{
  token: Scalars['String'];
  net: Scalars['String'];
  to: Array<Scalars['String']> | Scalars['String'];
  message: Scalars['String'];
  images: Array<Scalars['String']> | Scalars['String'];
}>;


export type AddPostMutation = { __typename?: 'Mutation', postPost: { __typename?: 'Post', message: string, root: string } };

export type AddThreadMutationVariables = Exact<{
  token: Scalars['String'];
  net: Scalars['String'];
  board: Scalars['String'];
  message: Scalars['String'];
  title: Scalars['String'];
  images: Array<Scalars['String']> | Scalars['String'];
}>;


export type AddThreadMutation = { __typename?: 'Mutation', postThread: { __typename?: 'Thread', post: { __typename?: 'Post', title: string, message: string } } };


export const CentralsBoardDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"CentralsBoard"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"net"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"board"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"badgeTime"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"board"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}},{"kind":"Argument","name":{"kind":"Name","value":"net"},"value":{"kind":"Variable","name":{"kind":"Name","value":"net"}}},{"kind":"Argument","name":{"kind":"Name","value":"board"},"value":{"kind":"Variable","name":{"kind":"Name","value":"board"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"descr"}}]}},{"kind":"Field","name":{"kind":"Name","value":"threads"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}},{"kind":"Argument","name":{"kind":"Name","value":"net"},"value":{"kind":"Variable","name":{"kind":"Name","value":"net"}}},{"kind":"Argument","name":{"kind":"Name","value":"board"},"value":{"kind":"Variable","name":{"kind":"Name","value":"board"}}},{"kind":"Argument","name":{"kind":"Name","value":"badgeTime"},"value":{"kind":"Variable","name":{"kind":"Name","value":"badgeTime"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"replies"}},{"kind":"Field","name":{"kind":"Name","value":"unreads"}},{"kind":"Field","name":{"kind":"Name","value":"post"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"hash"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"message"}},{"kind":"Field","name":{"kind":"Name","value":"images"}},{"kind":"Field","name":{"kind":"Name","value":"root"}},{"kind":"Field","name":{"kind":"Name","value":"hidden"}},{"kind":"Field","name":{"kind":"Name","value":"onHold"}},{"kind":"Field","name":{"kind":"Name","value":"parents"}},{"kind":"Field","name":{"kind":"Name","value":"children"}}]}}]}}]}}]} as unknown as DocumentNode<CentralsBoardQuery, CentralsBoardQueryVariables>;
export const NewThreadSubscriptionDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"subscription","name":{"kind":"Name","value":"NewThreadSubscription"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"net"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"board"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"thread"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}},{"kind":"Argument","name":{"kind":"Name","value":"net"},"value":{"kind":"Variable","name":{"kind":"Name","value":"net"}}},{"kind":"Argument","name":{"kind":"Name","value":"board"},"value":{"kind":"Variable","name":{"kind":"Name","value":"board"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"replies"}},{"kind":"Field","name":{"kind":"Name","value":"unreads"}},{"kind":"Field","name":{"kind":"Name","value":"post"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"hash"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"message"}},{"kind":"Field","name":{"kind":"Name","value":"images"}},{"kind":"Field","name":{"kind":"Name","value":"root"}},{"kind":"Field","name":{"kind":"Name","value":"hidden"}},{"kind":"Field","name":{"kind":"Name","value":"onHold"}},{"kind":"Field","name":{"kind":"Name","value":"parents"}},{"kind":"Field","name":{"kind":"Name","value":"children"}}]}}]}}]}}]} as unknown as DocumentNode<NewThreadSubscriptionSubscription, NewThreadSubscriptionSubscriptionVariables>;
export const NewPostSubscriptionDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"subscription","name":{"kind":"Name","value":"NewPostSubscription"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"rootHash"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"post"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}},{"kind":"Argument","name":{"kind":"Name","value":"rootHash"},"value":{"kind":"Variable","name":{"kind":"Name","value":"rootHash"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"hash"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"message"}},{"kind":"Field","name":{"kind":"Name","value":"root"}},{"kind":"Field","name":{"kind":"Name","value":"parents"}},{"kind":"Field","name":{"kind":"Name","value":"children"}},{"kind":"Field","name":{"kind":"Name","value":"hidden"}},{"kind":"Field","name":{"kind":"Name","value":"onHold"}},{"kind":"Field","name":{"kind":"Name","value":"images"}}]}}]}}]} as unknown as DocumentNode<NewPostSubscriptionSubscription, NewPostSubscriptionSubscriptionVariables>;
export const PostsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"Posts"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"thread"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"posts"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}},{"kind":"Argument","name":{"kind":"Name","value":"thread"},"value":{"kind":"Variable","name":{"kind":"Name","value":"thread"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"hash"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"message"}},{"kind":"Field","name":{"kind":"Name","value":"root"}},{"kind":"Field","name":{"kind":"Name","value":"parents"}},{"kind":"Field","name":{"kind":"Name","value":"children"}},{"kind":"Field","name":{"kind":"Name","value":"hidden"}},{"kind":"Field","name":{"kind":"Name","value":"onHold"}},{"kind":"Field","name":{"kind":"Name","value":"images"}}]}}]}}]} as unknown as DocumentNode<PostsQuery, PostsQueryVariables>;
export const PostQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"PostQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"hash"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"post"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"hash"},"value":{"kind":"Variable","name":{"kind":"Name","value":"hash"}}},{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"hash"}},{"kind":"Field","name":{"kind":"Name","value":"date"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"message"}},{"kind":"Field","name":{"kind":"Name","value":"images"}},{"kind":"Field","name":{"kind":"Name","value":"root"}},{"kind":"Field","name":{"kind":"Name","value":"parents"}},{"kind":"Field","name":{"kind":"Name","value":"children"}},{"kind":"Field","name":{"kind":"Name","value":"hidden"}},{"kind":"Field","name":{"kind":"Name","value":"onHold"}}]}}]}}]} as unknown as DocumentNode<PostQueryQuery, PostQueryQueryVariables>;
export const NetworksDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"Networks"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"networks"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"icon"}}]}}]}}]} as unknown as DocumentNode<NetworksQuery, NetworksQueryVariables>;
export const NetStatsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"NetStats"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"netStats"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"online"}},{"kind":"Field","name":{"kind":"Name","value":"postRate"}}]}}]}}]} as unknown as DocumentNode<NetStatsQuery, NetStatsQueryVariables>;
export const SidebarLeftQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"SidebarLeftQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"net"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"network"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}},{"kind":"Argument","name":{"kind":"Name","value":"net"},"value":{"kind":"Variable","name":{"kind":"Name","value":"net"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"descr"}},{"kind":"Field","name":{"kind":"Name","value":"icon"}}]}},{"kind":"Field","name":{"kind":"Name","value":"boardGroups"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}},{"kind":"Argument","name":{"kind":"Name","value":"net"},"value":{"kind":"Variable","name":{"kind":"Name","value":"net"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"position"}},{"kind":"Field","name":{"kind":"Name","value":"boards"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"position"}},{"kind":"Field","name":{"kind":"Name","value":"descr"}},{"kind":"Field","name":{"kind":"Name","value":"lastUpdateTime"}}]}}]}}]}}]} as unknown as DocumentNode<SidebarLeftQueryQuery, SidebarLeftQueryQueryVariables>;
export const GetNewTokenDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"GetNewToken"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"newToken"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"token"}},{"kind":"Field","name":{"kind":"Name","value":"flags"}}]}}]}}]} as unknown as DocumentNode<GetNewTokenMutation, GetNewTokenMutationVariables>;
export const ValidateTokenDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"ValidateToken"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"validateToken"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"token"}},{"kind":"Field","name":{"kind":"Name","value":"flags"}}]}}]}}]} as unknown as DocumentNode<ValidateTokenMutation, ValidateTokenMutationVariables>;
export const ModPostDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"ModPost"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"post"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"hidden"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"onHold"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"modPost"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}},{"kind":"Argument","name":{"kind":"Name","value":"post"},"value":{"kind":"Variable","name":{"kind":"Name","value":"post"}}},{"kind":"Argument","name":{"kind":"Name","value":"hidden"},"value":{"kind":"Variable","name":{"kind":"Name","value":"hidden"}}},{"kind":"Argument","name":{"kind":"Name","value":"onHold"},"value":{"kind":"Variable","name":{"kind":"Name","value":"onHold"}}}]}]}}]} as unknown as DocumentNode<ModPostMutation, ModPostMutationVariables>;
export const PostRootThreadDataDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"PostRootThreadData"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"hash"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"post"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"hash"},"value":{"kind":"Variable","name":{"kind":"Name","value":"hash"}}},{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"hash"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"root"}},{"kind":"Field","name":{"kind":"Name","value":"board"}}]}}]}}]} as unknown as DocumentNode<PostRootThreadDataQuery, PostRootThreadDataQueryVariables>;
export const LoginDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"Login"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"name"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"pwd"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"login"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"name"}}},{"kind":"Argument","name":{"kind":"Name","value":"pwd"},"value":{"kind":"Variable","name":{"kind":"Name","value":"pwd"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"token"}},{"kind":"Field","name":{"kind":"Name","value":"flags"}},{"kind":"Field","name":{"kind":"Name","value":"validTill"}}]}}]}}]} as unknown as DocumentNode<LoginMutation, LoginMutationVariables>;
export const NetInfoWindowQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"NetInfoWindowQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"net"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"extendedNetworkInfo"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}},{"kind":"Argument","name":{"kind":"Name","value":"net"},"value":{"kind":"Variable","name":{"kind":"Name","value":"net"}}}]}]}}]} as unknown as DocumentNode<NetInfoWindowQueryQuery, NetInfoWindowQueryQueryVariables>;
export const AddPostDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"addPost"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"net"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"to"}},"type":{"kind":"NonNullType","type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"message"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"images"}},"type":{"kind":"NonNullType","type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"postPost"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}},{"kind":"Argument","name":{"kind":"Name","value":"net"},"value":{"kind":"Variable","name":{"kind":"Name","value":"net"}}},{"kind":"Argument","name":{"kind":"Name","value":"to"},"value":{"kind":"Variable","name":{"kind":"Name","value":"to"}}},{"kind":"Argument","name":{"kind":"Name","value":"message"},"value":{"kind":"Variable","name":{"kind":"Name","value":"message"}}},{"kind":"Argument","name":{"kind":"Name","value":"images"},"value":{"kind":"Variable","name":{"kind":"Name","value":"images"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"message"}},{"kind":"Field","name":{"kind":"Name","value":"root"}}]}}]}}]} as unknown as DocumentNode<AddPostMutation, AddPostMutationVariables>;
export const AddThreadDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"addThread"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"token"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"net"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"board"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"message"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"title"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"images"}},"type":{"kind":"NonNullType","type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"postThread"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"token"},"value":{"kind":"Variable","name":{"kind":"Name","value":"token"}}},{"kind":"Argument","name":{"kind":"Name","value":"net"},"value":{"kind":"Variable","name":{"kind":"Name","value":"net"}}},{"kind":"Argument","name":{"kind":"Name","value":"board"},"value":{"kind":"Variable","name":{"kind":"Name","value":"board"}}},{"kind":"Argument","name":{"kind":"Name","value":"message"},"value":{"kind":"Variable","name":{"kind":"Name","value":"message"}}},{"kind":"Argument","name":{"kind":"Name","value":"title"},"value":{"kind":"Variable","name":{"kind":"Name","value":"title"}}},{"kind":"Argument","name":{"kind":"Name","value":"images"},"value":{"kind":"Variable","name":{"kind":"Name","value":"images"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"post"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"message"}}]}}]}}]}}]} as unknown as DocumentNode<AddThreadMutation, AddThreadMutationVariables>;