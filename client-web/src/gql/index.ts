export { graphql } from './generated';

import { getApiRoute, getSubscriptionRoute } from "utils/routes";

import { WebSocketLink } from "@apollo/client/link/ws";
import { getMainDefinition } from "@apollo/client/utilities";

import { SubscriptionClient } from "subscriptions-transport-ws";
import {
  ApolloClient,
  HttpLink,
  split,
  InMemoryCache,
} from "@apollo/client";


const wsLink = new WebSocketLink(
  new SubscriptionClient(getSubscriptionRoute(), {})
);

const httpLink = new HttpLink({
  uri: getApiRoute(),
});

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === "OperationDefinition" &&
      definition.operation === "subscription"
    );
  },
  wsLink,
  httpLink
);

export const client = new ApolloClient({
  link: splitLink,
  cache: new InMemoryCache(),
});
