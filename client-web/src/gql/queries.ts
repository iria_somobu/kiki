import { graphql } from './generated/gql';

export const GQL_AVAILABLE_NETWORKS_QUERY = graphql(`
  query Networks($token: String!) {
    networks(token: $token) {
      id
      title
      icon
    }
  }
`);

export const GQL_POST_QUERY = graphql(`
  query PostRootThreadData($hash: String!, $token: String!) {
    post(hash: $hash, token: $token) {
      hash
      title
      root
      board
    }
  }
`);
