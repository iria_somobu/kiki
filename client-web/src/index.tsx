import React, { Suspense } from "react";
import ReactDOM from "react-dom/client";
import { Trans as Translation, useTranslation } from "react-i18next";
import { Provider as ReduxProvider } from "react-redux";

import { ApolloProvider } from "@apollo/client";
import "@fontsource/ibm-plex-sans/700.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";

import App from "App";
import { client } from "gql";
import { PersistGate } from "redux-persist/integration/react";
import { persistor, store } from "store/indexStore";

import "./i18n";
import "./index.css";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

window.addEventListener("dragover", (e) => e.preventDefault(), false);
window.addEventListener("drop", (e) => e.preventDefault(), false);

const Core: React.FC = () => {
  const { t } = useTranslation();

  const loadingContainer = (
    <div className={"loading_container"}>{t("loading.short")}</div>
  );

  return (
    <ApolloProvider client={client}>
      <ReduxProvider store={store}>
        <Suspense fallback={loadingContainer}>
          <PersistGate loading={loadingContainer} persistor={persistor}>
            <App />
          </PersistGate>
        </Suspense>
      </ReduxProvider>
    </ApolloProvider>
  );
};

root.render(
  <Translation>
    <Core />
  </Translation>
);
