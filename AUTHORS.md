# Authors

The team:
* Ilya Somov (@somobu) - Project leader;
* Dmitry Kuznetsov (@tabstabs90) - UI/UX designer;
* @1amL0st - Fullstack programmer;

Contributors:
* @BiFotLy
* @videobitva
