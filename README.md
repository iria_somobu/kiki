# Kitsuki

Imageboard client and server written on Rust, Graphql and React.

![](https://gitlab.com/somobu/kitsuki/-/raw/master/docs/header_v1_0.png)


## Is there any project demo?

Yes: visit [kitsuki.cc][srv_prod] - it is not just a demo, it is completely functioning imageboard.

Also, there is dev server, you can interact with it [here][srv_dev]. The server is not intended for public use due to database wipes on each new commit in `master` and may be broken or unstable from time to time.


## How to build and run?

**Server**: go to `server` directory and build & run server in debug mode with `cargo run` (Rust toolchain is required). When deploying server somewhere, keep in mind that you have to place `config.yaml` in server's working dir.

**Client**: go to `client-web` directory and build & run client in debug mode with `npm start` (both Node and running debug server are required).


## Artifacts

If you want to setup Kitsuki yourself but dont want to tinker with compilation you can just grab these artifacts, extract client-web near `kitsuki_server`, place there `config.yaml` (something like [this][art_config], just dont forget to edit it) and launch `./kitsuki_server`.

* Server: [latest on master][art_server]
* Client: [latest on master][art_client]

[art_config]: https://gitlab.com/somobu/kitsuki/-/blob/master/confs/ci/server-config-devel.yaml
[art_server]: https://gitlab.com/somobu/kitsuki/-/jobs/artifacts/master/file/kitsuki-server?job=build_server
[art_client]: https://gitlab.com/somobu/kitsuki/-/jobs/artifacts/master/file/client-web.tar.gz?job=build_web_client


## Dev resources

* Design: [figma][figma]
* Dev-server: [80.237.90.71:14237][srv_dev]
* Docs:
  - [rust doc][rust_apidoc] - API documentation;
  - [server notes][notes_srv] - DB explained + tips;
* Git hooks: `cp -r confs/hooks .git/`


[figma]: https://www.figma.com/file/dDgqaVg8Ceq4H8lHx9kJIm/Kitsuki?node-id=4
[notes_srv]: https://gitlab.com/somobu/kitsuki/-/blob/master/docs/server.md
[rust_apidoc]: https://somobu.gitlab.io/kitsuki/api/index.html

[srv_dev]: http://80.237.90.71:14237/
[srv_prod]: https://kitsuki.cc/


## License

This project is created by [a few people][authors] and distributed under [GNU GPL v3 License][license].

[authors]: https://gitlab.com/somobu/kitsuki/-/blob/master/AUTHORS.md
[license]: https://gitlab.com/somobu/kitsuki/-/blob/master/LICENSE
