# About web server

## Routes

```
/           [client-web]'s index.html
/index      -//-
/index.html -//-
/api        [server]'s graphql api
/img        [server]'s image api
/static     [client-web] static files
```


## Database
Hand-written schema

![](https://gitlab.com/somobu/kitsuki/-/raw/master/docs/db.png)


## Implementation details

### Post hashing
Создай вспомогательную функцию `calc_hash(message: String, title: String, to: Vec<String>, imgs: Vec<String>) -> String`, и передавай в нее соответствующие поля (дефолться на пустую строку и пустой вектор). 

Возвращай или sha3-256 от суммарной строки всех аргументов (вектора собирай через простой `.join("")`).

