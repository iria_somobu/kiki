# FAQ

## Why registration is limited?

Since registration is completely optional on this site, the functionality of accounts and free registration is given
attention on a residual basis. To put it simply, the registration is not done in full, and therefore - inaccessible to
regular users.
