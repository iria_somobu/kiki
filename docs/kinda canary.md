# Data collection
- No intentional data collection performed;
- Token is required for API access but only token string itself is stored on server;
- User's IP address, access time, http request details may be logged by Nginx but this data is not processed further;

# Data access
- No data except available through public API was transmitted to a 3rd party;

